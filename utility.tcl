#   DEFINE UTILITIES


#--  Return Row Number of insert cursor
proc GetRowNumber {} {
	return [ expr int([$::txt index insert]) ]
}


#--   Display markdown syntax and 'details' (expandable text)
proc ViewSyntax {} {
	set ::HideSyntax false;
	$::txt tag configure syntax -elide $::HideSyntax
	$::txt tag configure TrailingSpace -elide $::HideSyntax
	$::txt tag configure metadata -elide $::HideSyntax
}

#--   Hide markdown syntax and 'details' (expandable text)
proc HideSyntax {} {
	set ::HideSyntax true;
	$::txt tag configure syntax -elide $::HideSyntax
	$::txt tag configure TrailingSpace -elide $::HideSyntax
	$::txt tag configure metadata -elide $::HideSyntax
}

#--  Insert Date/Time in cursor position
proc InsertTime {} {
	$::txt insert insert [clock format [clock seconds] -format "%d %b %Y"]
}

#--  Return an absolute path with 'source' as a reference
proc GetAbsolutePath { Path  Source } {
	if { $Source ne "" && ! [ string match "/*" [file dirname $Path] ] && \
			! [ string match "http*" $Path]  } {
         	return [ file normalize [file dirname $Source]/$Path ]
   } else { return $Path }
}

#--  Return a relative path with 'source' as a reference
proc GetRelativePath { Path  Source } {
	if { ! [ string match "http*" $Path] && [wm title . ] ne "Untitled"   } {
		#set RelativePath [exec  realpath --relative-to [file dirname $Source] $Path ]
		set RelativePath [ fileutil::relative [file dirname $Source] $Path ]
		#puts "DebugRelPath:  Path <$Path>  Source <$Source>\n  Result <$RelativePath>"
		return $RelativePath
	} else { return $Path }
}

#-- Return Temp dir depending on OS
proc getTempDir {} {
    if {[file exists /tmp]} {  return /tmp  								;# UNIX
    } elseif {[info exists ::env(TMP)]} { return $::env(TMP)		;# Windows
    } elseif {[info exists ::env(TEMP)]} {  return $::env(TEMP)  	;# Windows
    } elseif {[info exists ::env(TMPDIR)]} {	return $::env(TMPDIR);# OSX
    }
}

#--  Capitalize all characters of a string
proc capitalize {string} {
	set Text0 [string tolower $string]
	return [string toupper [string index $Text0 0]][string range $Text0 1 end]
}

#--  Capitalize each first characters of words in a string
proc capitalizeEachWord {sentence} {
	set Text0 [string tolower $sentence]
	regsub -all {\S+} [string map {\\ \\\\ \$ \\$} $Text0] {[string toupper [string index & 0]][string range & 1 end]} cmd
	return [subst -nobackslashes -novariables $cmd]
}

