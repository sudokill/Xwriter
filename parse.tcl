#!/usr/bin/wish
# Parse markdown files and set tags (bold,...)
#	1. ParseLine :  Parse one line
#  1.1  Generic parsing for bold,underline...
#  1.2  ParseBlock : Parse block of lines 
#	( Require forward/backward search, specific commands from current line)
#  1.3 Image parsing for the line
#	2. ParseAll : Parse ALL the document with regexp (slow)


#===   PARSE  LINE  REQUIRING FORWARD/BACKWARD SEARCH, or function
proc ParseBlock { RowNum Row ListTag} {
   set PreviousLine [expr $RowNum - 1]
	#puts "DebugParseBlock: $RowNum  Tag:$ListTag"
	switch -regexp $Row {
		{^\s*```}	{;  #----   FENCED CODE BLOCK WITH "```"
						if { ! [string match "*syntax*"  $ListTag ] } {; #Not a previous ``` already tagged
							set EndBlock [$::txt search -- "```" $RowNum.end end]
							if { $EndBlock ne "" } { ;#-- Valid Code Block
								$::txt tag add syntax  $RowNum.0 $RowNum.end
								$::txt tag add CodeBlock $RowNum.0+1l  $EndBlock
								$::txt tag add syntax "$EndBlock linestart" "$EndBlock lineend"
							}
						}
					}
		{^\t*(?:\*|-|\+|•)\s+} { ; #-- LIST Unordered  with Spaces/Tabs and *,-,+
							regexp -indices {\S} $Row Range; # Position non-whitespace
							set ID [lindex $Range 0]
				   		set EndList [$::txt search -regexp {^\s*(?:\*|-|\+|•)\s+|^\s*$|^```} $RowNum.end end]
							if { $EndList eq "" } { set EndList $RowNum.end+1c }
							#puts "-DebugList($RowNum)  list$ID  End:$EndList $Row"
							$::txt tag add list$ID  $RowNum.0 $EndList-1c
						}
		{^\t*(\d+\)|\d+\.)\s+}	{  ;#---  LIST Ordered  0-2Tab and XX. XX)
							regexp -indices {[0-9]+\.|[0-9]+\)} $Row Range;  # Range Position Bullet
							set ID [lindex $Range 0]
							set EndList [$::txt search -regexp {^[\t]{0,2}(\d+\)|\d+\.)\s+|^\s*$|^```} $RowNum.end end]
							if { $EndList eq "" } { set EndList $RowNum.end+1c }
							$::txt tag add listNum$ID  $RowNum.0 $EndList-1c
						}
		{^\|\t+?}	{  ;#---  PARAGRAPH INDENT (|+Tabs+Text)
							regexp -indices {[^\|]\S} $Row TabLine
							set NbTabs [lindex $TabLine 0]; # Number of tabs
				   		set EndBlock [$::txt search -regexp {^\|\t+?|^\s*$|^```} $RowNum.end end]
							if { $EndBlock eq "" } { set EndBlock $RowNum.end+1c }
							$::txt tag add indent$NbTabs  $RowNum.0 $EndBlock-1c
							$::txt tag add syntax $RowNum.0  $RowNum.1
						}
		{^-{3,}$}	{	; #--   HORIZONTAL RULE
							DisplayHorizontalRule  $RowNum  $Row  Normal
						}
		{^\*{4,}$}	{	; #--   PAGE BREAK
							DisplayHorizontalRule  $RowNum  $Row  PageBreak
						}
		{^:\s+.*?}	{	; #--   DEFINITION LIST
							$::txt tag add DefinitionListTerm  $PreviousLine.0 $PreviousLine.end
							$::txt tag add syntax $RowNum.0  $RowNum.1
				   		set EndBlock [$::txt search -regexp {^$} $RowNum.end end]; # search empty lines
							$::txt tag add DefinitionList $RowNum.2  $EndBlock
						}
		{^(:>|▶).*?} {	; #--   EXPANDABLE CONTENT (DETAILS)
							set EndBlock [$::txt search -regexp {^$} $RowNum.end end]; #search empty line
							if { $EndBlock ne "" } { ;#-- Valid Block
								set Tag_Detail  Details[md5::md5 -hex $Row]
								$::txt tag add $Tag_Detail  $RowNum.end  $EndBlock-1c
								$::txt tag configure $Tag_Detail -elide 1
								$::txt tag raise $Tag_Detail;  # Set tag Details higher in priority than tag 'syntax'
							}
						}
		{([: ]*?-{3,}[: ]*?)\|([: ]*?-{3,}[: ]*?)}	{ ;#--  TABLE SEPARATOR as ---|----...
									$::txt tag add TableHeader $PreviousLine.0  $PreviousLine.end
									$::txt tag remove TrailingSpace $PreviousLine.0  $PreviousLine.end; # Necessary as trailing spaces collide with table header style 
									$::txt tag add syntax  $RowNum.0  $RowNum.end+1c
									$::txt tag add Table  $RowNum.0  $RowNum.end+1c
									set EndTable [$::txt search -regexp {^\s*$|^```} $RowNum.end end]
									$::txt tag add Table  $RowNum.0 $EndTable
								}
		default    { return 1;  # Continue Block Parsing  }
	};  # end switch
	return 0;  # Stop Block Parsing
}

#====  PARSE A LINE IN MARKDOWN
proc ParseLine { RowNum Row } {
	if { $Row eq "" } { return };  # Nothing to do if empty line

	#--  Inside Code Block: Code Highlighting (slow),  Stop Line Parsing
	set ListTag [$::txt tag names $RowNum.0]; # Get Tags
	regexp -lineanchor {^(```)(.*?)$} $Row match symbol ::CodeLanguage; # set language
	if { [string match "*CodeBlock*"  $ListTag ] } {
		#FormatCodeBlock [string trim $::CodeLanguage] $RowNum $Row; # Highlight at start
		return
	}


	#--  Replace list bullet '*' with '•' 
	#      (except in expandable paraph (details). bug)
	if { [regsub -all -line {^(\s*?)(\*)(\s+.*?)} $Row {\1•\3} Row] && \
			! [string match "*Details*" $ListTag] } {
		$::txt replace $RowNum.0 $RowNum.end  $Row
	}
	#--  Replace details (expandable paraph) ':>' with '▶'  \u25b6
	if { [regsub -all -line {^(:>\s+)(.*?)$} $Row {▶ \2} Row] } {
		$::txt replace $RowNum.0 "$RowNum.0 lineend"  $Row
	}
	
	#--   PARSE  BLOCK ( require forward/backward search or specific function )
	ParseBlock $RowNum $Row $ListTag

	#--  Parse document Title based on H1 title with 2 trailing spaces
	#if { ! info exists $::DocumentTitle } {
	#	regexp -all -lineanchor  {^(#\s+)(.*?)  $} $Row match group1 ::DocumentTitle
	#}

	#--   PARSE FULL-LINE  (syntax)(Line To Tag)(syntax)
	# \S \s (no)whitespace \y wordBoundary .*? .+? :AnyThing with 0/1+ char until next match  \1 Repeat group1(First ())

	if { ![ TagReg title {^(#\s+)(.*?)  $} $Row $RowNum ] } {
	} elseif { ![ TagReg heading1 {^(#\s+)(.*?)$} $Row $RowNum ] } {
	} elseif { ![ TagReg heading2 {^(##\s+)(.*?)$} $Row $RowNum ] } {
	} elseif { ![ TagReg heading3 {^(###\s+)(.*?)$} $Row $RowNum ] } {
	} elseif { ![ TagReg heading4 {^(####\s+)(.*?)$} $Row $RowNum ] } {
	} elseif { ![ TagReg heading5 {^(#####\s+)(.*?)$} $Row $RowNum ] } {
	} elseif { ![ TagReg heading6 {^(######\s+)(.*?)$} $Row $RowNum ] } {
	} elseif { ![ TagReg BlockQuote2 {^(\s*>>\s*)(.*?)$} $Row $RowNum ] } {
	} elseif { ![ TagReg BlockQuote {^(\s*>\s*)(.*?)$} $Row $RowNum ] } {
	} elseif { ![ TagReg details  {^(:>\s+)(.*?)$} $Row $RowNum ] } {
	} elseif { ![ TagReg details  {^()▶\s+(.*?)$} $Row $RowNum ] } {
	} elseif { ![ TagReg fulljustify {^(/\s+)(.*?)$} $Row $RowNum ] } {
	} elseif { ![ TagReg center {^(//\s+)([^\{].*?)$} $Row $RowNum ] } {
	} elseif { ![ TagReg center {(<center>)(.+?)(</center>)} $Row $RowNum ] } {
	} elseif { ![ TagReg right {^(///\s+)(.*?)$} $Row $RowNum ] } {
	}

	#--  PARSE  WORDS   (syntax)(Words To Tag)(syntax)
	TagReg listBullet	   {^()\s*(\*|-|\+|•)\s+()} $Row $RowNum
	TagReg listBullet	   {^()\s*(\d+\)|\d+\.)\s+()} $Row  $RowNum
	TagReg HTMLalias		{(?:")({)([^\\]*?)(})}	$Row $RowNum;  # "Text"{#red...}
	TagRegS TrailingSpace { {2,}$}	$Row $RowNum; # At least 2 trailing spaces
	TagReg footnote		{(\[\^)([^\]]+?)(\])(?!:)}  $Row $RowNum ;# [^Text]
	TagReg URL				{^(\s*\[)(.+?)(\]):\s+}	$Row $RowNum; # footnote description [^Text]:
	TagReg italics       {(\y_)((?=\S)[^_]*?[^\\])(_)(?=( |,|.|$))} $Row $RowNum;
	TagReg italics			{(\*)((?=\S)[^\*]*?[^\\])(\*(?!\*))}	$Row $RowNum;			  # *Text*
	#TagReg italics       {(\*)((?=\S)[^\*]*?[^\\])(\*)(?=( |$))} $Row $RowNum;
	TagReg italics			{(<i>)(.*?)(</i>)}			$Row $RowNum
	TagReg bold				{(\*\*)((?=\S).*?\S)(\*\*)}	$Row $RowNum
	TagReg bold				{(<b>)(.*?)(</b>)}			$Row $RowNum
	TagReg bold				{(___)((?=\S).*?\S)(___)}	$Row $RowNum;  # For Bold Underline
	TagReg BoldItalics	{(\*\*\*)((?=\S).*?\S)(\*\*\*)}	$Row $RowNum
	TagReg underline		{(__)((?=\S).*?\S)(__)}		$Row $RowNum
	TagReg underline		{(<u>)(.*?)(</u>)}			$Row $RowNum
	TagReg overstrike    {(~~)((?=\S).*?\S)(~~)}  	$Row $RowNum
	TagReg highlight     {(<mark>)(.*?)(</mark>)}	$Row $RowNum
	TagReg highlight     {(==)([^=]+?)(==)}	$Row $RowNum;			  # ==Text==
	TagReg superscript	{(<sup>)(.*?)(</sup>)}		$Row $RowNum;
	TagReg superscript	{(\^)([^\\]+?)(\^)}		$Row $RowNum;			# ^Test^
	TagReg subscript		{(<sub>)(.*?)(</sub>)}		$Row $RowNum;
	TagReg subscript		{(~)([^~\\]+?)(~(?!~))}	$Row $RowNum;			  # ~Text~
	TagReg BigFont			{(<big>)(.*?)(</big>)}			$Row  $RowNum
	TagReg SmallFont		{(<small>)(.*?)(</small>)}			$Row  $RowNum
	#TagReg ModificationAdded  {(<ins>)(.*?)(</ins>)}  $Row $RowNum
	#TagReg ModificationDeleted  {(<del>)(.*?)(</del>)}  $Row $RowNum
	TagReg URL				{(\[)([^\[\]]+?)(\]\(.+?\))}	$Row $RowNum; # [Name](Pathfile..)
	TagReg URL				{(\[\]\()(.+?)(\))}			$Row $RowNum; # [](http or file)
	TagReg URL				{(<)(http.+?[^\\])(>)}		$Row $RowNum; # <http..>
	TagRegS URL			   {(www|http:|https:)[^\s]+[\w]} $Row $RowNum; # http..
	TagReg Comment			{(<!)(--.*?--)(>)}			$Row $RowNum
	TagReg BlockHTML		{(<kbd>)(.*?)(</kbd>)}		$Row $RowNum
	TagReg EscapeChar	{(\\)([`_':"/~=<>#\!\{\}\-\*\+\|\^\[\]\(\)\\])()}	$Row $RowNum
	TagReg Task			{^\s*(- )(\[[Xx ]\])()}	$Row $RowNum
	TagReg BoldItalics	{(>\s+?\[!)(.*?)(\])}	$Row $RowNum;  # alerts as >[!WARNING]
	TagReg CodeInline		{()(\$\$.+?\$\$)()} 		$Row $RowNum; # Math Equation
	TagRegS syntax			{(width|alt).+?</img>} $Row $RowNum; # Hide image syntax as width= ... </img>

	#---  Tag CodeInline  (To do AFTER other TagReg instruction. Remove other tags)
	TagReg CodeInline		{(`)([^`]*?[^\\`])(`(?!\{))} $Row $RowNum;  	#  Regexp CodeInline

	#--  Parse Image : MUST BE DONE LAST ( Introduce offset in text position )
	DisplayImage  $RowNum  $Row;   								# Display/Tag  Image

	#puts "DebugParse: $RowNum  Tags: $ListTag"

};   # End ParseLine


#===    PARSE ALL THE DOCUMENT ( vs line-by-line before )
proc ParseAll { RowNum  Text } {
		#  Empty Line : ^\s*$
		#TagReg BlockHTML	{^()(<div[^\\]*?>.*?</div>\n)()}	$Text $RowNum
		#TagReg DefinitionList {^(:\s+?)(.*?)()\n\n}	$Text $RowNum
		#--  Parse Metadata just for the first 20 lines 
		TagReg metadata {^(---)(\n.*?:.*?){1,}?(---)$} [$::txt get 1.0 20.0] $RowNum
		#--  Parse Center paragraph
		#TagReg center {(<p align="center".*?>)(.*?)(</p>)} $Text $RowNum
		#TagReg center {(<div align="center".*?>)(.*?)(</div>)} $Text $RowNum
		#--  Parse Details/Summary
		#TagReg details	{(<details>.*?<summary)(>.*?<)(/summary>)}	$Text $RowNum
}

#===  GENERIC REGEX TO TAG TEXT  ( Pattern as <syntax>Text<syntax> )
proc TagReg { tag pattern Text RowNum } {
	set RegIndice [regexp -indices -all -inline -lineanchor $pattern $Text ]
	if { $RegIndice eq "" } { return 1 }
	#if [string match "*" $tag] { puts ":parseTagReg: ($RowNum)  $tag $pattern  $RegIndice\n"}
	#--   For HTML alias/comment, the pattern is reverse : <Text>syntax<Text>
	if { $tag eq "HTMLalias" || $tag eq "Comment" } {
		set ReverseTag 1
	} else { set ReverseTag 0 }
	#--  Parse elements
	foreach elt  $RegIndice {
     	lassign [split $elt " "] x0 x1
		set start $RowNum.0+${x0}c
		set end  $RowNum.0+${x1}c+1c
		incr i 1;
		#--  TAG  SYNTAX ( i=2,4.. / group1 )
		if { ! [expr $i%2] && $x1>=$x0 }	{
			#puts  "DebugTagReg ($RowNum): $tag: $Text"
			if { ! $ReverseTag } {
				$::txt tag add syntax $start $end
			} else  { $::txt tag add $tag $start $end }
		}
		#--  TAG  TEXT  ( i=3,7.. / group2 )
		if { ! [expr ($i+1)%4] && $x1>=$x0 } {
			#--  if CodeInline, remove previous tags and retag text after
			if { $tag eq "CodeInline" } { RemoveAllTag  $start  $end }
			if { ! $ReverseTag } {
				$::txt tag add $tag $start  $end
			} else { $::txt tag add syntax $start $end }
		}
		#--   TAG URL PATH NAME
		if { $tag eq "URL" && ! [expr $i%4] && $x1>=$x0 } {
			$::txt tag add URLpath  $RowNum.0+${x0}c+2c $RowNum.0+${x1}c
		}
	}
	return 0
}

#===  GENERIC REGEX TO TAG TEXT without syntax  ( NO  <syntax> TextToTag <syntax> )
proc TagRegS { tag pattern Row RowNum } {
	set RegIndice [regexp -indices -all -inline -lineanchor $pattern $Row]
	foreach elt  $RegIndice {
     	lassign [split $elt " "] x0 x1
		if { $tag eq "CodeInline" } {
			RemoveAllTag "$RowNum.0+${x0}c" "$RowNum.0+${x1}c+1c"
		}
		$::txt tag add $tag $RowNum.0+${x0}c $RowNum.0+${x1}c+1c
	}
}

#===    DISPLAY/TAG  Horizontal Rule
proc DisplayHorizontalRule { RowNum Row Type } {
	if { $Type eq "Normal" } { set NextTag [string last "-" $Row]; set Color grey}
	if { $Type eq "PageBreak" } { set NextTag [string last "\*" $Row]; set Color #dc342a }
	$::txt tag add syntax $RowNum.0 $RowNum.$NextTag+1c
	frame $::txt.horizRule$RowNum
	$::txt.horizRule$RowNum configure -background $Color -height 2 -width [winfo screenwidth .]
	$::txt window create $RowNum.$NextTag+1c -window $::txt.horizRule$RowNum
}

#===    DISPLAY/TAG  IMAGE(S)  FOR A LINE
proc DisplayImage { RowNum Row } {
	set i 0
	set ListImage {};  # Init list of Image to display
	#--  Convert <img src...> to markdown syntax ![](PathToImage)
	# Image path must be exactly at the same row.index than <img src...
	regsub -all {(<img src=["'])(.*?)(["'].*?>)} $Row {![img123](\2)} Row

	#--  Parse/Display Image
	set pattern {(!\[)(.*?)(\]\()(.+?)(\))}; # ![Name;OptionalWidth](Path)
	foreach elt  [regexp -indices -all -inline $pattern $Row] {
		incr i 1;  # Syntax(i=2,4,6,8...)  Name(i=3,9...)  Path(i=5,11..)
     	lassign [split $elt " "] x0 x1
		#puts "DebugParseImg $i. ($elt) [$::txt get $RowNum.[lindex $elt 0] $RowNum.[lindex $elt 1]+1c ]"
		#-- If in codeinline, ignore image display, goto next image in same line
		if { [ $::txt tag names $RowNum.$x0 ] eq "CodeInline" } { continue }
		#-- Remove Tag URL ( issue when clicking with ![Name;100px](xx) )
		$::txt tag remove URL $RowNum.$x0 $RowNum.$x1+1c
		$::txt tag remove URLpath $RowNum.$x0 $RowNum.$x1+1c
		#--  Tag/Get Image Name, width and Path
		if { ! [expr $i%2] }	{ $::txt tag add syntax $RowNum.$x0 $RowNum.$x1+1c }
		if { ! [expr ($i+3)%6] } {;  # Name of Image + Width
			$::txt tag add syntax $RowNum.$x0 $RowNum.$x1+1c
			set ImageName [$::txt get $RowNum.$x0 $RowNum.$x1+1c]
			set ImageWidth [lindex [split $ImageName ";"] 1]
			#puts "DebugParseImg1: Name:$ImageName   Width:$ImageWidth"
		}
		if { ! [expr ($i+1)%6] } {;   # Path/URL of image : Add to the list
			$::txt tag add syntax $RowNum.$x0 $RowNum.$x1+1c
			$::txt tag add ImagePath $RowNum.$x0 $RowNum.$x1+1c
			set ImagePath [$::txt get $RowNum.$x0 $RowNum.$x1+1c]
			set ListImage [ linsert $ListImage 0 $ImagePath $RowNum.$x1+2c $ImageWidth ]
			#puts "DebugParseImg2:\n  Path:$ImagePath\n  ListImage:$ListImage"
		}
	};  # End foreach elt


	#==  DISPLAY IMAGES in REVERSE order  (Display image introduce an offset on position indice)
	foreach {ImagePath Pos ImageWidth} $ListImage {
		#puts "DebugParseImg3: $ImagePath  Pos:$Pos   Width:$ImageWidth"
		if { $ImagePath ne "" } { InsertImage $ImagePath $Pos $ImageWidth }
	}
	#--  Add a space at EndOfLine if line contain only the image
	#  A space(or any char) is just necessary to justify image  (center...)
	if { $ListImage ne "" && [ $::txt get $Pos+1c $Pos+2c ] eq "\n" } {
		$::txt insert $Pos+1c " "
	}

}
