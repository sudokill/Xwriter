## Header2
**bold**   _Italic_   __Underline__    ***BoldItalic***      ___BoldUnderline___     with <big> big </big> and <small> small </small> font 
~~Strikeout~~     <mark>Highlight</mark>      H<sup>2</sup>O<sub>3</sub>    `Code **Inline**`    http://google.com    [URLlink](http://google.com)    Comment now :  <!--  A comment now  -->  

### Header3
- [ ]   Task ToDo       ∞‰▪▸•○◇♦◦➢➤⇒√≤≥≠≈±
- [x]   Task Done  
:> Details Summary  
Text Details1  

a footnote reference[^1]  
****
```tcl
#-- Code block in monospace, syntax highlighting
puts "**Xwriter**";   set $var_s;   set ${var_x}
```
// Center Image ![Name quick;100px](../test/test1.jpg) with **custom zoom**
/// 					__Justify right and underline__

|  Header1       |   Header2          |   Header3          
|----------------|:------------------:|-------------------:|  
| Left1          | Center1            | Right1

[^1]: the footnote description  