#!/usr/bin/wish
#  Define Keyboard/Notebook Shortcut/event
#  Note: Shortcut/Event for treeview are defined in plugin/treeView.tcl
#   Some example: /usr/share/tcltk/tk8.6/text.tcl
#  Use F10 to get the text associated to a key


#==  UNBIND SOME KEYS (predefined bindings from Text widget)
proc UnbindKey {} {
	set bindings [list	<Control-space> <Control-t> <Control-o> <Control-n>\
    						<Control-i>	<Control-k> <Control-Tab>
	]
	foreach keysym $bindings {
		bind Text $keysym {return 0}
	}
}


#====  MOST-USED  SHORTCUT
bind . <F10>	{ 
	Identify_Key_Pressed
	break
}


#-- View/Hide Markdown Symbol
bind $::txt <Control-space>	{
	set status " View markdown symbol:  $::HideSyntax ";
	set ::HideSyntax [expr {!$::HideSyntax}];
	if { ! $::HideSyntax } { ViewSyntax } else { HideSyntax }
	break;
}

#--  Change Theme
bind $::txt <Control-t> {
	# Warning: use "bind ./all" do not work (transpose chars in insert cursor position)!  ) 
	switch -glob $cfg(MAIN.GlobalTheme) {
		"default"	{ set cfg(MAIN.GlobalTheme) dark }
		"dark"		{ set cfg(MAIN.GlobalTheme) terminal }
		"terminal"	{ set cfg(MAIN.GlobalTheme) default }
		default		{ set status " Error: Theme Undefined " }
	}
	LoadTheme
	set status " Theme:  $cfg(MAIN.GlobalTheme)"
	config:save $ConfFile ""
	break
}


#====  SHORTCUT ESSENTIAL
bind $::txt <Alt-space>	{ tk::TextSetCursor $::txt [tk::TextScrollPages $::txt 1]; break }
bind $::txt <Home>		{ $::txt see [$::txt index 0.0 ] ; break}
bind $::txt <End>			{ $::txt see [$::txt index end ] ; break}
bind $::txt <Control-A>	{ $::txt tag add sel 1.0 end ; break}
bind . <Control-q>	{
	if { [$::txt edit modified] } {
		if { [tk_messageBox -message "Document not saved. Quit ? \n" -type yesno -icon question] eq yes } {exit}
	} else {
		#--  Set/Save Config with ScriptPath/config.cfg as reference
		if { $::FileName ne $ConfFile } {     ;# Not editing preference manually
			set cfg(INFO.WindowGeometry) [wm geometry .];  # Save window geometry
			config:save $ConfFile $ScriptPath/config.cfg
		}
		exit
	}
}
bind . <Control-s>	{  SaveFile $FileName ; break; }
bind . <Control-S>	{ SaveFile ""; break; }
bind $::txt <Control-o>	{ LoadFile "" ""; break;};  # if bind to . ,insert a line
bind . <Control-O>	{ CreateRecentFileList; break; }
bind . <Control-r>	{ ParseDocument ; break;}
bind . <Control-P>	{ LoadFile $ConfFile "NoBackup"; break; }
bind . <Escape>		{
	$::txt tag remove sel 0.0 end; $::txt tag remove search 0.0 end
	set SpellStart 1.0
	set ::status ""; .fr.tb.entry configure -background white -show ""
	$::txt configure -cursor xterm
	bind $::txt <y> {}; bind $::txt <n> {};			# y/n Used by search. Reset if esc
	ResetExtraView
}
bind . <Control-exclam>  { AskUser " "; break};  # Enter command on entry widget


#====  SHORTCUT  ADVANCED (goto, search...)

bind . <Control-g>  { AskUser " GoTo : "; break};
bind . <Control-f>  {
	set SearchMode [ dialog_FindReplace .dialog "" ];  # Open dialog window
	if { $SearchMode eq "" } { focus $::txt; break };	#  Nothing to do
	lassign  $SearchMode  FindStr  ReplaceStr  ModeRegexp  ReplaceAll
	set LastSearch  [SearchStr $FindStr  $ReplaceStr  $ModeRegexp $ReplaceAll  1.0]
	if { $::projectFile != "" } {  HideCodeBlock false };  # View all code in project mode 
	#--  Interactive or automatic Find/Replace
	if { $ReplaceAll eq 0 } {
		#--  Interactive Find/Replace
		bind $::txt <Control-n>  { set LastSearch	[SearchStr $FindStr $ReplaceStr $ModeRegexp  $ReplaceAll $LastSearch] ; break}
	} else {
		#--  Replace All occurences
		while { $::LastSearch ne 1.0 } {
			set ::LastSearch	[SearchStr $FindStr $ReplaceStr $ModeRegexp $ReplaceAll $::LastSearch]
		}
	}
	break
}
bind . <Control-n>  { break };			#  Reserved for Find/Replace
bind $::txt <Control-n>  { break };			#  Reserved for Find/Replace
bind . <Control-w>  { Toggle_Wordwrap; break }
bind . <F1> {
	if { $::FileName ne "$::ScriptPath/help.md" } { 
		InitExtraView "Help"
	} else { ResetExtraView }
	break
}
bind $::txt <Control-L>	{
   #-- If selection, Info about position+tags, lines number otherwise
	if { [ $::txt tag ranges sel ] ne "" } {
		set ListTags  [GetAllTags [$::txt index sel.first] [$::txt index sel.last] ]
		set message " [$::txt index sel.first] $ListTags"
	} else {
		set message " [$::txt index insert] ([$::txt count -lines 1.0 end ] lines)"
	}
	tk_messageBox -message " $::FileName \n $message \n" -type ok -icon info
	break;
}
bind . <Control-B>  {
	if { $::PaneView ne "Bookmark" } {
		CreateBookmarkList
	} else  { TreeViewClose }
	break;
}
bind $::txt <Control-p>  {
	#--  bind to $::txt (and not .) : Avoid a scrolling to 'insert' cursor with ctrl-p 
	set outfile "$::TempDir/preview.html";
   ExportAsHTML $outfile
	break
}

#====  PAGE VIEW
bind . <Control-less>	{
	switch -glob $cfg(GEOMETRY.PageView) {
		"portrait"	{ set cfg(GEOMETRY.PageView) "large" ; }
		"large"		{ set cfg(GEOMETRY.PageView) "portrait" }
		default		{ set cfg(GEOMETRY.PageView) "large" }
	}
	set status " Page View : $cfg(GEOMETRY.PageView) "
	SetTextWindowWidth  $cfg(GEOMETRY.PageView)
	config:save $ConfFile ""
	break;
}


#===  OTHERS

bind . <Control-plus>	{  ChangeFontSize 1; break;  }
bind . <Control-minus>	{ ChangeFontSize -1 ; break; }
bind . <F2>	{
	#-- First Spell: cursor at 1.0/no text selected (spell check aborted by user )
   #   Note: Escape set also spell position at 1.0
	if { $SpellStart eq "1.0" || [$::txt tag ranges search ] eq "" } {
		set SpellStart 1.0
		if { [ $::txt tag ranges sel ] ne "" } { set SpellStart [$::txt index sel.first] }
		if {[init_spell] eq 1} { set SpellStart 0}; # if Init return 1, Stop now
	}
	#--  Start next spell
	if { $SpellStart ne 0 } { set SpellStart [check_spell $SpellStart ] }
	break
}
bind . <F3>	{
	if { $SpellLanguage ne "en" } { set SpellLanguage "en"
	} else { set SpellLanguage $cfg(MAIN.CheckSpellLanguage) }
	TreeViewClose;					# Close treeview/Spell if already opened. 
	set status " Spell Language: $SpellLanguage"
	break;
}
bind . <F4>  { AskUser " Conjugate : "; break}
bind . <F5>  {	InitExtraView "Translate" }
bind . <F6>  { Display_Web_Image }
bind . <F8>  {
	set ::CodeEval [expr {!$::CodeEval}];
	set status " Code Evaluation: [ string map {0 "OFF" 1 "ON"} $::CodeEval ] ";
}
bind . <Control-colon> { InitExtraView "Search"; break }
bind . <Control-T>	{
	#--  Open Project files treeview or Table of content
	if { $projectFile ne "" }  {
		if { $::PaneView ne "ProjectMode"} { 
			OpenProjectTree $::projectFile
		} else  { TreeViewClose } 
	}
	if { $projectFile eq "" }  {
		if { $::PaneView ne "TOC"} { 
			CreateTOC "display"
		} else { TreeViewClose }
	}
	break;
}

#==  FOLD/UNFOLD CODE
bind $::txt <Control-Up>	{ HideCodeBlock true; set ::status " Code Folding: True"; break }
bind $::txt <Control-Down>	{ HideCodeBlock false; set ::status " Code Folding: False"; break }

#====  FORMATING WORD  ( Ctrl+b=bold... )
bind $::txt <Control-b>	{ ReplaceTag "Word" "bold" ""; break; }
bind $::txt <Control-i>	{ ReplaceTag "Word" "italics" "" ; break; }
bind $::txt <Control-u>	{ ReplaceTag "Word" "underline" ""; break; }
bind $::txt <Control-C>	{ InsertCodeBlock; break; }
bind $::txt <Control-m>	{ ReplaceTag "Word" "highlight" ""; break }
bind $::txt <Control-k>	{
	#--  Goto last link clicked. Reload previous file (or SearchBuffer)
	if { $::LastLink ne "" } {
		lassign $::LastLink LinkFile LinkLine
		#puts "_Shortcut_K:\n  FileName:<$::FileName>\n  Link:<$LinkFile> at $LinkLine"
		if { $::LinkFile ne "SearchBuffer" } {
			LoadFile  $LinkFile  "NoBackup"
		} else {
			text:restore  $::txt  $::SearchBuffer
		}
		$::txt yview [$::txt index $LinkLine ]; # scroll to view last link at edge of window
		#$::txt see [$::txt index [lindex $::LastLink 1] ]; # same as yview but center last link at the center of the window
	} else { set ::status " No previous link" }
	break;
}
bind $::txt <Control-U>	{ InitExtraView "SpecialChar"; break }
bind $::txt <Control-l>	{ $::txt insert insert "\[LinkName\](https://xxx)"; break;}
bind $::txt <Control-dollar> {;  # Insert HTML alias
	$::txt insert insert "\"\"{#red ##lightgrey |black 1.0em serif}"
	$::txt mark set insert "insert-38c"
}

#====  FORMATING LINE  ( Ctrl+h=heading... )

bind $::txt <Control-h>	{
  if { [ $::txt tag ranges sel ] eq "" } { set ::status " Select a text !"; break; }
	switch -glob  [$::txt tag name sel.first ] {
		*heading1*	{ ReplaceTag "Line" "heading2" "heading1"; set status " Heading2" }
		*heading2*	{ ReplaceTag "Line" "heading3" "heading2"; set status " Heading3" }
		*heading3*	{ ReplaceTag "Line" "heading4" "heading3"; set status " Heading4"	}
		*heading4*	{ ReplaceTag "Line" "heading5" "heading4"; set status " Heading5"	}
		*heading5*	{ ReplaceTag "Line" "heading6" "heading5"; set status " Heading6"	}
		*heading6*	{ ReplaceTag "Line" "" 			 "heading6"; set status ""	}
		default		{ ReplaceTag "Line" "heading1" ""; set status " Heading1" }
	}
	break
}
bind $::txt <Control-j>			{  	;# Justify text fill/center/right
	if { [$::txt tag ranges sel] eq "" } { set ::status " Select a text !"; break }
   switch -glob   [$::txt tag name sel.first ] {
		*fill*      { ReplaceTag "Line" "center" "fill"; set ::status " Center" }
		#*indent1*   { ReplaceTag "Line" "indent2" "indent1"; set ::status " Paragraph Indent2" }
		#*indent2*   { ReplaceTag "Line" "indent3" "indent2"; set ::status " Paragraph Indent3"}
		#*indent3*   { ReplaceTag "Line" "center"  "indent3";set ::status " Justify Center"}
		*center*    { ReplaceTag "Line" "right"   "center"; set ::status " Justify Right"}
		*right*     { ReplaceTag "Line" ""        "right" ; set ::status " Justify Left"}
		default     { ReplaceTag "Line" "fill" ""  ; set ::status " Fully-Justify"}
   }; break
}
bind $::txt <Control-I> {				;# Indent paragraph
	$::txt insert "insert linestart" "|\t";   # Indent Paragraph
	#$::txt insert insert "   " ;  # Insert 3 spaces 
	break;
}


#===   BUFFER
bind $::txt <Alt-Up>		{ AddToBufferList; break }
bind $::txt <Alt-Down>	{ RemoveFromBufferList; break }
bind $::txt <Alt-Right> { NavigateBuffer forward; break }; # Go forward in buffer list
bind $::txt <Alt-Left> { NavigateBuffer backward; break }; # Go backward in buffer list

#==  Project mode specific shortcut
bind . <Control-R> { };						# unbind project mode 'Run'
bind $::txt <Control-Tab>  { } 

#====  SPECIAL SHORTCUT   :  RETURN, DOUBLE-RETURN, DELETE ...

bind $::txt <Control-Return>	{
	if { [ $::txt tag ranges sel ] eq "" } { set ::status " Select a text !"
	} else { RemoveSymbolAndTag sel.first sel.last;}
	break;
}
bind $::txt <Double-space>	{ }
bind $::txt <Return>	{
	#--  Reset Status entry
	set ::status ""
	.fr.tb.entry configure -background white -show ""
	set RowNumber [ GetRowNumber ]
	set Text [$::txt get "insert linestart" "insert" ]
	#--   Reformat text for markdown files or Code
	if { [ string toupper $::FileExtension ] eq ".MD" } {
		#--  Read Tags : If in codeblock, nothing to do
		set Tags [ $::txt tag names [$::txt index "insert linestart"] ]
		if { [ string match "*CodeBlock*" $Tags ] } { return }
		#--  Replace the line(keep markdown symbols) / Reformat in markdown
		$::txt replace "insert linestart" "insert"  $Text 
		catch { ParseLine $RowNumber $Text }
		#--  Add Tags to next line if inside Indent1/2/3 block, Table, BlockQuote
		set ::status " Double-return to exit the block"
		switch -glob  [ $::txt tag names [$::txt index "insert-1c" ] ] {
      	*indent1*  { $::txt insert insert "\n\t" indent1; $::txt tag add indent1 "insert"; break }
      	*indent2*  { $::txt insert insert "\n\t\t" indent2; $::txt tag add indent2 "insert"; break }
      	*indent3*  { $::txt insert insert "\n\t\t\t" indent3; $::txt tag add indent3 "insert"; break }
      	*BlockQuote*  { $::txt insert insert "\n> " BlockQuote; ; break }
      	*Table*    {; #-- Remove tag 'syntax' to delimiter row. Add table tag to next line
								$::txt tag remove syntax "insert linestart" "insert lineend"
								$::txt insert insert "\n" Table
							 	$::txt tag remove syntax "insert"
								break }
			*details* { set ::status " Use Control-r to reformat details" }
			default  { set ::status "" }
		}
	} else {
		#--  Syntax highlighting depending of Code Language
		RemoveAllTag  "insert linestart" "insert"
		FormatCodeBlock $::CodeLanguage  $RowNumber  $Text
	}
}


#--  Double-Return   (if whithin a list/paragraph, delete/remove tags/line)
bind $::txt <Double-Return>	{
	#puts "DebugDoubleReturn : [ $::txt tag names [$::txt index "insert-1c" ] ]"
	switch -glob  [ $::txt tag names [$::txt index "insert-1c" ] ] {
		*indent*	{ 	foreach TagI { indent1 indent2 indent3 } {
							$::txt tag remove $TagI "insert" }
					}
		*Table*	{ 	$::txt tag remove Table "insert"  }
		*BlockQuote*	{ 	$::txt tag remove BlockQuote "insert";
								$::txt delete "insert linestart"  "insert lineend" }
		DefinitionList {  $::txt tag remove DefinitionList "insert" }
		default	{ ; #ReformatAll [$::txt get 1.0 "end-1c"] }
	}
	set ::status ""
}

bind $::txt <Delete>	{ }

bind $::txt <KeyPress> {
	#break
}

bind $::txt <KeyRelease> {
	#--  Real-time Code highlighting (Project Mode/no markdown files)
	#  Problem if tag 'trailing space' is defined and set with 'elide'= true
	#   Be sure this tag is either delete or set to  'elide' = false
  	catch {
		if { [string toupper [file extension $::FileName]] ne ".MD" \
				&& [wm title . ] ne "Untitled"  && $::projectFile ne "" } {
			set Text [$::txt get "insert linestart" "insert lineend" ]
  			#puts ":KeyRelease: Syntax $::CodeLanguage [ GetRowNumber ] $Text"
			RemoveAllTag "insert linestart" "insert lineend"
			FormatCodeBlock $::CodeLanguage  [ GetRowNumber ] $Text
		}
	}
}

bind $::txt <space> {
}

#===   EVENT FOR NOTEBOOK
#bind $hpane.nb <<NotebookTabChanged>> { puts "Tab changed: [$hpane.nb select]" }



#===   TOGGLE WORD WRAP
proc Toggle_Wordwrap {} {
	if { $::wordWrap == "word" } {
		set ::wordWrap "none"; .fr.tb.buttonWrap config -relief flat
	   #pack $::hpane.scrollX -side bottom -fill x -expand false;  # Restore text scroll>
	} else {
		set ::wordWrap "word"; .fr.tb.buttonWrap config -relief solid
		#pack forget $::hpane.scrollX;       # Hide Text scroll x bar
	}
   $::txt configure -wrap $::wordWrap;
	set ::status " Line Wrap: $::wordWrap";
}


#===   IDENTIFY WHICH KEY IS PRESSED. USED FOR DEBUG
proc Identify_Key_Pressed {} {
	set ::status " Press Any Key ..."
	bind $::txt <Key> {
		if { "%K" ne "Escape"} {set ::status " Key Pressed : %K"; break
		} else { bind $::txt <Key> "" }
	}
}


#===   INSERT/FORMAT  CODE BLOCK
proc InsertCodeBlock {} {
	#--  Insert a code block if no text selected
	if { [$::txt tag ranges sel] eq "" } {
		$::txt insert insert "\n" normal "```" "syntax" "\n\n" CodeBlock  "```" "syntax" "\n"
		$::txt mark set insert "insert-2l linestart"
		ViewSyntax
	}
	#-- If at least one line selected ==> CodeBlock, else CodeInline
	if { [$::txt tag ranges sel] ne "" } {
		set start [$::txt index sel.first];  set end [$::txt index sel.last]
		if { [ expr $end-$start ] >= 1 } {
			RemoveAllTag $start $end
			$::txt tag add CodeBlock $start $end
			$::txt insert $start-1l "```" "syntax"
			$::txt insert $end "```" "syntax" "\n"
			ViewSyntax
		} else {
			ReplaceTag "Word" "CodeInline" "";
		}
	}
}


#===   EXECUTE AT START
UnbindKey

