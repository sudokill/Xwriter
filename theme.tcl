#  relief values : flat, solid, raised, sunken, ridge, groove
#  Font values : TextFont, TextFontBold, TextFontItalic, MonospaceFont, TextFontBig, TextFontSmall

#===   THEME  DEFAULT  (light)
proc SetTheme_Default {} {
   $::hpane.fr configure  -bg {*}$::cfg(THEME.FrameBackground) ;		# Background color if page view=portrait
	ttk::style configure TNotebook -bg {*}$::cfg(THEME.FrameBackground)
	$::txt configure  -fg {*}$::cfg(THEME.Foreground) -bg {*}$::cfg(THEME.Background)
	$::txt tag configure sel    -background "#FFEFD5";  		# Selection
	$::txt tag configure search    -background  "#FFEFD5";
	$::txt tag configure syntax    {*}$::cfg(THEME.Syntax)
	$::txt tag configure bold    -font TextFontBold  -foreground black
	$::txt tag configure italics    -font TextFontItalic   -foreground black
	$::txt tag configure highlight		{*}$::cfg(THEME.Highlight)
	$::txt tag configure superscript    -offset 11  -font TextFontSmall
	$::txt tag configure subscript    -offset -9 -font TextFontSmall
	$::txt tag configure footnote    -offset 11  -font TextFontSmall  -foreground "#1168cc"
	$::txt tag configure heading1	 {*}$::cfg(THEME.Heading1)  -font FontHeading1
	$::txt tag configure heading2	 {*}$::cfg(THEME.Heading2) -font FontHeading2
	$::txt tag configure heading3	 {*}$::cfg(THEME.Heading3) -font FontHeading3
	$::txt tag configure heading4	 {*}$::cfg(THEME.Heading4)  -font FontHeading4
	$::txt tag configure heading5	 {*}$::cfg(THEME.Heading5) -font FontHeading5
	$::txt tag configure heading6	 {*}$::cfg(THEME.Heading6)  -font FontHeading6
	$::txt tag configure title		{*}$::cfg(THEME.Title) -font TextFontTitle
	$::txt tag configure listBullet	-foreground black  -font TextFontBold
	$::txt tag configure URL			{*}$::cfg(THEME.URL) 
	$::txt tag configure CodeInline	{*}$::cfg(THEME.CodeInline) 
	$::txt tag configure CodeBlock   {*}$::cfg(THEME.CodeBlock) -relief solid -borderwidth 1 -font MonospaceFont
	$::txt tag configure BlockQuote   {*}$::cfg(THEME.BlockQuote)  
	$::txt tag configure BlockQuote2  {*}$::cfg(THEME.BlockQuote)
   $::txt tag configure BlockHTML    -relief ridge -borderwidth 1  -background "#f4f4f4"
	$::txt tag configure TableHeader	{*}$::cfg(THEME.TableHeader) -relief sunken -borderwidth 1 -font MonospaceFont
	$::txt tag configure Table			-font MonospaceFont
	$::txt tag configure HTMLalias		-foreground grey;  		# curly brace in "text"{HTMLalias}
	$::txt tag configure Task			-font TextFontBold ; #-font TextFontMonospaceBold
	$::txt tag configure Comment		{*}$::cfg(THEME.Comment)
	$::txt tag configure details		{*}$::cfg(THEME.Details)
	$::txt tag configure TrailingSpace	-bgstipple gray12  -background grey40; # gray12,25,50,75
	$::txt tag configure DefinitionListTerm		-font TextFontItalicBold -foreground black
	$::txt tag configure DefinitionList			-lmargin2 [expr 1*$::TabWidth] 
	$::txt tag configure ModificationAdded	-foreground green -background "#fdfef1" 
	$::txt tag configure ModificationDeleted	-foreground brown -background "#fdfef1" 
	#$::txt tag configure border 	-bgstipple "@$::ScriptPath/img/gray50.xbm" -foreground grey70

	#--   TREEVIEW (Left Pane.  See plugin: PaneView.tcl)
	ttk::style configure Treeview	-foreground {*}$::cfg(THEME.Foreground) -background {*}$::cfg(THEME.Background) -fieldbackground {*}$::cfg(THEME.Background)
	$::tree tag configure BookmarkCategory  -font DefaultFontBold -foreground brown
	#ttk::style map Treeview.Heading -foreground [list active black !active black] -background [list active grey90 !active grey90]
	#$::tree.tb.b1 configure -foreground black -background  #f6f8fa  -highlightbackground grey90
	#$::tree.tb.b2 configure -foreground black -background  #f6f8fa  -highlightbackground grey90
	#$::tree.tb.b3 configure -foreground black -background  #f6f8fa  -highlightbackground grey90

	#--   CODE BLOCK  ( see below )
	SetTheme_GenericCode

	#--  TEXT WIDGET  CONFIG
  	$::txt configure	-font TextFont  -insertbackground black -highlightcolor black

	#--  THEME  GEOMETRY  ( see below )
	SetTheme_Geometry
}


#===    THEME   DARK
proc SetTheme_Dark {} {
	SetTheme_Default;							# Load Default Theme before
   $::hpane.fr  configure -bg black;  	# Background color if page view=portrait
	ttk::style configure TNotebook -background black
	$::txt configure								-font TextFont  -insertbackground white -highlightcolor grey40
	$::txt configure								-foreground grey80  -background  "#101010"
	$::txt tag configure syntax					-foreground magenta
	foreach it { sel  search }					{$::txt tag configure $it -background "#888033" }
	$::txt tag configure highlight				-foreground coral  -background grey15
	foreach it { BlockQuote BlockQuote2 }	{  $::txt tag configure $it -foreground grey60 }
	$::txt tag configure URL						-foreground "#00BBBB"
	$::txt tag configure footnote				-foreground "#00BBBB"
	$::txt tag configure TableHeader			-background grey40 -foreground white;
	$::txt tag configure TrailingSpace			-bgstipple gray12  -background grey50; # 12,25,50,75
	$::txt tag configure title						-foreground "#B6B600"
 	$::txt tag configure bold						-foreground "#DDDDDD"
	$::txt tag configure italics					-foreground  white
	$::txt tag configure heading1				-foreground "#B6B600"
	$::txt tag configure heading2				-foreground "#EB9000"
	$::txt tag configure heading3				-foreground "#DDDDDD"
	foreach it { heading4 heading5 heading6}	{$::txt tag configure $it -foreground "#DDDDDD"  }
 	$::txt tag configure details	-font TextFont -foreground "#DDDDDD"   -background  "#101010" -underline 0
 	$::txt tag configure listBullet				-foreground "#EEEEEE"
	$::txt tag configure CodeInline				-foreground "#00AA00" -background grey0
	$::txt tag configure CodeBlock				-foreground grey80 -background grey0  -relief sunken -borderwidth 1
	$::txt tag configure DefinitionListTerm		-font TextFontItalicBold -foreground "#DDDDDD"
	$::txt tag configure BlockHTML				-relief ridge -borderwidth 1 -background grey10
	$::txt tag configure ModificationAdded	-foreground "#00CC00" -background  grey10 
	$::txt tag configure ModificationDeleted	-foreground "#FC54FC" -background  grey10 

	#--  Theme for code
	SetTheme_GenericCode_Dark;

	#-- Change Treeview style (Left Pane)
	ttk::style configure Treeview -background black -fieldbackground black -foreground "#00BBBB"
	$::tree tag configure BookmarkCategory	-foreground grey70
	#ttk::style map Treeview.Heading -foreground [list active black !active white] -background [list active grey90 !active black]
	#$::tree.tb.b1 configure -foreground black -background  grey90  -highlightbackground grey
	#$::tree.tb.b2 configure -foreground black -background  grey90  -highlightbackground grey
	#$::tree.tb.b3 configure -foreground black -background  grey90  -highlightbackground grey
}


#===  THEME   TERMINAL
proc SetTheme_Terminal {} {
	SetTheme_Dark
	$::txt configure	-foreground grey80	-background  black
	$::txt configure -font MonospaceFont	-padx 5 -pady 5 -spacing1 0 -spacing2 0 -spacing3 0
	foreach it { sel search }	{$::txt tag configure $it -background "#686013"  }
	#--  In Terminal theme, tag 'details' is used to expand/collapse function block
	$::txt tag configure details -foreground {} -background {grey18} -font {} -underline 0
}



#===   THEME  LIGHT for  CODE
proc SetTheme_GenericCode {} {
	#   See above 'CodeBlock'  for background color definition inside block of code
	#  		Code_SubCommand;    #   echo , sh ... or -foo --foo
	#  		Code_Command;       #   if while ....
	#  		Code_Function;      #  [ ] { } proc func def  #ifdef
	#  		Code_Variable;      #   $foo   ${foo} ....
	#  		Code_SpecialChar;   #   ( ) ; ` \ < > ! = & |
	#  		Code_String;        #   "foo"  'foo'
	#  		Code_Comment;      #   # foo  /* foo */
	$::txt tag configure Code_Comment		-foreground #8B4513
	$::txt tag configure Code_Function		-foreground "#0000FF"
	$::txt tag configure Code_Variable		-foreground "#008800"
	$::txt tag configure Code_String			-foreground "#AA0000"
	$::txt tag configure Code_SpecialChar		-foreground brown
	$::txt tag configure Code_Command			-foreground "#0000CC"
	$::txt tag configure Code_SubCommand		-foreground magenta
}



#===   THEME  DARK  for  CODE
proc SetTheme_GenericCode_Dark {} {
	$::txt tag configure Code_Comment		-foreground "#00AAAA";  # Cyan
	$::txt tag configure Code_Function      		-foreground "#00FFFF";  # Cyan light
	$::txt tag configure Code_Variable		-foreground "#FC54FC";  # purple
	$::txt tag configure Code_String			-foreground "#DDDD00";  # yellow  
	$::txt tag configure Code_SpecialChar		-foreground "#00DD00";  # green
	$::txt tag configure Code_Command		-foreground "#00CC00";  # green
	$::txt tag configure Code_SubCommand		-foreground lightgreen;	# lightgreen or blue #0087D7
}



#===  THEME GEOMETRY  ( COMMON TO ALL THEME )
proc SetTheme_Geometry {} {

	#  spacing1 :  Extra space to leave above a line
	#  spacing2 :  If text line wraps, space to leave between the display lines that make up the text line
	#  spacing3 :  Extra space to leave below the line
	#  lmargin1 :  Left-margin for the first wrapped line
	#  lmargin2 :  Left-margin except first wrapped line

	$::txt configure					-padx 30 -pady 30 -spacing1 $::cfg(GEOMETRY.LineHeight)   -spacing2 $::cfg(GEOMETRY.LineHeight)
	$::txt tag configure indent1		-lmargin2 [expr 1*$::TabWidth]
	$::txt tag configure indent2		-lmargin2 [expr 2*$::TabWidth]
	$::txt tag configure indent3		-lmargin2 [expr 3*$::TabWidth]
	$::txt tag configure list0			-lmargin2 [expr 0*$::TabWidth+2*$::SpaceWidth]  -spacing1 [expr 2*$::cfg(GEOMETRY.LineHeight)] ;  # 0 Tabs
	$::txt tag configure list1			-lmargin2 [expr 1*$::TabWidth+2*$::SpaceWidth]  -spacing1 [expr 2*$::cfg(GEOMETRY.LineHeight)];  # 1 Tabs
	$::txt tag configure list2			-lmargin2 [expr 2*$::TabWidth+2*$::SpaceWidth]  -spacing1 [expr 2*$::cfg(GEOMETRY.LineHeight)];  # 2 Tabs
	$::txt tag configure list3			-lmargin2 [expr 3*$::TabWidth+2*$::SpaceWidth]  -spacing1 [expr 2*$::cfg(GEOMETRY.LineHeight)];  # 3 Tabs
	$::txt tag configure listNum0		-lmargin2 [expr 0*$::TabWidth+2*$::SpaceWidth]  -spacing1 [expr 2*$::cfg(GEOMETRY.LineHeight)];  # 0 Tabs
	$::txt tag configure listNum1		-lmargin2 [expr 1*$::TabWidth+2*$::SpaceWidth]  -spacing1 [expr 2*$::cfg(GEOMETRY.LineHeight)];  # 1 Tabs
	$::txt tag configure listNum2		-lmargin2 [expr 2*$::TabWidth+2*$::SpaceWidth]  -spacing1 [expr 2*$::cfg(GEOMETRY.LineHeight)] ;  # 2 Tabs
	$::txt tag configure listNum3		-lmargin2 [expr 3*$::TabWidth+2*$::SpaceWidth]  -spacing1 [expr 2*$::cfg(GEOMETRY.LineHeight)] ;  # 3 Tabs
	$::txt tag configure CodeBlock		-spacing1 0 -spacing2 0 -spacing3 0 -lmargin1 1c -lmargin2 1c
	$::txt tag configure BlockQuote	-lmargin1 [expr 1*$::TabWidth]  -lmargin2  [expr 1*$::TabWidth] 
	$::txt tag configure BlockQuote2	-lmargin1 [expr 2*$::TabWidth]   -lmargin2 [expr 2*$::TabWidth]
}
