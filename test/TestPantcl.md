
# Test PANTCL pandoc filters  


***⚠  Don't forget to enable filters with  :  Toolbar > ▣ > Enable Code Evaluation and make the file plugin/pantcl.tapp executable***

May fail if web service 'kroki' is unavailable

1. Check executable with  './pantcl.tapp --version'
Works only when you use Menu > 'print to browser' or export to another format
If you use 'kroki' filter, images are saved in Home Folder/Images
Usage :    echo=true :  Display code,   eval=true :  Display code evaluation 




##  Test Filter tcl

```tcl
# just syntax highlighting / NO code evaluation
puts [ expr (10+3) ]
```

```{.tcl}
#  NO code evaluation if no 'eval=true' in description 
puts [ expr (10+3) ]
```

```{.tcl  eval=true}
#  Code Evaluation with eval=true
puts [ expr (10+3) ]
```


Test Inline conversion (V0.9.13?) :   `tcl clock format [clock seconds] -format "%Y-%m-%d %H:%M:%S"` CET.


## Test Filter SQLITE

```{.sqlite   results="asis"  echo=false  eval=true}
/* Create/Display a sqlite database */
CREATE TABLE contacts (
    first_name TEXT NOT NULL,
    email TEXT NOT NULL UNIQUE,
    phone TEXT NOT NULL UNIQUE
        );
INSERT INTO contacts (first_name, email, phone)
       VALUES       ("Max", "musterm@mail.de","1234");
INSERT INTO contacts (first_name, email, phone)
       VALUES       ("Musterwoman", "musterw@mail.de","1235");
SELECT * from contacts;
```

```{.sqlite  results=asis  eval=true  echo=false   file="/home/myself/.bin/Xwriter/test/obj/test.sqlite" }
/*  List available tables */
/*.tables */
/* Display the first 3 item in table 'Course' */
SELECT * from  Course  LIMIT 3;
/* Display the 2 first items in table 'Student' */
SELECT * from  Student LIMIT 2;
```


##  Test filter CMD

```{.cmd  echo=false  eval=true  file="temp1.sh"  }
echo === Display a calendar now ===;
cal
```

```{.cmd  echo=false  eval=true  file="temp2.py" }
#!/usr/bin/env python3
import sys
print ("Hello PYTHON World")
print (sys.version)
```

```{.cmd   echo=false eval=true  file="temp3.c"   }
///usr/bin/cc -o "${0%.c}" "$0" -lm && exec "${0%.c}"
#include <stdio.h>
#include <math.h>
int main (int argc, char** argv) {
    printf("Hello C World\n");
    float pi = 3.141492653;
    printf("pi is: %f\n",pi);
    printf("sin(pi) is: %f\n",sin(pi));
    return(0);
}
```

```{.cmd   echo=false eval=true  file="temp4.cpp"  }
///usr/bin/g++ -o "${0%.cpp}" "$0" && exec "${0%.cpp}"
#include <iostream> 
int main(int argc, char** argv) {
  using namespace std;
  int ret = 0;
  cout << "Hello C++ World" << endl;
  return ret;
} 
```

```{.cmd   echo=false  eval=true  file="temp5.v"}
///usr/local/bin/v run $0 $@  2>&1 && exit 0
println("Hello V World!")
```

```{.cmd  echo=false  eval=true  file="temp6.go"}
//usr/bin/go run $0 $@  2>&1 && exit 0
package main
func main() {
    println("Hello GO World")
}
```

```{.cmd   file="hello-rust.rs"  eval=true  echo=false }
///usr/bin/rustc $0 $@  -o ${0%.rs} 2>&1 && exec "${0%.rs}" && exit 0
fn main () {
  println!("Hello RUST World")
}
```

```{.cmd   file="factorial.lua"   eval=true  echo=false}
#!/usr/bin/env lua
-- defines a factorial function
function fact (n)
    if n == 0 then
        return 1
    else
        return n * fact(n-1)
    end
end
print(fact(5))
print("Hello LUA  World!")
```


##  Test  Graphic DOT

```dot
graph G {
	#  NO code evaluation, just syntax highlighting
  node [shape=box,style=filled,fillcolor=skyblue,
              color=black,width=0.4,height=0.4];
          n0 -- n1 -- n2 -- n3 -- n0;
}
```


```{.dot  label=neato-sample app=neato  echo=false eval=true}
graph G {
  node [shape=box,style=filled,fillcolor=skyblue,
              color=black,width=0.4,height=0.4];
          n0 -- n1 -- n2 -- n3 -- n0;
}
```

```{.dot label=digraph echo=false  eval=true }
digraph G {
       main -> parse -> execute;
       main -> init [dir=none];
       main -> cleanup;
       execute -> make_string;
       execute -> printf
       init -> make_string;
       main -> printf;
       execute -> compare;
}
```

##  Test Filter Kroki

```{.kroki label=digraph eval=true echo=false}
digraph G {
       main -> parse -> execute;
       main -> init [dir=none];
       main -> cleanup;
       execute -> make_string;
       execute -> printf
       init -> make_string;
       main -> printf;
       execute -> compare;
}
```

```{.kroki label=erd dia=erd eval=true  echo=false }
[Person]
*name
height
weight
+birth_location_id

[Location]
*id
city
state
country
Person *--1 Location
```

```{.kroki   dia=mermaid  eval=true  echo=false }
graph TD;
A-->B;
A-->C;
B-->D;
C-->D;
```

```{.kroki   dia=mermaid   eval=true  echo=true }
gantt
    title A Gantt Diagram
    dateFormat  YYYY-MM-DD
    section Section
    A task           :a1, 2014-01-01, 30d
    Another task     :after a1, 20d
    section Another
    Task in sec      :2014-01-12, 12d
    another task     :24d
```

```{.kroki   dia=mermaid   eval=true  echo=true }
pie title Pets adopted by volunteers
    "Dogs": 386
    "Cats": 85
    "Rats": 15
```

```{.kroki   dia=blockdiag  eval=true  echo=false }
blockdiag {
  blockdiag -> generates -> "block-diagrams";
  blockdiag -> is -> "very easy!";

  blockdiag [color = "greenyellow"];
  "block-diagrams" [color = "pink"];
  "very easy!" [color = "orange"];
}
```

```{.kroki   dia=plantuml  eval=true  echo=false }
@startwbs
skinparam monochrome true
* Business Process Modelling WBS
** Launch the project
*** Complete Stakeholder Research
*** Initial Implementation Plan
** Design phase
*** Model of AsIs Processes Completed
**** Model of AsIs Processes Completed1
**** Model of AsIs Processes Completed2
*** Measure AsIs performance metrics
*** Identify Quick Wins
** Complete innovate phase
@endwbs
```


##  Test PIKCHR

```pikchr
oval "Start" fit; arrow; box "Hello, World!" fit; arrow; oval "Done" fit
```

