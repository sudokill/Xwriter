---
title: Test Include files
subtitle: 
author: 
date: 
subject: 
description: 
keywords: 
---


#  Test Include files  


1.  Test ` Menu > INS > Include files ` with QuickTest.md now :


Check the syntax : {\{include [QuickTest.md](xxx/test/QuickTest.md) }\}

2.  Test Export HTML with the following block :

```include
# Insert list of files to include. Absolute/Relative path allowed
./TestArchive.md
./TestAlias.md
```

3.  Print to browser (crtl-p)

#  Test Include File OK ?  



