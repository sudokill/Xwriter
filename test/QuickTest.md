---
title:                   Metadata_Title
subtitle:            Metadata_Subtitle
author:             Metatada_Author
date:                 03 Feb 2023
subject:            Metadata_Subject
description:    Metadata_Description
keywords:       Metadata_Keywords
---

# Quick Test  

// _Markdock syntax to edit/share 'rich-text' document_


# Header1
## Header2
### Header3
#### Header4
#####  Header5
######  Header6

 Heading 1 Setext-style  
==================  

 Heading 2  Setext-style  
---------

A line with lot of spaces:    and a tab:	and a special tab: 	now  

**bold**   _Italic_   *italic*    __Underline__    ***BoldItalic***      ___BoldUnderline___  
~~Strikeout~~     <mark>Highlight</mark>      H<sup>2</sup>O<sub>3</sub>    ∞‰▪▸•○◇♦◦➢➤⇒√≤≥≠≈±  

A line with <big> __big__  font  size with **bold** and _italic_ </big> now  
A line in normal font size  
A line with <small> __small__  font  size with **bold** and _italic_  </small> now  

This is      `Code **Inline**`   now  

http://google.com    <http://google.com>     [URLlink](http://google.com)  
[File (Absolute Path)](/etc/apt/sources.list)  	   [File (Relative Path)](obj/test.md)  

a footnote reference[^1]  

A page Break Now :  

****

- List line1
a new line  inside 
- A new     line with    lot of spaces
	- hgfhsgfd
	- gfqfdgqf d

Horizontal Rule :  

----


|	Paragraph **Indent1**
	A new line with a spacial tab: 	now very long q usd fqhgdhqgfs dgqfhsdgqf qsdqsd hqgfsdhqhgfs dhgfqhgfsdhqg sdfhqfd  

|		Paragraph **Indent2**
		A new line with a special tab: 	now q hsdgfq sdhgqhfsdhgq ghs dhgqf hdf hqgf1 hfdhqgfsh dgfhqg fshdgfqh df  

|			Paragraph **Indent3**
			A new line with a special tab: 	now qjs gdqs djhgjqhgjs dhg qjhd hgqjd gqjhgsd jhgqjhsg djhqgsjdh gjqhgsd  

/ **Fully-Justify** line  dgqgfd hqgfsgzd hbvx hxgfqhqgfd yt h qhsfd hgfqhsgfd hgfq stxrytq svc hqfdyt hgfq shfd qtytr yt yt yts ytdr trqysddf qhsd fgqfs hqgfs hd hqgfshdgf hqgfhsd hqgfs d hqgfs hgqfs hdfghqgfshdf hqfgs d  qshfd hqhgf hqgfs hdfghq hgqf shgfdqhgfsd h hgqfhsf dhgfqh fsd hgfqhgfs hg hgf dh qshfdtry ytryt gfq sd

```tcl
#-- Code block in monospace, syntax highlighting
puts "**Xwriter**";   set $var_s;   set ${var_x}
```

// Center Image ![Name quick;100px](test1.jpg) with **custom zoom**

/// 					__Justify right and underline__


|  Header1       |   Header2          |   Header3          
|----------------|:------------------:|-------------------:|  
| Left1          | Center1            | Right1
| Left2          | Center2            | Right2
|================|====================|====================  


A comment now :  <!--  A comment now  -->  

>  A block quote level1
> A new     line with    lot of spaces
>  
>> A block quote level2
>> A new     line with    lot of spaces

- [ ]   Task ToDo  
- [x]   Task Done  

Definition List1  
:	The first line begin with : + tab, and it can be very long to test the definition list. Without new line, the left margin of this line should be equal to 1 tab.  
	A new line inside the definition list begins with tab  

Definition List2  
:	A new line  

HTML alias in "red bold"{#red bold} and "blue italic"{#blue italic} now  

:> Details Summary  
Text Details1  
Text **Details2**  

Badge img.shields  Active  : ![Active](https://img.shields.io/badge/Active-brightgreen.svg)   
Badge img.shields  Unknow  : ![Unknown](https://img.shields.io/badge/Unknown-grey.svg)   
Badge img.shields  Any Text : ![AnyText](https://img.shields.io/badge/any_text-you_like-blue.svg)   
TCL built-in command for Date/Time :  `tcl clock format [clock seconds]  -format "%Y-%m-%d  %H:%M:%S" `  

<kbd> keyboard </kbd>  

[^1]: the footnote description  



