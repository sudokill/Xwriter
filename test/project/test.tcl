
proc tkconInit {args} {
    variable VERSION
    set tcl_interactive 1
    set argc [llength $args]

    if {$PRIV(WWW)} {
	lappend PRIV(slavealias) history
	set OPT(prompt1) {[history nextid] % }
    } else {
	lappend PRIV(slaveprocs) tcl_unknown unknown
	set OPT(prompt1) {([file tail [pwd]]) [history nextid] % }
    }
}


proc ::tkcon::GarbageCollect {} {
    variable OPT
    variable PRIV

	if {[winfo exists $w]} {
	    foreach tag [$w tag names] {
	    }
	}
}
