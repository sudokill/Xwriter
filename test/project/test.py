#!/usr/bin/env python3

"""
Begin Test
Python
"""


#  IMPORTANT: Folding works only for function ending with double empty-line 

#  Test folding ( Double empty lines ), no indentation
def test1():
  """Add the record of new expense from user to users sheet"""
  return line


async def test2(self):
	return test2


def test3(line):
  # string comparisons
  line = re.sub(r' eq +"', r' == "', line)

  # complete expression is bareword
  if re.match(r"^[a-zA-Z_][a-zA-Z0-9_]*$", line):
    return '"' + line + '"'

  # empty list OR string
  if line == "{}": line = "None"


#  Test Folding ( with indentation )
   def test3(params):
     orig_params = params
     ok = True

     parl = []




'''
End of test
python
'''

