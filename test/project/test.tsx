
function test1({ user }: { user: User }) {
	const t = useLocale()

	async function handleSubmit({ username, password}: UserFormData) {
		if (isLoading) return
		if (password !== passwordConfirm) {
			setIsLoading(false)
			setError(t("user-edit--password-mismatch"))
			return
		}
	}

	return (
		<Container>
				admin={admin}
		</Container>
	)

}

export default function UserEdit() {
	const t = useLocale()
	if (error) {
		console.error(error)
	}
	return <UserEditComponent user={user} />
}
