
test1() {
  (( $# == 0 )) && batslib_err || batslib_err "$@"
  return 1
}


test2 ( blabla ) {
  { if (( $# > 0 )); then
      echo "$@"
    else
      cat -
    fi
  } >&2
}

test3() {
	local -i n_lines=0
	local line
	while IFS='' read -r line || [[ -n $line ]]; do
		(( ++n_lines ))
	done < <(printf '%s' "$1")
	test4() {
			for string in "$@"; do
			(( $(batslib_count_lines "$string") > 1 )) && return 1
			done
	}
	echo "$n_lines"
}


