

public class IqGenerator extends AbstractGenerator {

// TEST FUNCTION PUBLIC, PROTECTED, PRIVATE

    public IqGenerator(final XmppConnectionService service) {
        super(service);
    }

    public IqPacket purgeOfflineMessages() {
        final IqPacket packet = new IqPacket(IqPacket.TYPE.SET);
        packet.addChild("offline", Namespace.FLEXIBLE_OFFLINE_MESSAGE_RETRIEVAL).addChild("purge");
        return packet;
    }

    public static Bundle defaultGroupChatConfiguration() {
        Bundle options = new Bundle();
        options.putString("muc#roomconfig_persistentroom", "1");
        return options;
    }

    protected IqPacket publish(final String node, final Element item, final Bundle options) {
        final IqPacket packet = new IqPacket(IqPacket.TYPE.SET);
        final Element pubsub = packet.addChild("pubsub", Namespace.PUBSUB);
         return packet;
    }

    private IqPacket retrieve(String node, Element item) {
        final IqPacket packet = new IqPacket(IqPacket.TYPE.GET);
        final Element pubsub = packet.addChild("pubsub", Namespace.PUBSUB);
    }

// TEST FOLDABLE FUNCTION IN FUNCTION WITH INDENTATION

	private void attachFileToConversation(Conversation conversation, Uri uri, String type) {
        activity.xmppConnectionService.attachFileToConversation(
                conversation,
                uri,
                type,
                new UiInformableCallback<Message>() {
                    @Override
                    public void inform(final String text) {
                        hidePrepareFileToast(prepareFileToast);
                        runOnUiThread(() -> activity.replaceToast(text));
                    }

                    @Override
                    public void success(Message message) {
                        runOnUiThread(() -> activity.hideToast());
                        hidePrepareFileToast(prepareFileToast);
                    }

                    @Override
                    public void error(final int errorCode, Message message) {
                        hidePrepareFileToast(prepareFileToast);
                        runOnUiThread(() -> activity.replaceToast(getString(errorCode)));
                    }

                    @Override
                    public void userInputRequired(PendingIntent pi, Message message) {
                        hidePrepareFileToast(prepareFileToast);
                    }
                });
	}




}
