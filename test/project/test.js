


// TEST FOLDABLE FUNCTION WITH NO INDENTATION
function test_nospace($parent, monitors, hls) {
	let selectedMonitors = [];
	const isMonitorSelected = (monitor) => {
		if (selectedMonitors.length === 0) {
			return true;
		}
		return false;
	};
}

function test_space ($parent, monitors, hls) {
	let selectedMonitors = [];
	const isMonitorSelected = (monitor) => {
		if (selectedMonitors.length === 0) {
			return true;
		}
		return false;
	};
}

function test_noarguments () {
	let selectedMonitors = [];
	const isMonitorSelected = (monitor) => {
		if (selectedMonitors.length === 0) {
			return true;
		}
		return false;
	};
}


// TEST FOLDABLE FUNCTION IN FUNCTION WITH INDENTATION
function level0() {
	const getRes = () => {
		const saved = localStorage.getItem("preferLowRes");
		if (saved) {
			return saved === "true";
		}
		return preferLowResByDefault;
	};
	function level1() {
		// Globals.
		const $contentGrid = document.querySelector("#content-grid");
		function level2() {
			// Globals.
			const $contentGrid = document.querySelector("#content-grid");
		}
		const viewer = newViewer($contentGrid, monitors, Hls);
	}
	return 0
}


//  ASYNC FUNCTION IN FUNCTION
async function sendAlert(msg, response) {
	alert(`${msg}: ${response.status}, ${await response.text()}`);

	async function sendAlert() {
		alert(`${msg}: ${response.status}, ${await response.text()}`);
	}
}


//  EXPORT FUNCTION
export function tflite(msg, response) {
	// @ts-ignore
	const monitorsInfo = MonitorsInfo; // eslint-disable-line no-undef
	export function tflite() {
		// @ts-ignore
		const monitorsInfo = MonitorsInfo; // eslint-disable-line no-undef
	}
}


// TEST HIGHLIGHTING
function test_highlighting($parent, monitors, hls) {
	import { sortByName } from "./libs/common.js";
	let selectedMonitors = [];
	const isMonitorSelected = (monitor) => {
		if (selectedMonitors.length === 0) {
			return true;
		}
		for (const id of selectedMonitors) {
		}
		return false;
	};
	return {
		reset() {
			for (const feed of feeds) {
				feed.destroy();
			}
			feeds = [];
				if (monitor["enable"] !== true) {
					continue;
				}
				const recordingsPath = toAbsolutePath("recordings");
			for (const feed of feeds) {
				html += feed.html;
			}
			$parent.innerHTML = html;
		},
	};
}


