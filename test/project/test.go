//   TEST GO


func test1() {
	AddImageHandler(
		func(r io.Reader) (Image, error) {
			return NewNativeImage(r)
		},
		"image/bmp",
		"image/gif",
		"image/x-icon",
		"image/jpeg",
		"image/png",
		"image/tiff",
		"image/webp",
	)
}


func test2(r io.Reader) (*NativeImage, error) {
	buf := new(bytes.Buffer)
	c, format, err := image.DecodeConfig(io.TeeReader(r, buf))
}
