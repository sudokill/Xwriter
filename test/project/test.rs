
// TEST FOLDABLE FUNCTION WITH NO INDENTATION
pub fn test1 () {
    token: CancellationToken,
    config: MonitorConfig,
}

pub fn test2 (&self) -> &MonitorConfig {
    token: CancellationToken,
    config: MonitorConfig,
}


// TEST FOLDABLE FUNCTION IN FUNCTION WITH INDENTATION
pub fn test3 () {
	token: CancellationToken,
	config: MonitorConfig,
	pub fn config(&self) -> &MonitorConfig {
        &self.config
			pub fn config(&self) -> &MonitorConfig {
        		&self.config
			}
	}
	config: MonitorConfig,
}



//  PUB/ASYNC FUNCTION
	pub async fn test4(&self) -> Result<Arc<Source>, Cancelled> {
        let (res_tx, res_rx) = oneshot::channel();
        tokio::select! {
            () = self.token.cancelled() => return Err(Cancelled),
            _ = self.source_main_tx.send(res_tx) => {}
        }
	}

	async fn test5(&self, config: MonitorConfig) -> Option<Arc<Monitor>> {
        let hooks = self.hooks.clone().expect("hooks to be set");

	}

