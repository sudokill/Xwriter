
# Test MATH  



**Note** : Enable Pandoc extension : +tex_math_dollars (Menu > Preferences)

Use syntax :  ` $$Code$$ `

##  Math Inline

1.  Einstein came up with :     $$E=m\text{c}^2$$
2.  This is Euler's formula :     $$e^{i \cdot pi} = -1$$
3.  Some examples :    $$\sum{x} \leq 2 \geq 3$$
4.  A fraction  :     $$\frac{1}{2}$$
5.  A square root :      $$a^2 + b^2 = c^2 + \sqrt(b^2-4ac)$$
  or   $$\sqrt{3x-1}+(1+x)^2$$
6.  A quadratic formula:     $$-b \pm \sqrt{b^2 - 4ac} \over 2a$$

Escape literal dollar signs to prevent math rendering: \$bling\$
$10,000 and $20,000 don't have to be escaped.

For more voluminous equations (such as     $$\sum{\frac{(\mu - \bar{x})^2}{n-1}}$$ ), some line spacing disruptions are unavoidable.  
Math should then be displayed in displayed mode.     $$\\sum{\frac{(\mu - \bar{x})^2}{n-1}}$$

**The Cauchy-Schwarz Inequality**
$$\left( \sum_{k=1}^n a_k b_k \right)^2 \leq \left( \sum_{k=1}^n a_k^2 \right) \left( \sum_{k=1}^n b_k^2 \right)$$


##  Math Block

Not suported
