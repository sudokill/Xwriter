
#   Test MERMAID  


1. Method 1 :  A code block as :    \`\`\`mermaid....\`\`\`
2. Method 2 :  HTML syntax  ` <pre class="mermaid">...</pre> `

**Note** :  Xwriter includes automatically the following code when exporting to HTML :
```
<script type=\"module\">
  	import mermaid from 'https://cdn.jsdelivr.net/npm/mermaid@10/dist/mermaid.esm.min.mjs';
</script>
```


##   Code Block syntax

```mermaid
---
title: graph example
---
graph TD;
      A-->B;
      A-->C;
      B-->D;
      C-->D;
```

##   HTML syntax

<pre class="mermaid">
  	graph TB;
      		A-->B;
      		A-->C;
      		B-->D;
      		C-->D;
</pre>

##   Flowchart

```mermaid
---
title: Flowchart example
---
flowchart LR
	%% Comments after double percent signs
	A[Rectangle shape] --Text--> B(Round shape)
	B --> C{Decision with <br/> line break}
	C -->|One| D([Stadium-shaped node])
	C -->|Two line<br/>edge comment| E((Circular<br/> shape))
	F>Odd Shape] --> B
	subgraph subA
		G{{hexagon shape}} --Test--> H
		I[[SubRoutine Shape]] -.-> J
		M[/Trapezoid shape\] --> J
		M(((double circle shape))) --> J
	end
	subgraph subB
		K[/ Parallelogram shape /] --> L
		L[\ Alt Parallelogram shape \]  --> K
	end
	%% THEME
	classDef green fill:#9f6,stroke:#333,stroke-width:2px;
	classDef orange fill:#f96,stroke:#333,stroke-width:4px;	
	class C,F green
	class E orange
```

##  Sequence Diagram

```mermaid
sequenceDiagram
	Alice->>John: Hello John, how are you?
	loop Healthcheck
    		John->>John: Fight against hypochondria
	end
	Note right of John: Rational thoughts!
	John-->>Alice: Great!
	John->>Bob: How about you?
	Bob-->>John: Jolly good!
```


##  Gantt Chart

```mermaid
gantt
    section Section
    Completed :done,    des1, 2014-01-06,2014-01-08
    Active        :active,  des2, 2014-01-07, 3d
    Parallel 1   :         des3, after des1, 1d
    Parallel 2   :         des4, after des1, 1d
    Parallel 3   :         des5, after des3, 1d
    Parallel 4   :         des6, after des4, 2d
```


##  Class Diagram


```mermaid
classDiagram
    note "From Duck till Zebra"
    Animal <|-- Duck
    note for Duck "can fly\ncan swim\ncan dive\ncan help in debugging"
    Animal <|-- Fish
    Animal <|-- Zebra
    Animal : +int age
    Animal : +String gender
    Animal: +isMammal()
    Animal: +mate()
    class Duck{
        +String beakColor
        +swim()
        +quack()
    }
    class Fish{
        -int sizeInFeet
        -canEat()
    }
    class Zebra{
        +bool is_wild
        +run()
    }
```


##  State Diagram

```mermaid
stateDiagram-v2
	[*] --> Still
	Still --> [*]
	Still --> Moving
	Moving --> Still
	Moving --> Crash
	Crash --> [*]
```


##  Pie Chart

```mermaid
pie  
	title What Voldemort doesn't have?
	"Dogs" : 386
	"Cats" : 85.9
	"Rats" : 15
```

##  Bar chart

```mermaid
gantt
    title Git Issues - days since last update
    dateFormat  X
    axisFormat %s

    section Issue19062
    71   : 0, 71
    section Issue19401
    36   : 0, 36
    section Issue193
    34   : 0, 34
    section Issue7441
    9    : 0, 9
    section Issue1300
    5    : 0, 5
```


##  User Journey  Diagram

```mermaid
 journey
    title My working day
    section Go to work
      Make tea: 5: Me
      Go upstairs: 3: Me
      Do work: 1: Me, Cat
    section Go home
      Go downstairs: 5: Me
      Sit down: 3: Me
```


##   C4 diagram

```mermaid
C4Context
	title System Context diagram for Internet Banking System

Person(customerA, "Banking Customer A", "A customer of the bank, with personal bank accounts.")
Person(customerB, "Banking Customer B")
Person_Ext(customerC, "Banking Customer C")
System(SystemAA, "Internet Banking System", "Allows customers to view information about their bank accounts, and make payments.")

Person(customerD, "Banking Customer D", "A customer of the bank, <br/> with personal bank accounts.")

Enterprise_Boundary(b1, "BankBoundary") {

  SystemDb_Ext(SystemE, "Mainframe Banking System", "Stores all of the core banking information about customers, accounts, transactions, etc.")

  System_Boundary(b2, "BankBoundary2") {
    System(SystemA, "Banking System A")
    System(SystemB, "Banking System B", "A system of the bank, with personal bank accounts.")
  }

  System_Ext(SystemC, "E-mail system", "The internal Microsoft Exchange e-mail system.")
  SystemDb(SystemD, "Banking System D Database", "A system of the bank, with personal bank accounts.")

  Boundary(b3, "BankBoundary3", "boundary") {
    SystemQueue(SystemF, "Banking System F Queue", "A system of the bank, with personal bank accounts.")
    SystemQueue_Ext(SystemG, "Banking System G Queue", "A system of the bank, with personal bank accounts.")
  }
}

BiRel(customerA, SystemAA, "Uses")
BiRel(SystemAA, SystemE, "Uses")
Rel(SystemAA, SystemC, "Sends e-mails", "SMTP")
Rel(SystemC, customerA, "Sends e-mails to")
```

