#!/usr/bin/wish
#		- Manage Tags (Init, Add, Replace, Remove tags)


#==  DEFINE MAIN TAG (center, underline...)
proc Set_TagMain {} {
	$::txt tag configure underline		-underline true
	$::txt tag configure monospace		-font MonospaceFont
	$::txt tag configure BigFont		-font TextFontBig
	$::txt tag configure BigFontBold	-font TextFontBigBold
	$::txt tag configure BoldItalics	-font  TextFontItalicBold
	$::txt tag configure SmallFont		-font TextFontSmall
	$::txt tag configure SmallFontBold	-font TextFontSmallBold
	$::txt tag configure overstrike	-overstrike true
	$::txt tag configure left			-justify left
	$::txt tag configure center			-justify center
	$::txt tag configure right			-justify right
}

#==   DEFINE TAG PRIORITY
proc Set_TagPriority {} {
	$::txt tag raise syntax;			# Tag 'syntax' is always higher in priority Vs other tags
	$::txt tag raise sel; 			# Tag 'sel' is always higher in priority Vs other tags
	$::txt tag lower italics; 		# Tag 'italics' is always lower in priority Vs other tags
	$::txt tag lower BlockQuote2; 	# Tag 'Blockquote' is always lower in priority Vs other tags
	$::txt tag lower BlockQuote;	# Tag 'Blockquote' is always lower in priority Vs other tags
	$::txt tag lower BlockHTML;
	$::txt tag lower TrailingSpace;
}


#===  REMOVE TAGS (Keep markdown symbol)
proc RemoveAllTag { start end }  {
	#-- Remove tags except Details (used to toogle text visibility)
	foreach tag [GetAllTags $start $end] {
		if { ![string match "Details*" $tag] && ![string match "details" $tag] } { 
			$::txt tag remove $tag $start $end 
		}
	}
	#puts ":TagRemove: [$::txt index $start]-[::txt index $end] : [GetAllTags $start $end]"

	#--  Old Method (deprecated)
	#set TextWithoutTag  [ $::txt get $start $end ]
	#$::txt replace $start $end  $TextWithoutTag
}


#===  REMOVE TAGS AND MARKDOWN SYMBOLS
proc RemoveSymbolAndTag { start end } {
	if { [$::txt tag ranges sel] eq "" }  { return }; # No selection, stop
   HideSyntax
	set TextWithoutSyntax  [ $::txt get -displaychars -- $start $end ]
	$::txt replace $start  $end  $TextWithoutSyntax
}


#==  GET ALL TAGS BETWEEN START,END RANGE
proc GetAllTags { start end } {
	# for each char between start,end, add tags for this char (start+$ic) to a list
	set ListTags ""
	set StringLength [ string length [$::txt get $start $end] ]
	for {set i 0} {$i < $StringLength} {incr i} {
		set CharTag [ $::txt tag names $start+[string cat $i c]  ]
		if { ![string match "*$CharTag*" $ListTags] } {
			append ListTags " $CharTag"
		}
	}
	regsub -all {sel} $ListTags {} ListTags; # Remove tag 'sel' from the tag list
	return $ListTags
}


#===  REPLACE TAGS/MARKDOWN SYMBOL ON TEXT SELECTION
proc ReplaceTag { TagType TagNew TagRemove }  {
	#   Used by shortcut. Example: ReplaceTag "Word" "italics" "bold"
	if { [ $::txt tag ranges sel ] eq "" } { set ::status " Select a text !"; return }

   #-- If several lines selected, abort
   if { [ expr [$::txt index sel.last]-[$::txt index sel.first] ] > 1 } {
		tk_messageBox -message " Format multiple lines not supported\n" -type ok -icon error; return
   }

	set ListTag [$::txt tag names sel.first];			# Return Selection Tags
	set line [$::txt get "sel.first linestart" "sel.first lineend"]; # line content
	regsub -all {sel} $ListTag {} ListTag;				# Remove Tag 'sel' to the list
	#puts "DebugTag  Type: $TagType  Add: $TagNew   Remove: $TagRemove"

	#--  Remove Symbols and Tag for line
   if { $TagType eq "Line" } {
      $::txt tag remove $TagRemove "sel.first linestart" "sel.last lineend"
		if { [regexp -indices {^(:|:>|▶|/{1,3}|#{1,6})(\s+)} $line s0] } {
			$::txt delete "sel.first linestart"  "sel.first linestart+[lindex $s0 1]c"
		}
   }

	#--  Add new Tag to selection
	#if { $TagNew eq "" } { return }; 			# Stop now if No Tag to add
	if { $TagNew ne ""  && $TagType eq "Line"} {
		$::txt tag add $TagNew "sel.first linestart" "sel.first lineend"
	}
	if { $TagNew ne "" && $TagType eq "Word"} { $::txt tag add $TagNew sel.first sel.last }

	#--  Write new mardown symbol
	switch -glob $TagNew {
		"bold" { $::txt insert sel.first "**" syntax ; $::txt insert sel.last "**" syntax }
		"italics" { $::txt insert sel.first "_" syntax;	$::txt insert sel.last "_" syntax }
		"underline" { $::txt insert sel.first "__" syntax;	$::txt insert sel.last "__" syntax }
		"overstrike" { $::txt insert sel.first "~~" syntax;	$::txt insert sel.last "~~" syntax }
		"highlight" { $::txt insert sel.first "<mark>" syntax;	$::txt insert sel.last "</mark>" syntax; $::txt tag remove sel 0.0 end }
		"superscript" { $::txt insert sel.first "<sup>" syntax; $::txt insert sel.last "</sup>" syntax }
		"subscript" { $::txt insert sel.first "<sub>" syntax; $::txt insert sel.last "</sub>" syntax }
		"BigFont" { $::txt insert sel.first "<big>" syntax;	$::txt insert sel.last "</big>" syntax }
		"BigFontBold" { $::txt insert sel.first "<big>**" syntax;	$::txt insert sel.last "**</big>" syntax }
		"SmallFont" {	$::txt insert sel.first "<small>" syntax;	
							$::txt insert sel.last "</small>" syntax }
      "CodeInline" { $::txt insert sel.first "`" syntax; 
							$::txt insert sel.last "`" syntax; 
							$::txt tag remove sel 0.0 end}
      "heading1" { $::txt insert "sel.first linestart" "# " syntax }
      "heading2" { $::txt insert "sel.first linestart" "## " syntax }
      "heading3" { $::txt insert "sel.first linestart" "### " syntax }
      "heading4" { $::txt insert "sel.first linestart" "#### " syntax }
      "heading5" { $::txt insert "sel.first linestart" "##### " syntax }
      "heading6" { $::txt insert "sel.first linestart" "###### " syntax }
      #"indent1"   {$::txt insert "sel.first linestart" "|" syntax "\t" }
      #"indent2"   {$::txt insert "sel.first linestart" "|" syntax "\t\t" }
      #"indent3"   {$::txt insert "sel.first linestart" "|" syntax "\t\t\t" }
      "center"   { $::txt insert "sel.first linestart" "// " syntax }
      "right"    { $::txt insert "sel.first linestart" "/// " syntax }
		"fill"	{ $::txt insert "sel.first linestart" "/ " syntax " "}
		default    {}
	}
	#--  Reparse the line
	#puts "DebugTag : [ GetRowNumber ] $line"
	#catch { ParseLine  [ GetRowNumber ] $line }
}
