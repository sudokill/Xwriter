#   Manage Font  

#===   CREATE FONT FOR ALL THEME
# WARNING :  Do NOT change Font Size if you want to be compliant with 'Markdock' format
proc SetDefaultFont {} {
	set TextFontFamily				[ font actual TextFont -family ]
	set TextFontSize					[ font actual TextFont -size ]
	font create TextFontBold		-family "$TextFontFamily" -size "$TextFontSize" -weight bold
	font create TextFontMonospaceBold		-family "monospace" -size "$TextFontSize" -weight bold
	font create TextFontItalic		-family "$TextFontFamily" -size "$TextFontSize" -slant italic
	font create TextFontItalicBold	-family "$TextFontFamily" -size "$TextFontSize" -slant italic -weight bold
	font create TextFontSmall		-family "$TextFontFamily" -size [expr $TextFontSize -3]; # sub/supscript
	font create TextFontBig			-family "$TextFontFamily" -size [expr $TextFontSize +3]
	font create TextFontBigBold	-family "$TextFontFamily" -size [expr $TextFontSize +4] -weight bold
	font create TextFontTitle		-family "$TextFontFamily" -size [expr $TextFontSize +6] -weight bold
	font create FontHeading1		-family "$TextFontFamily" -size [expr $TextFontSize +4] -weight bold
	font create FontHeading2		-family "$TextFontFamily" -size [expr $TextFontSize +3] -weight bold
	font create FontHeading3		-family "$TextFontFamily" -size [expr $TextFontSize +1] -weight bold
	font create FontHeading4		-family "$TextFontFamily" -size [expr $TextFontSize +1] -weight bold
	font create FontHeading5		-family "$TextFontFamily" -size [expr $TextFontSize +0]
	font create FontHeading6		-family "$TextFontFamily" -size [expr $TextFontSize +0]
}


#===   CHANGE/SAVE  FONT SIZE
#  Note : Update also font family if default font family has changed
proc ChangeFontSize { increment } {
	global cfg
   set TextFontFamily   [ font actual TextFont -family ]
   set MonospaceFontFamily   [ font actual MonospaceFont -family ]
	#-- Change Font size and Font Family except for monospace
	foreach fontName { \
		TextFont TextFontBold TextFontItalic TextFontItalicBold \
		TextFontTitle FontHeading1 FontHeading2 FontHeading3 FontHeading4 \
		FontHeading5 FontHeading6 TextFontBig TextFontSmall TextFontBigBold
	} {
			set fontSize [font actual $fontName -size]
			incr fontSize $increment
			font configure $fontName -size $fontSize -family $TextFontFamily
	}
	font configure MonospaceFont -size [expr ( [font actual MonospaceFont -size]+$increment)]
   set TextFontSize     [ font actual TextFont -size ]
   set MonospaceFontSize     [ font actual MonospaceFont -size ]
	#-- Save new font settings in user config file
	set cfg(FONT.TextFont)  "-family \"$TextFontFamily\" -size $TextFontSize"
	set cfg(FONT.MonospaceFont)  "-family \"$MonospaceFontFamily\" -size $MonospaceFontSize"
	config:save $::ConfFile  ""
	#-- Display Font used
	if { $cfg(MAIN.GlobalTheme) eq "terminal" } {
		set ::status " Font: $MonospaceFontFamily  $MonospaceFontSize"
	} else { set ::status " Font: $TextFontFamily  $TextFontSize" }
}


#===   DISPLAY FONT CHOOSER
proc DisplayFontChooser {} {
	if { [$::txt tag ranges sel] ne "" }  { 
		set ::status " Font choice is not for selection "
	}
	tk fontchooser configure -title "Select document font" -parent . \
			-font [$::txt cget -font] -command { font_changed }
	tk fontchooser show
	puts "\nAvailable Fonts:\n[lsort -dictionary [font families]]\n";
}


#===   EVENT ACTION WHEN FONT CHANGED
proc font_changed { font args } {
	set ::status " $font $args";
	set FontFamily [lindex $font 0]; set FontSize [lindex $font 1]
	set OldFontSize [font actual TextFont -size]
	#puts "DegubFont: Family: $FontFamily  NewSize: [expr $FontSize-$OldFontSize]"
	font configure TextFont -family $FontFamily
	ChangeFontSize [expr $FontSize-$OldFontSize]
}

bind . <<TkFontchooserFontChanged>> { }

