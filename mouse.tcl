#!/usr/bin/wish
#		- Define mouse Event on text widget

#package require tooltip;        # For hint display when mouse over button
#namespace import tooltip::*;    # Import Namespace tooltip

#==     DEFINE 'RIGHT-CLICK' MOUSE SUB-MENU WHEN TEXT SELECTED
menubutton .fr.menuMouseTextSelected -menu .fr.tb.menuMouseTextSelected.m
menu .fr.menuMouseTextSelected.m -tearoff false -background grey90 -foreground grey20 -activebackground #82A0B9 -activeforeground white
.fr.menuMouseTextSelected.m add command -label "  Copy" -command { event generate  $::txt <Control-c> }
.fr.menuMouseTextSelected.m add separator
.fr.menuMouseTextSelected.m add command -label "  Capitalize Word" -command {
		$::txt replace sel.first sel.last [capitalizeEachWord $::TextSelected]
}
.fr.menuMouseTextSelected.m add command -label "  lowercase" -command {
		$::txt replace sel.first sel.last [string tolower $::TextSelected]
}
.fr.menuMouseTextSelected.m add command -label "  UPPERCASE" -command {
		$::txt replace sel.first sel.last [string toupper $::TextSelected]
}
.fr.menuMouseTextSelected.m add command -label "  Size + " -command { ReplaceTag "Word" "BigFont" "" }
.fr.menuMouseTextSelected.m add command -label "  Size - " -command { ReplaceTag "Word" "SmallFont" "" }
.fr.menuMouseTextSelected.m add command -label "  Super Script" -command { ReplaceTag "Word" "superscript" "" }
.fr.menuMouseTextSelected.m add command -label "  Sub Script" -command {	ReplaceTag "Word" "subscript" "" }
.fr.menuMouseTextSelected.m add command -label "  Strikethrough" -command {	ReplaceTag "Word" "overstrike" "" }
.fr.menuMouseTextSelected.m add separator
if { [auto_execok trans] ne "" } {;   # translate package installed
   	.fr.menuMouseTextSelected.m add command -label "  Translate   " \
			-command {event generate $::txt <F5>} -accelerator "   F5"
	}
.fr.menuMouseTextSelected.m add command -label "  Clear formatting" -command {
		RemoveSymbolAndTag sel.first sel.last; }  -accelerator "   Ctrl-Return "

#==  DEFINE POPUMENU FOR URL
menu .popupMenu -tearoff false	-background grey90 -foreground grey20 -activebackground #82A0B9 -activeforeground white

#--  MOUSE  SINGLE-CLICK-LEFT
bind $::txt <1> {}

#--  MOUSE MOOVE
bind $::txt <Motion> {}

#--  DOUBLE-CLICK-LEFT  ( equivalent to click+click.   select )
bind $::txt <Double-Button-1>  {}

#--  SINGLE-CLICK MIDDLE
bind $::txt <2>  { break; }

#--  SINGLE-CLICK-RIGHT  
# No text selected : View Insert Menu.   Text Selected : View Selection Menu
bind $::txt <3>  {
   set pos [$::txt index @%x,%y];    	# Debug: set ::status " %x %y"
   set Tags [ $::txt tag names $pos ];  # Return Tags for the position
	#puts "DebugMouse:   tag:<$Tags>"
	#-- Toggle wordwrap if in codeblock or table
   if { [ string match "*Table*" $Tags ]  || [ string match "*CodeBlock*" $Tags ] } { Toggle_Wordwrap; return}
	#--  Display a menu ( Menu 'MouseTextSelected', or menu 'insert' )
	if { [ $::txt tag ranges sel ] eq "" } {
		tk_popup .fr.tb.menuInsert.m %X %Y;
	} else {
		set ::TextSelected [$::txt get -displaychars "sel.first" "sel.last"]
		tk_popup .fr.menuMouseTextSelected.m %X %Y;
	}
	break;
}

#--  WHEN SELECTION CHANGE ( Warning: also used with 'search' function )
bind $::txt <<Selection>> {}


#==     MOUSE EVENT FOR TAG 'DETAIL'  (HTML <details>/Expandable paragraph)
$::txt tag bind details <Double-Button-1> {
	#==  Toogle 'HTML Details' visibility
   set pos [$::txt index @%x,%y]
	set Line  [$::txt get "$pos linestart" "$pos lineend"]
	#--  Configure tag 'Detail' ( View/Hide and Priority Vs tag 'syntax')
	set Tag_Detail Details[md5::md5 -hex $Line]
	set ::HideDetails [expr {![$::txt tag cget $Tag_Detail -elide]}]
	$::txt tag configure $Tag_Detail -elide $::HideDetails
	if { ! $::HideDetails } {
	   $::txt tag lower $Tag_Detail;	# View details but not markdown syntax
	} else {
   	$::txt tag raise $Tag_Detail;	# Hide details and markdown syntax
	}
	#-- Remove tag selection for all the document
	after idle	$::txt tag remove sel 0.0 end;					# Remove tags 'sel'
}
$::txt tag bind details <Enter> {
	if { $::projectFile == "" } { set ::status  " Ctrl-r to reformat if changed" }
	$::txt configure -cursor arrow
}
$::txt tag bind details <Leave> {
	set ::status "";   $::txt configure -cursor xterm
}


#=====     MOUSE EVENT FOR IMAGE (tag=TagImage)
$::txt tag bind TagImage <Button-1> {
   #-- Get Image Path and open image viewer
   set pos [$::txt index @%x,%y];    	# Debug: set ::status " %x %y"
   set range [$::txt tag prevrange ImagePath  $pos  [$::txt index "$pos linestart"] ]
   if { $range ne "" } {set ImagePath [eval $::txt get $range]} ;   # Get Text with tag "URL"
	set ImagePath [ GetAbsolutePath $ImagePath  $::FileName ]
	catch { exec $::cfg(MAIN.FileViewer)  $ImagePath & }
	break;
}
$::txt tag bind TagImage <Enter> { $::txt configure -cursor hand1 }
$::txt tag bind TagImage <Leave> { $::txt configure -cursor xterm }


#=====     MOUSE EVENT FOR TAG 'CODEBLOCK'
$::txt tag bind CodeBlock <Enter> {
   set pos [$::txt index @%x,%y];    #Debug: set ::status " %x %y"
   set Tags [ $::txt tag names $pos ];  # Return Tags for the position
   #--  Code Highlighting if inside CodeBlock
   if { [ string match "*CodeBlock*" $Tags ] } {
		set BeginBlock [lindex [ $::txt tag prevrange syntax $pos] 0 ]
		set EndBlock [lindex [ $::txt tag nextrange syntax $pos] 0 ]
		if { $BeginBlock ne "" && $EndBlock ne "" } {
			#-- Get Code Language
			set BlockID [$::txt get $BeginBlock "$BeginBlock lineend"]
   		regexp -lineanchor {^(\s*?```)(.+?)(\s+|$)} $BlockID All s1 CodeLanguage; # set language
   		regexp -lineanchor {^(\s*?```\{\.)(.+?)(\s+|$)} $BlockID All s1 CodeLanguage; # s>
			#-- Highlight each line in code block
			for { set i [expr int ($BeginBlock)+1] }  {$i < [expr int ($EndBlock)]} {incr i } { 
        		#puts "DebugFormat: <$CodeLanguage> $i: [$::txt get $i.0 "$i.0 lineend"]"
				catch { FormatCodeBlock $CodeLanguage  $i  [$::txt get $i.0 "$i.0 lineend"] }
			}
		}
	};   # endif  match codeblock
}

$::txt tag bind CodeBlock <Leave> {
	set ::CodeLanguage ""; # Reset language code when leaving Code block
}


#=====     MOUSE EVENT FOR URL
#--  Define Action when click on URL
$::txt tag bind URL <Button-1> { openURL %x %y };  # Open URL defined after

#--  Define Action when mouse hover URL

# Display URL on status entry ( Or popup window on bottom-left corner)
$::txt tag bind URL <Enter> {
	if { [ focus ] ne "$::txt" } { return };   # Do Nothing if not in text widget
	$::txt configure -cursor hand1
	set pos [$::txt index @%x,%y];   # ForDebug:  set ::status " %x %y"
   set TextTag [$::txt tag nextrange URLpath  $pos  "$pos lineend"]
   if { $TextTag ne "" } {
		set Text [string map { "https://" "↳" "http://" "↳" } [eval $::txt get $TextTag] ]
		set ::status "$Text"

		##-- Draw popup: Get RowHeight, set popup content and draw the popup (bottom-left)
		#label .test -text "M"  -font DefaultFont
		#set rowHeight [expr [winfo reqheight .test]+3]
		#destroy .test
		#.popupMenu  delete 0 1000
		#.popupMenu  add command -label " $Text "
		#tk_popup .popupMenu [winfo x .] [expr [winfo y .]+[winfo height .]-$rowHeight]
		#after 1000  destroy .popupMenu
	}
}

#--  Define Action when mouse exit URL
$::txt tag bind URL <Leave> {
	if { [ focus ] ne "$::txt" } { return };   # Do Nothing if not in text widget
	$::txt configure -cursor xterm; set ::status ""
}



#=====     MOUSE EVENT FOR IMAGE
#-- Define Action when hover on "Error image"
$::txt tag bind TagImageError <Enter> {
	if { [ focus ] ne "$::txt" } { return };   # Do Nothing if not in text widget
	#tk_messageBox -message "\n Image with http link are not downloaded by default  \n Enable with 'Toolbar > ▣ > Display Web Image' \n\n  Note: Image are stored in ~/.cache/xwriter" -type ok -icon info;
	set ::status " GoTo Menu ▣: Display Web Image"
}
#-- Define Action when mouse exit "Error image"
$::txt tag bind TagImageError <Leave> {
	if { [ focus ] ne "$::txt" } { return };   # Do Nothing if not in text widget
  	set ::status ""
}


#===    ACTION WHEN CLICK ON URL
proc openURL { xpos ypos } {
	set url ""; set urlPath "";
   #-- Get/Format URL
   set pos [$::txt index @$xpos,$ypos]
   set range [$::txt tag prevrange URL  $pos  [$::txt index "$pos linestart"] ]
	set range2 [$::txt tag nextrange URLpath  $pos [$::txt index "$pos lineend"] ]
   if { $range ne "" } {set url [eval $::txt get $range]} ;   # Get Text with tag "URL"
	if { $range2 ne ""} {set urlPath [ eval $::txt get $range2 ]}; # Get Text with tag "URLpath"
	set ::LastLink [list "$::FileName"   $pos ];  # Keep Last link to go back if request by user
   #puts "\nDebugClick1: Name: $url  Path: $urlPath"
  	#puts "DebugClick1: Pos:$pos  [$::txt index "$pos linestart"]  [$::txt index "$pos lineend"] "

	#-- if URL is an header link with ID as "#xxx".  
	if { [ string match "#*" "$urlPath" ] } {
		catch { CreateTOC "" }; # Update/load TOC to generate header ID
		foreach {key value index} [$::txt dump -mark 1.0 end] {
      	if { [ string match "#$value" $urlPath ]} {
				#puts "DebugShortcutID: $value $index"
				$::txt yview [$::txt index $index ];  # scroll to view '#Id' at top of window
				return
			}
		}
	}
	#-- If URL is a direct link , set path 
	if { [ string match "http*://*" "$url" ]} {
		set urlPath $url
   	#puts "DebugClick2: Name: $url  Path: $urlPath  Pos:$pos [$::txt index "$pos lineend"] "
	}
	#--  Check if document is saved before opening another document
	if { ! [ string match "*http*" "$urlPath" ] &&  [$::txt edit modified] } {
		if { [tk_messageBox -message " You try to open another document but \n your current document is not saved. Continue ?" \
					-type yesno -icon question] eq no } { return }
	}
   #--  Open URL
	set SourcePath [file dirname $::FileName ]
	set NormalizePath [file normalize $SourcePath/./$urlPath ]
   #puts "DebugClick3: Name: $url  urlPath: $urlPath   \n\t\tSourcePath: $SourcePath \n\t\tNormalizePath: $NormalizePath "
	switch -glob "$urlPath" {
		"http*" - "www.*"	- "*.html"	{  catch { exec {*}$::cfg(MAIN.BrowserViewer) $urlPath & } }
		"/*.md" - "/*.diff"	{  LoadFile  $urlPath "";  }
		"/*" 						{  catch { exec $::cfg(MAIN.FileViewer)  $urlPath & } }
		"*.md" - "*.diff"		{	LoadFile  "$NormalizePath" ""; }
		default				{	catch { exec $::cfg(MAIN.FileViewer)  "$NormalizePath" & } }
	}
	$::txt configure -cursor xterm;   # Style cursor = xterm
}

#==   CENTER MOUSE
proc centerMouse {w {wmgeo ""}} {
  if {$wmgeo==""} {set wmgeo [winfo geom $w]}
  set t [split [split $wmgeo x] +]
  event generate $w <Motion> -warp 1 -x [expr [lindex $t 0 0]/2] -y [expr [lindex $t 0 1]/2]
}


