
To Mr Jhones								" My Name "{right}
Street										" My Adress "{right}
City											" My email/Phone "{right}
  


**Subject : A letter with full-justify text**
  
  

//	Dear Mr/Ms Jhones

  
  
/ I am pleased to write a letter of recommendation to admit Sandy Everett to your English Studies program. I had the honor of teaching Ms. Everett in two of my literature classes at Pownall High School, and she is one of the most gifted and hard-working students I've taught.
  

"
Ms. Everett has the ability to balance multiple assignments and complete each one on time with exceptional attention to detail. She is also a brilliant writer who understands how to use words to make convincing arguments and craft beautiful stories. 
She has even been the editor of the school yearbook for the past two years and has transformed it into a stunning work that showcases our school, students and projects in the best light
"{.fulljustify}
  

/ Ms. Everett is a model student and would be an exceptional addition to your program. Please contact me at 391-339-3042 if you need additional information.

  

Yours faithfully  <!--  Unknown firm/person, English -->
Yours truly       <!--  Unknown firm/person, American -->
Yours sincerely   <!--  A person you know the name -->
Best wishes       <!--  A person you know personally -->

