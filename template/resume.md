"**Janna Gardner**"{1.5em center #brown}

//  _4567 Main Street, Chico, Illinois 98052 • (716) 555-0100 • janna@example.com_ 

**Human Resources Generalist** with 6+ years of experience assisting with and fulfilling organization staffing needs and requirements. A proven track record of using my excellent personal, communication and organization skills to lead and improve HR departments, recruit excellent personnel, and improve department efficiencies. Team player with excellent communication skills, high quality of work, driven and highly self-motivated. Strong negotiating skills and business acumen and able to work independently.

" 	**Experience**"{##eeeeef |grey <>98% 1.1em}

20XX – Present
**Human Resources Generalist \| Lamna Healthcare Company \| Chicago, Illinois**

Review, update, and revise company hiring practices, vacation, and other human resources policies to ensure compliance with OSHA and all local, state, and federal labor regulations. By creating and maintaining a positive and responsive work environment, raised employee retention rates by over 10% to achieve a greater than 90% employee retention over a 2-year period.

JUNE 20XX – AUGUST 20XX
**Human Resources Intern \| Wholeness Healthcare \| Boomtown, Ohio**

Assisted in recruitment outreach to prospective employees. Organized and conducted several seminars for hospital employees to educate and update them regarding available employment benefit options. Arranged hospital-wide guest speakers symposia to educate management about new employment laws and workplace confidence and morale building techniques. Administrative tasks.

" 	Skills"{##eeeeef |grey bold <>98% 1.1em}

Type 96WPM • Proficient with project management software • Team player • Excellent time management skills • Conflict management • Public speaking • Data analytics

" 	Education"{##eeeeef  |grey bold <>98% 1.1em}

MAY 20XX
Bachelor of Arts Human Resources Management \| Jasper University \| Ft. Lauderdale, FL

" 	Activities"{##eeeeef  |grey  bold <>98% 1.1em}

Literature • Environmental conservation • Art • Yoga • Skiing • Travel
