
#  Xwriter  V0.6.4  

Use 'Escape' or 'F1' to exit Help

   [Introduction](#introduction)
   [Essential ShortKeys](#essential-shortkeys)
   [Mouse](#mouse)
   [Editing Shortkeys](#editing-shortkeys)
   [Formatting Shortkeys](#formatting-shortkeys)
   [Markdock syntax](#markdock-syntax)
   [Table](#table)
   [Menu tools ▣](#menu-tools)
   [Left Pane](#left-pane)
   [Bookmark](#bookmark)
   [Find and Search](#find-and-search)
   [Spell](#spell)
   [Translate](#translate)
   [Buffer](#buffer)
   [Export to HTML/PDF](#export-to-htmlpdf)
   [Export to Other format](#export-to-other-format)
   [Pandoc filters](#pandoc-filters)
   [Archive .mdz](#archive-mdz)
   [HTML Alias](#html-alias)
   [Shortcode](#shortcode)
   [TCL command](#tcl-command)
   [Metadata](#metadata)
   [Compare Files](#compare-files)
   [Force New lines](#force-new-lines)
   [Execute external programs](#execute-external-programs)
   [Project Mode](#project-mode)


##  Introduction

*  To view/hide markdown symbols, use shortkey 'Ctrl-Space'
*  The text is formatted each time you enter a new line (or with 'Ctrl-r' )
*  Markdown files must have extension .md  ( or .mdz/.mdZ for archive )
*  Code syntax is highlighted when mouse hover the code block

## Essential ShortKeys

 MAIN SHORTKEY |  DESCRIPTION                                      
 --------------|---------------------------------------------------
 Esc           | Clear message, Exit buffer (Help, Unicode symbol )
 Ctrl-Space    | View/Hide  Markdown symbols/Metadata/Trailing spaces
 Ctrl-r        | Reformat (Apply syntax formatting to entire document)
 Double-Return | End of Table/Indented paragraph/BlockQuote
 Ctrl +/-      | Zoom text +/-

## Mouse

  EVENT                                  |  DESCRIPTION           
 ----------------------------------------|-------------------------
 Double-Click                            | Select Word
 Single-Click-Right on Table/CodeBlock   | Toogle word wrap
 Single-Click-Right on selection         | View 'Selection' menu
 Single-Click on image                   | Open Image on viewer
 Single-Click-Right for other case       | View 'Insert' menu


## Editing Shortkeys

  SHORTKEYS            |          -               |         -            
 ----------------------|--------------------------|----------------------
 Ctrl o: Open          | Ctrl s: Save             | Ctrl q: Quit
 Ctrl O: Recent files  | Ctrl g: Goto line        | Ctrl+-: Zoom +/-
 Ctrl S: SaveAs        | Ctrl f: Find/Replace     | Ctrl w: Wrap line
 Ctrl x: Cut           | Ctrl c: Copy             | Ctrl v: Paste
 Ctrl A: Select All    | Ctrl z: Undo             | Ctrl Z: Redo
 Ctrl left: Next Word  | Ctrl right: Prev. word   | 
 Ctrl a: Line start    | Ctrl e: Line End         | Ctrl k: Previous link
 Ctrl <: Width +/-     | Ctrl B: Bookmark         | Ctrl t: Switch Theme
 Ctrl T: Table Content | Ctrl r: Reformat         | Ctrl p: Print/Preview
 Ctrl U: Unicode char  | Ctrl L: Get Info         | 
 Ctrl: : Search        | Ctrl !: Shell command    |
 F1    : Help          | F2    : Spell            | F3: Spell language
 F4    : Conjugate Fr  | F5    : Translate        | F6: Display web image
 Ctrl Up: Fold All     | Ctrl Down: Unfold All    | Alt Up: Add to buffer 
 Alt Right: Next Buffer| Alt Left: Previous buffer| Alt Down: Remove buffer


##  Formatting Shortkeys

  SHORTKEY         |          -             |         -                
 ------------------|------------------------|---------------------------
 Ctrl b: Bold      | Ctrl i: Italic         | Ctrl u: Underline
 Cltr h: Heading   | Ctrl m: Mark/Highlight | Ctrl j: Justify 
 Ctrl l: Hyperlink | ctrl I: indent line    | Ctrl C: Code

 **Note** : For Code, 
	- One line selected: CodeInline, 
	- Multiple lines selected: CodeBlock


## Markdock syntax

Based on [markdown syntax](doc/markdown cheatsheet.md)
Advanced Help for markdock format : [Markdock](markdock.md)
A lot of [Example/Test](./test) 

 ITEM         |                       SYNTAX                           
 -------------|---------------------------------------------------------
 Title        | # Document Title + 2 trailing spaces
 Heading      | # Heading1    ## Heading2    ###(###) Heading3-6
 Emphasis     | \*italic\*  \_italic\_   \*\*Bold\*\*   \*\*\*Bold Italic\*\*\*
 Underline    | \_\_Underline\_\_    \_\_\_BoldUnderline\_\_\_
 Mark/Delete  | <mark\>highlight</mark\> or =\=mark=\=  ~~strikeout~\~
 Supscript    | <sup\>supscript</sup> or ^supscript\^
 Subscript    | <sub\>subscript</sub> or ~subscript\~
 Horiz. line  | -\-\-\-  ( 4 or more )
 Font size    | <big>Big Font</big\>  <small>Small Font</small\>
 HTML alias   | "Text"\{serif #red ...}   ( See section 'HTML alias')
 HTML details | :> Summary (Foldable paragraph to next empty line)
 Quote        | > Block Quote     >> Block Quote Level 2
 List         | [0-2Tabs]+/-/*    [0-2Tabs]1. or 1)
 Definition   | : [Tab] Description   (line above=Term Name)
 Code         | `CodeInline\`     \`\`\`[Language] Code Block \`\`\`
 URL          | http://google.com   [name]\(http://google.com)
 Image        | ![Name]\(/x/x.jpg) ![Name]\(x.jpg) ![Name]\(SubDir/x.jpg)
              | ![Name;100px]\(xx.png) ![Name;30%]\(xx.png) 
              |  (Image width not 100% exact due to limitations)
 FileLink     | [Name]\(Path/FileName)    No Path= Document Path
 HeaderLink   | [Name]\(#HeaderName)   HeaderName= "xxx-yyy-zzz"
 Comment      | <\!--  Text  --\>    Use Ctrl-space to view/hide
 Justify      | / Fully-justify   // Center    //// Right
 Indent       | \|+1tab Indent1   \|+2tab Indent2   \|+3tabs Intent3
 Tab          | space+tab
 Page Break   | \*\*\*\*  ( 4 or more )
 Metadata     | \-\-\-  Text block \-\-\-
 Include file | {{include [Name]\(FilePath) }\} or
              | In a code block as \`\`\`include , then files path   \`\`\`  


## Table

See [Markdock](markdock.md) , section 'Table'
Tips : On table header, use trailing spaces to adjust the header style

## Menu tools ▣

*  Force New Line : Add 2 trailing spaces at each line. Force HTML new line
*  Display Web Image : Download Image with a link (http...)

 
## Left Pane

*  The left pane appears when you activate 'bookmark', 'recent files' , 'Table Of Content', 'Spell'...
*  Single-click on item : Load item, Keep left pane open ( usefull to preview/copy-paste...)
*  Double-click on item : Load item and close left pane
*  Click on left pane Top header : Close the left pane 


## Bookmark

*  Click toolbar '★'  to display bookmark List
*  Left pane buttons :
	**Add:** Bookmark current document    **Remove:** Remove selection      **Edit:** Edit Bookmark
*  Edit bookmark :
	Use 'tabs' indentation to define tree structure
	Save the file (Ctrl-s)


##  Find and Search

*  Use "Control-f" to search a string inside a document
*  To search string inside Bookmark/Recent files : 
	- Open "Recent File"
	- Click on 'Search' button
	- Enter a pattern (use regexp format)
	- In the returned list, 
		1. Click on the line number to open the file
		2. On toolbar, click "^" to return to "search" screen 
* In project mode, search string is searched in project files only

##  Spell

*  Install language packages: sudo apt install aspell-en  aspell-fr (for french...)
*  Spell check is not done inside Code Inline/Code Block
*  Set native language with "menu > Preferences" and use key 'F3' to toggle language ( Native <--> english )
*  F2: Start Spell check       Escape: Abort        F2: next/continue
*  Left pane buttons :
		**Add** Add to dictionary   **Ignore** Ignore until next restart   **Next** Continue
*  Replace word : Select a word on left pane
*  Start spell check at a position : select the position, then F2
*  Custom Dictionary : Edit file ~/.config/xwriter/dict.en.pws
(only english custom dictionary available)


## Translate

Require Internet. Use Google Translate.
*  Install translate-shell program : sudo apt install translate-shell
*  Use F3 to toogle destination language ( Native <--> english )
*  Select a text, then F5 (may take few seconds)


##  Buffer

Easily switch between files with buffer : 
	- Add current document to buffer list : Alt-Up
	- Remove a buffer : Alt-Down
	- Navigate between buffer : Alt-Right, Alt-Left
Buffers are displayed as:  [Number] FileName (without extension)  
If the buffer is unsaved, a sign '*' is displayed after the buffer number  


## Export to HTML/PDF

Application 'pandoc' or 'lowdown' are recommended (but not necessary)
Export done in order of priority : 1. [Pandoc](doc/pandoc-markdown.md), 2. [Lowdown](doc/lowdown.md), 3. Internal converter(pantcl)
Be sure to understand the limitation and syntax for the converter you use
*  Preview HTML : Ctrl+p
*  Export As HTML : Ctrl+S (SaveAs) and select 'HTML file'
( a file 'style.css' will be copied in the destination folder)
*  Print/Export As PDF  : Export as HTML, then use 'print' in your browser
Tip1 : If you select a text inside xwriter, only the selection will be exported to HTML
Tip2 : In case of error, open the HTML file with entry box on toolbar
**Note** :  If pandoc/lowdown not installed, Mermaid/Mathjax code is disable
 
## Export to other format

Require application 'pandoc' installed
*  Edit export style:  Select Toolbar > ▣ > Edit Document style
*  Export a document:  Ctrl-S (SaveAs) and set filename with the appropriate extension (.docx/.odt/.pdf/.epub/.rtf/.xml)
*  Raw HTML inside document works only with :
		HTML, S5, Slidy, Slideous, DZSlides, EPUB, Emacs Org mode, Textile
*  Markdock syntax (center,underline...) works only with DOCX


##  Pandoc filters

1. Use tcl pandoc filters : [pantcl filters](doc/pantcl/)
tcl filters are disable by default at each start
Activate tcl filters in  Toolbar > ▣ > 'Enable Code Evaluation' and use 'eval=true' in Code block when necessary
or
2. Write your own lua filters in ~/.pandoc/filters and enable these filters in Menu > Preferences

## Archive .mdz
Similar to microsoft docx ( text/image on same file )
Must contain at least one file with extension '.md' in the archive root  
(Priority order: readme.md,  main.md,  first file .md found)
*  Export .md document to Archive (.mdz) :  Menu > SaveAs > select 'markdown archive'
*  Export .md document to encrypted Archive (.mdZ) : Menu > SaveAs > select 'markdown Encrypt Archive'
*  Open/Save archive .mdz/.mdZ like any other file


## HTML Alias

Works only when you export to HTML (and PDF with browser print menu)
Syntax: `"Text"{serif 1.2em #white ##grey |blue ...} `
Note: If you need quote in the text, only use single quote (')

Alias Definition:  

*  Text Family:  serif/sans/sans-serif/mono/monospace
*  Text Size  :  1.0em (100%/16px) 0.8em (=80%/11px) 1.2em (120%/19px) ...  
*  Text weight :  bold  italic
*  Text Color: #white,#red...,hexadecimal value (#0xFFAC09 #FFAC09 #ffac09)  
*  Background Color: ##white...,hexadecimal value (##0xFFAC09 ##FFAC09 ##ffac09)  
*  Border color : |black   |red ...
*  Text Justification:   center/right/left/fulljustify (=HTML float left/right)
*  Block : block (Apply style to full line (=HTML style display=block)  
*  Class : .Myclass  (Apply a HTML-CSS  class 'Myclass')
*  Block width:  <>50%  ( HTML Block width 50% )  
*  Block padding:  >10% ( Padding Left )   <20%  ( Padding Right )  
If alias is unknown, the alias is supposed to be a font family 

Use 'Control-Space' to Display/Hide HTML alias

## Shortcode

Shortcode can execute complex functions
1.  {{include  <file>}}   to include a file before exporting

## Tcl command
You can execute built-in tcl commands with `` `tcl command args` ``
Example : `` `tcl  clock format [clock seconds]  -format "%Y-%m-%d  %H:%M:%S" ` ``
	(display current date/time when exporting to html,docx...)
tcl commands are disable by default at each start
To activate tcl commands : Toolbar > ▣ > 'Enable Code Evaluation'


## Metadata

See [markdock](markdock.md), section metadata for help
- Use 'Ctrl+Space' to Display/Hide metadata


## Compare Files

1.  Load the new file
2.  Select 'Menu > Compare File' and choose the old file
3.  You can save the output in a file with '.diff' extension and re-open this file later
**Tip** : Use Ctrl-f(Find)/Ctrl-n(Next) to search differences inside a diff file

##  Force New lines

1. In Menu ▣ > ForceNewLine
Add 2 trailing spaces to lines in the document. 
Usefull if you want to export to a platform (github..) with pure markdown syntax
2. In Menu ▣ > Remove trailing spaces
Remove all trailing spaces in the document

##  Execute external programs

Use shortkey 'ctrl !' , then enter your shell command.
Example : `nano %i`    (%i = current document)
To define alias, select ' Menu > Shell Command '  

##  Project Mode

The project mode is activated automatically when you load a 'project' file .prj
To create a project file, load a file (readme...) from the project, Select "Menu tools ▣ > Create Project"  
Use shortkey 'Alt+Up/Down/Right/Left' to add/delete/switch between files
 
Limitations : 
1. Project Mode is minimalist/experimental
2. Code folding requires a double-empty lines to end python function (def)
3. Code folding do not work with functions having same name/parameters : Introduce additional spaces if you really need to keep same definition

