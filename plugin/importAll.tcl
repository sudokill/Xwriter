#!/usr/bin/wish
#    Convert Document/URL to markdown ( Require pandoc installation)


#==   CONVERT ANY DOCUMENT TO MARKDOWN
proc ImportDocument {} {
   if !($::PandocMissing) {
      after idle ForceWindowSize;                # Force OpenFile window size
      set FileName [ tk_getOpenFile -initialdir $::FilePath ]
      if { $FileName eq "" } { return }
		# --wrap=none: Don't wrap after default max column width=72 (pandoc --columns)
      # Get Extentions : pandoc --list-extensions=markdown
		file mkdir "$::TempDir/pandoc"
      eval exec pandoc \
					--wrap none \
					--extract-media "$::TempDir/pandoc" \
					$::cfg(PANDOC.PandocOptions) \
					--to gfm-raw_html  \
					\"$FileName\" -o "$::TempDir/pandoc/import.md"
      LoadFile $::TempDir/pandoc/import.md ""
   } else { set ::status " application Pandoc is missing " }
}


#==   IMPORT HYPERLINK
proc ImportURL { PathLink } {

	#--  Check curl installation
	if { [ auto_execok curl ] eq "" } {
		tk_messageBox -message "  Install 'curl' to import URL" -type ok -icon error
		return
	}
	#--  Check URL Header validity
	if { [ CheckURL $PathLink ] eq 1 } {
		tk_messageBox -message "  Timeout/Header Error  \n  Retry later\n" -type ok -icon error
		set ::status ""
		return
	}

	#-- Convert Document
		set ::ImportFile $::TempDir/pandoc/ImportURL.md
		set ImportDir $::TempDir/pandoc
		file delete $::ImportFile; file mkdir $ImportDir
		#--  Start a timeout
		countdown 40;   # Init a timeout at xx seconds
      #-- Start Pandoc  (Get Extentions : pandoc --list-extensions=markdown)
		#						--wrap=none: Don't wrap after default max column width=72 (pandoc --columns)
		#						--html-native_divs : Remove <div> tag 
   	eval exec pandoc \
					--wrap none \
					--extract-media $ImportDir  \
					--from html-native_divs  \
					$::cfg(PANDOC.PandocOptions) \
					--to gfm-raw_html  \
					$PathLink -o $::ImportFile &
}


#==  CHECK  URL HEADER validity 
proc CheckURL { URL } {
	set ::status " Checking URL ..."
	update idletasks
	set status 0
	#-- Check URL header (-I) before full download
	#  (4xx/5xx=error)
	catch { set status [ exec  curl -Is --connect-timeout 6  $URL | head -n 1 ] }
	if {  [string match "HTTP* 4*" $status] || [string match "HTTP* 5*" $status] || $status eq 0 } {
		#puts "Invalid URL: $URL    Status: $status"
		return 1
	}
	return 0
}


#==   TIMEOUT (timeout reached or pandoc output file exist)
proc countdown {cnt} {
	.fr.tb.entry configure -background "#FFE4B5"
	#--  End of Timeout
	if { $cnt <= 0 || [file exist $::ImportFile] } {
		set ::status ""
		.fr.tb.entry configure -background white
		#set PandocPID [ exec ps -eo pid,comm | grep pandoc ]
		#catch { exec kill -9 $PandocPID }; # kill pandoc
		if { [file exist $::ImportFile] } {
			LoadFile $::ImportFile ""
		} else {
			tk_messageBox -message " Process aborted ( Timeout ) \n Retry later" -type ok -icon error
		}
		return
	} else { set ::status " Wait $cnt seconds ..." }
	#-- Continue timeout	
	incr cnt -1
	update idletasks;  # Refresh User interface
	after 1000 [list countdown $cnt ]
}
