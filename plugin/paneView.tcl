#!/usr/bin/wish
#  Display Left Pane view for : Bookmark, Recent File, Spell
#  Clean list of files ( Remove duplicate/non-existent files + filter )


#==    GLOBAL VARIABLE
set PaneView "";         # Init Pane Name (Bookmark,Recent..). Do not change ("")


#===   CREATE RecentFile List
proc CreateRecentFileList {} {
	#--  Init User-interface
	set ::PaneView "RecentFile";
	TreeViewOpen
	$::tree heading #0 -text "Recent Files" -anchor center; # anchor w/e/center
	$::tree.tb.b1 configure -text "Search"
	tooltip::tooltip $::tree.tb.b1 " Search string in Bookmark/Recent files ( Ctrl :) \n Enter string on toolbar entry "
	$::tree.tb.b2 configure -text "Remove"
	tooltip::tooltip $::tree.tb.b2 " Remove selection from list "
	$::tree.tb.b3 configure -text "Clear"
	tooltip::tooltip $::tree.tb.b3 " Clear the list "
	#--  Get/Clean files list ( list is different in project mode )
   set infile [open  "$::ConfPath/recent.md" "RDONLY CREAT" ];
   set List0 [read $infile];
	set List0 [lreverse $List0]
	set List0 [cleanList $List0 "" 40];	 # Remove duplicate/inexistent files
	#--  Update left pane with list item
	#puts "----\n$List0\n------\n"
	foreach item $List0 {
		if { $item ne "" } {$::tree insert {} end -id $item -text [file tail $item] }
	}
	close $infile
}


#===   SAVE RECENT LIST
proc SaveRecentFileList {} {
	#--  Move Last selected item to top. Save the list
	if { [$::tree focus] ne "" } {
		$::tree move [$::tree focus] {} 0
	}
	#puts "DebugSaveRecent : \n[lreverse [$::tree children {} ] ]\n---"
	#--  Save to recent list (different if in project mode)
	set outfile [open "$::ConfPath/recent.md" w ]
	puts $outfile [ lreverse [$::tree children {} ] ]
	close $outfile
}


#===   CREATE BOOKMARK LIST
proc CreateBookmarkList {} {
	set ::PaneView "Bookmark";
	TreeViewOpen
	$::tree heading #0 -text "Bookmark" -anchor center; # anchor w/e/center
	$::tree.tb.b1 configure -text "Add"
	tooltip::tooltip $::tree.tb.b1 " Add current document to bookmark "
	$::tree.tb.b2 configure -text "Remove"
	tooltip::tooltip $::tree.tb.b2 " Remove selection from bookmark list "
	$::tree.tb.b3 configure -text "Edit"
	tooltip::tooltip $::tree.tb.b3 " Edit/Backup bookmark list\n To add item: 0/1/2/3Tab+NodeName/FilePath"
	LoadTreeViewFile "$::ConfPath/bookmark.md"
}


#===  SAVE BOOKMARK LIST
proc SaveBookmarkList {} {
	#puts "DebugSaveBookmark: \n----\n[GetTreeItem $::tree {} ]"
	set outfile [open "$::ConfPath/bookmark.md" w ]
	foreach item [GetTreeItem $::tree {} ] {
		#-- Write node. if node is opened, add 2 traling spaces 
		if { [$::tree item $item -open] } {
			puts $outfile "[string trimright $item]  "
		} else {
			puts $outfile "[string trimright $item]"
		}
	}
	close $outfile
}


#===  CREATE SPELL LIST
proc CreateSpellList {} {
	set ::PaneView "Spell"
	TreeViewOpen
	$::tree delete [$::tree children {}];   # Delete all items on treeview
	$::tree heading #0 -text "Spell  [string toupper $::SpellLanguage]" -anchor center; # anchor w/e/center
	$::tree.tb.b1 configure -text "Add"
	tooltip::tooltip $::tree.tb.b1 "Add to dictionnary"
	$::tree.tb.b2 configure -text "Ignore"
	tooltip::tooltip $::tree.tb.b2 "Ignore until\nnext restart"
	$::tree.tb.b3 configure -text "Next"
	tooltip::tooltip $::tree.tb.b3 ""
}


#==   RETURN A LIST OF N FILES WITHOUT DUPLICATE ELEMENTS/NON-EXISTENT FILES/EXCLUDING PATTERN
proc cleanList { list {ExcludePattern ""} { MaxNumber "" } } {
	set new {}
	foreach item $list {
		if { [lsearch $new $item] < 0} {
			if { [file exists $item] && ![string match -nocase $ExcludePattern $item]} {
				lappend new $item
			}
		}
	}
	# Limit list to N elements
	if { $MaxNumber ne "" } { set new [lreplace $new $MaxNumber end] }
	#puts "DebugCleanList: Max:$MaxNumber,  Exclude: $ExcludePattern\n$new\n\n"
	return $new
}
