#!/usr/bin/wish
#  Backup/Restore a text buffer.
#  Manage ExtraView to display help/translate/unicode... view
#  Manage Buffer list (Add, Remove, Navigate in buffer list)

		
#===  GLOBAL VARIABLE
set BufferList {}; 				# Buffer list
set Buffer ();	 					# Buffer array
set ExtraViewName "";			# ViewName (help,search,translate...)

#===  INIT/RESET EXTRA VIEW (Help,Unicode,Translate,Conjugate,Search)

proc InitExtraView { Name } {

	$::txt configure -state normal;   # Remove read-only property if previously set for an extra buffer
	set ::ExtraViewName $Name
	set ::BackupText [ text:save $::txt ];   # Save Text Buffer
	set ::FileName ""
	wm title . ""
	#puts ":BufferInit:  <$Name>  <$::FileName>"
 	switch -glob $Name {
		"Help"	{	LoadFile $::ScriptPath/help.md "NoBackup"
						$::txt configure -state disabled;   # Read-only buffer
					}
		"SpecialChar"	{ LoadFile  $::ConfPath/symbol.md "NoBackup" }
		"Emoji"			{ LoadFile $::ScriptPath/emoji.md "NoBackup" }
   	"Conjugate" 	{
				#puts "DEBUG conjugate\n   [ exec french-conjugator $::ConjugateVerb]"
				set ::status " ESC to exit"
				$::txt replace 1.0 end  ">>>   Use Escape key to quit conjugation  <<< \n\n" bold
				$::txt insert end [ exec french-conjugator $::ConjugateVerb  ]
				}
   "Translate"	{
	   		if { [$::txt tag ranges sel] ne "" } {
					set TextToTranslate [$::txt get -displaychars "sel.first" "sel.last"]
					set ::status " Translate: <$::SpellLanguage>. ESC to exit"
					update idletasks
					set Translation [exec trans -brief -e google -t $::SpellLanguage "$TextToTranslate" ]
					$::txt replace 1.0 end  ">>>   Use Escape key to quit translation   <<< \n\n" bold
					$::txt insert end $Translation
				} else { tk_messageBox -message " Select a text to translate \nUse F3 to change destination language" -type ok -icon error }
			}
   "Search"	{	AskUser " Search: " }
	"DebugCode"  {	LoadFile $::DebugCode "NoBackup" 
						$::txt insert 1.0 ">>>   Use ESCAPE key to exit debug screen  <<<\n\n" bold
						}
	};  # end switch
}


proc ResetExtraView {} {
	if { $::ExtraViewName ne ""  } {
		$::txt configure -state normal;   # Remove read-only property if previously set for an extra buffer
   	text:restore $::txt  $::BackupText;   # Restore Text Buffer
		set ::ExtraViewName ""
	}
}


#===  ADD/REMOVE/LOAD FILE IN BUFFER LIST
proc AddToBufferList {} {
	#-- Save file to buffer list if not already in the list
	if { [lsearch $::BufferList $::FileName] eq -1} {
	lappend ::BufferList  $::FileName;		# Add to buffer list if not found
		set ::buffer($::FileName)  [text:save $::txt];   		# Save text widget in buffer array
		set ::status " \[ [llength $::BufferList] \]  [file root [file tail $::FileName]]"
	} else { set ::status " Already in buffer list" }
	#puts "_BufferList: $::BufferList"
}

proc RemoveFromBufferList {} {
	#--  Go forward, Remove current buffer on buffer list. If empty list, reset text widget
	set idx [lsearch $::BufferList $::FileName]
	array unset ::buffer $::FileName;					# Remove from array
	NavigateBuffer forward
	set ::BufferList [lreplace $::BufferList $idx $idx ]; # Delete FileName/Cursor Position
	NavigateBuffer none;          				# Just to update the screen
	if { [llength $::BufferList] == 0 } { ResetTextWidget }
}

proc LoadFromBufferList { bufferPath } {
	if { [file exists $bufferPath ] } {
		text:restore $::txt $::buffer($bufferPath);	 # Restore Buffer		puts "rest1: [$::txt edit modified]"
		set filestatus [string map {"1" "*" "0" ""} [$::txt edit modified ] ]
		set idx [lsearch $::BufferList $bufferPath]
		set ::status " \[ [expr 1+$idx] \]$filestatus   [file root [file tail $::FileName] ]"
	}
}

proc SaveInBufferList { fileName } {
	set idx [lsearch $::BufferList $fileName]
	if { $idx ne -1} {
		set ::buffer($fileName)  [text:save $::txt];	# Save text widget in a buffer
	}
}


#===  NAVIGATE FORWARD/BACKWARD IN BUFFER LIST
proc NavigateBuffer { direction } {
	# direction = forward/backward or none (just update/check buffer list)
	if { [llength $::BufferList] ==0 }  { set ::status " No files in buffer list" }
	#-- Find filename in the buffer list, if exist, Save the buffer
	set idx [lsearch $::BufferList $::FileName]
	if { $idx ne -1} {
		set ::buffer($::FileName)  [text:save $::txt];	# Save text widget in a buffer
	}
	#-- Find Next/previous buffer
	if { $direction eq "forward" } { set idx [expr $idx+1] }
	if { $direction eq "backward" } { set idx [expr $idx-1] }
	if { $idx >= [llength $::BufferList] } { set idx 0 }
	if { $idx < 0 } { set idx [expr [llength $::BufferList]-1] }
	set NewBuffer [lindex $::BufferList $idx]
	#puts "_Buffer:\n$::BufferList\n  Pos: $idx/[llength $::BufferList] NewBuff: $NewBuffer"
	#--  Load New buffer
	LoadFromBufferList $NewBuffer
}


#===   SAVE TEXT BUFFER
proc text:save {w} {
	set save "";											#  Init Buffer
	#--  Save text with all tags, mark, position...
	foreach {key value index} [$w dump -all 1.0 end-1c] {
		# puts ":BufferSave: $index $key $value"
		switch $key  {
   		mark {     ;# add attributes of a mark
         			lappend save $key $value $index
						if { $value != "" } {
							set exec "$w mark gravity $value [$w mark gravity $value]"
							lappend save exec $exec {}
						}
               }
 			image  {      ;# add attributes of an image
                set exec "\$w image create $index"
                foreach k {-align -image -name -padx -pady} {
                    set v [$w image cget $index $k]
                    if {$v != ""} { append exec " $k \{$v\}" }
                }
                lappend save exec $exec {}
            }
 			window  {     ; # add attributes of a window
                lappend save $key $value $index
                set exec "$w window configure $index"
                foreach k {-align -create -padx -pady -stretch} {
                    set v [$w window cget $index $k]
                    if {$v != ""} { append exec " $k \{$v\}" }
                }
                lappend save exec $exec {}
            }
        default { lappend save $key $value $index }
		};  #end switch
	};  #end foreach

	#--   Add Filename, Status, Scroolbar position to buffer
	if { $::FileName ne "" } { lappend save metadata0 $::FileName 0.0 }
	lappend save metadata1 [$w edit modified] 0.0
	lappend save metadata2 [$w yview] 0.0;  # Vertical position of text
	# puts ":BufferSave: <$::FileName>  <[$w edit modified]>  <[$w yview]>\n\n\n "

   return $save
}


#===   RESTORE TEXT BUFFER
proc text:restore { w buffer} {
	$w delete 1.0 end;											# empty the text widget
	set InsertCursor 1.0; set CurrentPosition 1.0;		# Init insert/current mark
	set ::FileName ""
	wm title . "Untitled"
	if { $buffer eq "" } { return  };				# Nothing to do, buffer is empty
   foreach {key value index} $buffer {
		#puts ":BufferRestore: $index $key $value"
        switch $key  {
            exec     { eval $value }
            image    {  set Split [split $OldText ":"];  # Get Image Zoom
								InsertImage $value $index [lindex $Split 1];
								#puts "DebugImg:$value $index $OldText  [lindex $Split 1]"
							}
            text     { $w insert $index $value;
								set OldText $value
							}
            mark     {  if {$value == "current"} { set CurrentPosition $index }
								if {$value == "insert"} { set InsertCursor $index }
                        $w mark set $value $index
							}
            tagon    { set tag($value) $index }
            tagoff   { $w tag add $value $tag($value) $index }
            window   { if { [ string match "*horizRule*" $value ] } {
                    		frame $value;
                     	$value configure -background grey -height 2 -width 3000
								}
								$w window create $index -window $value
							}
				metadata0 { ;# puts "DebugMetaData: $value"
								set ::FileName $value;
								set ::FileExtension [file extension $value];
								set ::status " [file tail $value]"
								wm title . $value
							}
				metadata1 { $w edit modified $value;  # Restore (un)modified status }
				metadata2 { lassign $value y0 y1 }
       } ;# end switch
    } ;# end foreach
	$w edit reset;									# Reset Undo/Redo stack
	set ::CodeLanguage [ GuessCodeLanguage $::FileName ]
	update
	$w mark set insert $InsertCursor;      # Restore Insert cursor
   $w see [$w index $CurrentPosition ];   # Scroll to previous mouse saved position 
	#after 800 $w yview moveto $y0;  		# Restore vertical position (don't works well)
}
