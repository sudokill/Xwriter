#!/usr/bin/wish
#  Manage Search/Grep research inside list of files
#	1. Build a list of files (bookmark,recent files, project files)
#	2. Search pattern in list of files. Display file/search results
#	3. Manage mouse event when user click on the line number


#===  BUILD LIST OF FILES FOR GREP
proc BuildListFilesGrep {} {
	if { $::projectFile eq "" } {
	   set infile0 [open  "$::ConfPath/bookmark.md" "RDONLY CREAT" ];
	   set List0   [read $infile0]
	   set infile1 [open  "$::ConfPath/recent.md" "RDONLY CREAT" ];
	   set List1 [lreverse [read $infile1]];						# Reverse: Most recent first
		set List0 [list {*}$List0 {*}$List1];						# Merge the list
		close $infile0
		close $infile1
	} else {;  # In projet mode : Search only in files defined in project file '.prj'
	   set infile0 [open  $::projectFile "RDONLY CREAT" ];
	   set List0   [read $infile0]
		close $infile0
		#--  Reformat relative path ./ to absolute path (projectdir/xxx)
		regsub -all -line  {^\s*\./} $List0 "[file dirname $::projectFile]/" List0
		#puts ":grep: BuildList  \n\n $List0" 
	}
	#--  Extract .mdz archive if in list. Add source file (readme.md...) to list
	foreach item $List0 {
		if { [ file extension $item ] eq ".mdz" } {
			lappend List0 [ ExtractArchive  $item  ""];  	# Add source file to list
		}
	}

	#--  Clean list (remove duplicates, comment, key/value, section )
	regsub -all -line {^#.*?$}  $List0  {} List0;			# Remove #comments
	regsub -all -line {^.*?=.*?$}  $List0  {} List0;		# Remove key=value
	regsub -all -line {^\[.*?\]$}  $List0  {} List0;		# Remove [Section]
	set List0  [ cleanList $List0 "*.mdz"];		# Remove duplicate, .mdz
	# puts "_GrepBuildList:\n [string map {" " "\n"} $List0] \n\n"
	return $List0
}


#===  SEARCH PATTERN IN LIST OF FILES
proc SearchPatternInFiles { Pattern } {
	if { [string length $Pattern] < 2 } {
		set ::status " Error: Single/Null Char not allowed"; return
	}
	#--  Clean text widget / Set pattern / Init
	$::txt delete 1.0 end
	$::txt insert end ">>>   Use Escape key to quit Search  <<< \n" bold
	$::txt insert end "SEARCH:  $Pattern      ( Regexp Options:  \\yString\\y WordOnly)\n"
	$::txt insert end "Click line number to open the file.   Toolbar '^' to go back\n"
	set Pattern "(?i)$Pattern" ;    # i: case-insensitive, /y../y: Begin/End Word 
	set OldPath ""
	set List0 [ BuildListFilesGrep ];				# List of files to search inside
	if { $List0 eq "" } { $::txt insert end "\n NO result";
		return
	}
	#--  Search in all files. Reformat result
	foreach item [ ::fileutil::grep $Pattern $List0 ] {
		regexp  {^(.*?):(.*?):(.*?)$}  $item fullline path line content
		#puts ":GrepSearch: $item / $path / $line / $content\n"
		#--  Display File path ( modified if archive .mdz )
		if { $path ne $OldPath } {
			if { [string match "$::TempDir*" $path ] } {
				$::txt insert end "\nArchive  .mdz" bold "       " normal "$path" URL "\n"
			} else {
				$::txt insert end "\n[file tail $path]" bold "       " normal "$path" URL "\n"
			}
		}
		set OldPath $path
		#--  Display Line Number / Line Content for each file path
		set RowNum [GetRowNumber]
		$::txt tag configure TagGrepLine -foreground "#1168CC"
		$::txt insert end "$line"  TagGrepLine "    [string trim $content]\n"
		#--  Highlight the pattern found on the line
		$::txt tag configure TagGrepPattern	-foreground magenta
		TagRegS  TagGrepPattern  $Pattern   [$::txt get $RowNum.0  $RowNum.end]  $RowNum
	}
	$::txt edit modified false;            			# Set document as "unmodified"
}


#===  MOUSE EVENT WHEN CLICK ON LINE NUMBER
proc ConfigureClickLineNumber {} {
	$::txt tag bind TagGrepLine <Button-1> {
	   #-- Get Line Number and File Path
	   set pos [$::txt index @%x,%y]
	   set range [$::txt tag nextrange TagGrepLine   [$::txt index "$pos linestart"]  [$::txt index "$pos lineend"] ]
	   if { $range ne "" } {set LineNumber [eval $::txt get $range]} ;   # Get Text with tag "URL"
	   set range [$::txt tag prevrange URL  $pos  1.0]
	   if { $range ne "" } {set FilePath [eval $::txt get $range]} ;   # Get Text with tag "URL"
		#puts "DebugEventTagGrep : $LineNumber  $FilePath"
		#-- Open file and Go to line  
		if { $LineNumber ne "" && $FilePath ne "" } {
			set ::SearchBuffer [text:save $::txt];   			# Save result in buffer
			set ::LastLink [list "SearchBuffer"   $pos ];	# Keep Last link (go back if request by user)
			LoadFile  $FilePath  ""
			after idle  GoToLine  $LineNumber
		}
	}
	$::txt tag bind TagGrepLine <Enter> { $::txt configure -cursor hand1 }
	$::txt tag bind TagGrepLine <Leave> { $::txt configure -cursor xterm }
}


#===  EXECUTE PROCEDURE
ConfigureClickLineNumber
