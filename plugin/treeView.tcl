#!/usr/bin/wish
#  Define Treeview (Left Pane).
#  Used by plugin 'paneView' to display bookmark/recentfiles/tableOfContent...
#  1. Init the treeview (Geometry, Theme, Font)
#	2. Configure the 3 buttons below the treeview (Left/Middle/Right)
#  3. Configure Mouse/Keyboard event for the treeview
#  4. Define actions when treeview is opened/closed/item selected
#  5. Load a treeview file (project file with treeview hierarchy)



#==  INIT TREEVIEW
proc InitTreeView {} {
	#-- Create Image Used As 'close' Icon On Treeview
	catch { image create photo img_close -file "$::ScriptPath/img/item.png" }

	#-- Define Treeview
	#--  Define a temorary label to set appropriate RowHeight
	label .test -text "M"  -font DefaultFont; # Created Only to set appropriate rowheight
	ttk::style configure Treeview -rowheight [expr [winfo reqheight .test]+5] -font DefaultFont -movablecolumns yes
	destroy .test;                         # Useless Now. Destroy it


	#--  Define Heading Style and padding
	ttk::style configure Heading -foreground black -background grey90 -relief groove -font DefaultFont; # Define Heading Style
	#ttk::style map Treeview.Heading -foreground [list active black !active black] -background [list active grey90 !active grey90]
	$::tree configure -padding {0 0 10 50}; 			# Clearance with Scrollbar/Bottom buttons

	#-- Define 2 extra columns C2,C3 in addition to tree (column #0)
	#$::tree configure -columns "C2 C3" -displaycolumns "" -show "tree headings"

	#--  Define Tree Column #0
	$::tree column #0 -minwidth 100 -anchor nw
	$::tree heading #0 -text "" -anchor center  -image img_close -command {TreeViewClose}
}

#==  CONFIGURE BUTTONS LEFT/MIDDLE/RIGHT (ACTIONS)
proc ConfigTreeButtonLeft {} {
	$::tree.tb.b1 configure \
      -command {
         if { $::PaneView eq "RecentFile" } {;    # Search pattern in bookmark/recent files
					InitExtraView "Search"
				}
         if { $::PaneView eq "ProjectMode" } {;    # Search pattern in project files
					InitExtraView "Search" 
				}
         if { $::PaneView eq "Bookmark" } {
  				#-- if window title =*mdz/*mdZ, File Name = archive name
   			if { [ string match "*.mdz*" [wm title . ] ] || [ string match "*.mdZ*" [wm title . ] ]} {
      			lassign [ split [wm title . ] ">" ] ArchiveName RootFileName
					set FileName [string trim $ArchiveName]
   			}
				#-- Update Treeview and Save Bookmarks
				$::tree insert {} 0 -id [file normalize $FileName] -text [file tail $FileName]
				after idle SaveBookmarkList
			}
         if { $::PaneView eq "Spell" } { ; # Add word to custom dictionary
				set out [open "$::ConfPath/dict.en.pws" a+ ]
      		puts $out "[$::txt get sel.first sel.last]";
				close $out
				set ::status " Add Dictionary: [$::txt get sel.first sel.last]"
         	event generate $::txt <F2>; # Continue spell check
			}
      }
}

proc ConfigTreeButtonMiddle {} {
	$::tree.tb.b2 configure \
      -command {
         if { $::PaneView eq "Bookmark" } {
				$::tree delete [$::tree selection];  # Delete selected item on list
				after idle SaveBookmarkList
			}
         if { $::PaneView eq "RecentFile" } {
				$::tree delete [$::tree selection];  # Delete selected item on list
				SaveRecentFileList
			}
         if { $::PaneView eq "ProjectMode" } { RunProject }
         if { $::PaneView eq "Spell" } {
				set ::SpellIgnoreList "$::SpellIgnoreList[$::txt get sel.first sel.last]|"
				set ::status " Spell Ignore: [$::txt get sel.first sel.last]"
				event generate $::txt <F2>; # Continue spell check
			}
	}
}

proc ConfigTreeButtonRight {} {
	$::tree.tb.b3  configure \
      -command {
         if { $::PaneView eq "RecentFile" } {
				if { [tk_messageBox -message "Clear the list ?" -type yesno -icon question] eq no } { return }
				$::tree delete [$::tree children {}]
				TreeViewClose
			}
         if { $::PaneView eq "Spell" } { event generate $::txt <F2> }
         if { $::PaneView eq "ProjectMode" } { ResetToDefaultMode }
         if { $::PaneView eq "Bookmark" } {
				#--  Close treeview and load bookmard file to edit manually
				TreeViewClose;  # Close TreeView to save current bookmarks
				#file copy -force ~/.config/xwriter/bookmark.md  ~/.config/xwriter/bookmark.md.old
				LoadFile ~/.config/xwriter/bookmark.md "NoBackup";
			}
		}
}


#==  CONFIGURE MOUSE/KEYBOARD ACTION
proc ConfigTreeEvent {} {
	bind $::tree <<TreeviewSelect>> {
		#--  Do nothing if a branch with children below
		set ChildrenNumber [GetChildrenNumber $::tree [$::tree focus] ]
		if { $ChildrenNumber ne 0 } { return }
		TreeViewItemSelected;  # Item Selected
	}
	bind $::tree <Double-1> {
		#--  Double-click On parent:Expand/Collapse (Do nothing)
		set ChildrenNumber [GetChildrenNumber $::tree [$::tree focus] ]
		if { $ChildrenNumber ne 0 } { return };  # Do nothing if node is a parent
		#--  Double-click On child: close treeview, Open file or project
		TreeViewClose
	   TreeViewItemSelected
	   # if { $::FileExtension == ".prj" } {  InitProjectMode $::FileName }
	}
	bind $::tree <Up>  { ttk::treeview::Keynav %W up; break }
	bind $::tree <Down>  { ttk::treeview::Keynav %W down; break }
	bind $::tree <3> { ;  # Single right-click  }
}


#==  OPEN TREEVIEW ACTIONS
proc TreeViewOpen {} {
	#-- For XFCE:  Disable geometry propagation if window already maximized (bug when opening a left pane)
	if { [wm attributes . -zoomed] }	{
		pack propagate . false
	} else { pack propagate . true }
	#--  Force Text Widget layout at 'large'
	SetTextWindowWidth large
	#pack forget $::hpane.scrollX;			# Hide Text scroll x bar
	#--  Insert Treeview. Load treeview width (with sash position)
	$::hpane insert 0 $::hpane.tree;  			# Insert treeview in horiz. pane
	update idletasks
	set ::TreeWidth $::cfg(INFO.LeftPaneWidth);  # Get width from config file
	switch -- $::PaneView {
      	"Bookmark" 		{ set TreePos [lindex $::TreeWidth 0] }
      	"RecentFile"	{ set TreePos [lindex $::TreeWidth 1] }
      	"TOC" 			{ set TreePos [lindex $::TreeWidth 2] }
			"Spell"			{ set TreePos [lindex $::TreeWidth 3] }
			"ProjectMode"	{ set TreePos [lindex $::TreeWidth 4] }
			default 			{ set TreePos 400 }
	}
	$::hpane sashpos 0  $TreePos
	#--  Reset Treeview content / Buttons
	$::tree delete [$::tree children {}];   			# Delete items on treeview
	foreach item { b1 b2 b3} {
		$::tree.tb.$item configure -text "";			# Reset button text
		tooltip::tooltip $::tree.tb.$item "";    		# Reset tooltips
	}
}


#==  CLOSE TREEVIEW ACTIONS
proc TreeViewClose {} {
	#puts "DebugTree List (non-recursive): [$::tree children {}  ]"
	#puts "DebugTree List (recursively): [GetTreeItem $::tree {}  ]"
	#puts "DebugTree Item Selected: [$::tree focus] "
	#-- Save/Reset list /Treeview widget position
	switch -- $::PaneView {
      	"Bookmark" 		{ SaveBookmarkList; lset ::TreeWidth 0 [$::hpane sashpos 0]}
      	"RecentFile"	{ SaveRecentFileList; lset ::TreeWidth 1 [$::hpane sashpos 0]}
      	"TOC" 			{ set ::status ""; lset ::TreeWidth 2 [$::hpane sashpos 0]}
			"Spell"			{	set SpellStart 1.0
									$::txt tag remove search 0.0 end;          # Remove all tags 'search'
									$::txt tag remove sel 0.0 end;             # Remove all tags 'sel'
									set ::status ""
									lset ::TreeWidth 3 [$::hpane sashpos 0]
								}
			"ProjectMode"  { lset ::TreeWidth 4 [$::hpane sashpos 0] }
			default 			{ }
	}
	#--  Save left pane position (sash) and  Close Treeview
	if { $::PaneView ne "" } {
		set ::cfg(INFO.LeftPaneWidth) $::TreeWidth
		$::tree delete [$::tree children {}];   	# Delete all items on treeview
		$::hpane forget $::tree;						# Close treeview
		config:save $::ConfFile ""
	}
	#--  Close/Reset Paned Windows
	SetTextWindowWidth $::cfg(GEOMETRY.PageView);		# Restore layout (large or portrait)
	#pack $::hpane.scrollX -side bottom -fill x -expand false;  # Restore text scroll x
	set ::PaneView "";								# Reset the PanView name
	focus $::txt;										# Focus on text widget
}


#==   ITEM SELECTED ACTIONS
proc TreeViewItemSelected {} {
      regsub -all {\{|\}} [ $::tree selection ] {} TextSelect
      set TextSelect [string trim $TextSelect]
		#puts "DebugTreeSel1 : $TextSelect   View: $::PaneView   "
      #puts "DebugTreeSel2 : Id:[$::tree focus]  Values:[$::tree item [$::tree focus ]]"
      #puts "DebugTreeSel3 : Id:[$::tree focus]  Text : [$::tree item $TextSelect -text]"
      if { $TextSelect eq "" } { focus $::txt; return }
		switch -- $::PaneView {
      	"TOC" 		{ $::txt yview [$::txt index $TextSelect ]; # View text as top of the window }
      	"RecentFile" {	LoadFile $TextSelect "" }
      	"Bookmark"	{	;# Save Bookmark if project file, load file
								if { [file extension $TextSelect ] eq ".prj" } {
									SaveBookmarkList
								}
								LoadFile $TextSelect "" 
							}
      	"ProjectMode" 	{  LoadFile $TextSelect "NoBackup"
									HideCodeBlock true;  # collapse function blocks on code (code folding)
								}
			"Spell"		{ ;#--  Delete selection and replace with selected word
									set w [$::tree item $TextSelect -text]; #Replace with
									$::txt replace  sel.first  sel.last  $w
									ParseLine [GetRowNumber] [$::txt get "insert linestart" "insert lineend" ]
									$::txt tag add search insert-[string length $w]c insert; # necessary to indicate a spell check is ongoing
									event generate $::txt <F2>; # Continue spell check
							}
		}
      focus $::txt
}


#==  RETURN ALL CHILD (including sub-nodes) GIVEN AN INITIAL ITEM (id={}: root}
proc GetTreeItem {tree id} {
	set res [list]
	if { $id ne "" } { lappend res $id }
	foreach x [$tree children $id] { lappend res {*}[GetTreeItem $tree $x] }
	#puts "_TreeView_GetTree\n$res"
	return $res;
}

#==  RETURN NUMBER OF CHILDREN (for a given node)
proc GetChildrenNumber {tree id} {
	return [llength  [$::tree children $id]]
}

#===   LOAD TREEVIEW FILE  (Project File)
proc LoadTreeViewFile { treefile } {

	# 1. May contain a config section as INI/TOML files
	# 2. Contain a list of Files with Tab indentation for tree level

	#-- Open file and init section
   set infile [open  $treefile "RDONLY CREAT" ]
   set data [read $infile]
	set section ""
	#-- Insert/Define item
	set ParentList { "" "" "" "" "" ""};					# Treeview structure
	foreach item [split $data "\n"] {
		if { $item eq ""} { continue };					# nothing to do, next item
		#puts "DebugPaneView: $item"
		#--  Define the item level based on white-space/tabs indentation
		regexp -indices {\S} $item Pos;				# Fist Position of non-whitespace
		set LevelItem [expr [lindex $Pos 0]+1];	# Level of item (1=root)
		#puts "_TreeviewLoad: $LevelItem  $item"
		#--  Item is a directory : Reformat to be a parent node
		#if [file isdirectory [GetAbsolutePath [string trim $item] $treefile] ] {
			#set item [string map {"." "" "/" " "} $item]
		#}
		#--  Reformat relative path to absolute path
		if [ regexp {^\s*?\./.*?$} $item ] {
			set item [GetAbsolutePath [string trim $item] $treefile]
			#puts "_TreeViewLoad: $item > [GetAbsolutePath [string trim $item]  $treefile]"
		}
		#--  Insert in tree depeding if it's a comment/a parent/a child
		switch -regexp $item {
			{^$}		{;#  Empty line : Do nothing }
			{^#.*}	{;#  Comment : Do nothing }
			{^\[.*}	{ set section [string map {"[" "" "]" ""} $item]
						}
			{[^/].+?=.+?}	{; # Assign config value (as ini/toml config file)
								lassign [config:parseline $item]  name value comment
								set ::cfg($section.$name) $value
						}
			{/.*}	{; # file path. Add item to tree if not exist
						if {! [ $::tree exists $item ]} {
							$::tree insert [lindex $ParentList $LevelItem] end -id $item -text [file tail $item]
						}
					}
			default {; #  Not a comment/file path starting with / : it's a parent
				lset ParentList $LevelItem $item;   # Set the name in ParentName list
				set TopParent [lindex $ParentList [expr $LevelItem -1]]
				#puts "DebugBookmark: ---  $ParentList (TopParent: $TopParent)"
				#--  Expand Node if item has trailing spaces
				if { [regexp {^.*\s+$} $item] } {set OpenNode true} else {set OpenNode false}
				#-- Add Item. Special colors (Tag BookCategory in theme.tcl)
				$::tree insert $TopParent end -id $item -text "  [string trimleft $item]" -tags "BookmarkCategory" -open $OpenNode
				}
		};  # end switch
	}; # end foreach item
	close $infile
}


#==  EXECUTE PROCEDURE AT START
InitTreeView
ConfigTreeButtonLeft
ConfigTreeButtonMiddle
ConfigTreeButtonRight
ConfigTreeEvent
