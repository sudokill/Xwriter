#!/usr/bin/wish
#  Manage Table Of content in left pane view or inside the document


#==   REGENERATE TABLE OF CONTENT INSIDE DOCUMENT (Experimental)
# (IF Tag 'TOC' is found. Tag TOC is generated in file 'parse.tcl' )
proc  Update_TableOfContent {} {
	#foreach { start end } [$::txt tag ranges TOC] {}
   #   $::txt mark set insert "$TOCpos lineend"; # place insert cursor at TOC position
   #   CreateTOC "insert"
}


#===  CREATE  TOC/TableOfContent  
proc CreateTOC { Mode } {

	#  Mode= 'display' (view TOC on left pane) or 'insert' (insert TOC on document)
	#--  Init Left Pane view to display the Table of content 
	if { $Mode eq "display" } {
		set ::PaneView "TOC"
		TreeViewOpen
		$::tree heading #0 -text "Table of Contents" -anchor center; # anchor w/e/center
	}

	#-- Get Text Buffer with all tag values. Search Heading tag
	set TOClist ""    ;#  Init TOC list
	set Offset 1  		;#  TOC indentation offset (heading2 unless heading1 found)
	foreach {key value index} [$::txt dump 1.0 end] {
		if { $key eq "tagon"} {
			#puts "DebugPaneView: key:$key  value:$value  index:$index "
			if { [ string match "heading\[1-6\]" "$value" ] } {
				set TextWithoutSymbol "[$::txt get $index "$index lineend" ]"
				set TextWithSymbol "[$::txt get "$index linestart" "$index lineend" ]"
				#-- Replace some chars (quote char ") and calculate indentation
				set TextWithoutSymbol [string map { \" ' } $TextWithoutSymbol];	# Replace quote char with '
				set TextWithSymbol [string map { \" ' } $TextWithSymbol];	# Replace quote char with '
				if { [ string match "heading1" "$value" ] } { set Offset 0}
				set Indent [ expr [string first " " $TextWithSymbol] -1 -$Offset ]
				#puts "DebugTOC: <$Indent> <$TextWithoutSymbol> <[Header2Id $TextWithoutSymbol]> <$index>"
				#-- Set anchor at header position. Used by TOC inside document
				$::txt mark set [ Header2Id $TextWithoutSymbol] $index
				#-- Build TOC
				#if { $Indent eq 0 } {set TextWithIndent "• "} else {set TextWithIndent " "}
				set TextWithIndent [ string repeat " " [expr 4*$Indent]]$TextWithoutSymbol
				#--  to Display TOC on left pane
				if { "$Mode" eq "display"} {
					if { ! [$::tree exist $index] } {
						$::tree insert {} end -id $index -text $TextWithIndent -values $TextWithSymbol 
					}
				}
				#-- To insert TOC directly in document as [HeaderName](#HeaderID)
				if { "$Mode" eq "insert" } {
					lappend TOClist "[ string repeat "\t" $Indent]•  \[$TextWithoutSymbol\](#[Header2Id $TextWithoutSymbol])"
				}
			}
		}
	}

	#---  Insert the table of content in the document (Menu>ins>TableOfContent)
	if { "$Mode" eq "insert" } {
		#puts "DebugPaneView: TOClist: $TOClist"
		#$::txt insert insert "{{" syntax "Table Of Content"  TOC  "}}" syntax
		foreach line $TOClist  {
			$::txt insert insert "\n$line" TOC
			ParseLine [GetRowNumber] $line
		}
		$::txt insert insert "\n"
	}
	#if { $TOClist eq "" } { set ::status " Warning: No headings found" }
}




#===   CONVERT HEADER NAME TO HEADER ID  ( Used by procedure 'CreateTOC' after )
proc Header2Id { HeaderName } {
	set Format "hyphen"
	set HeaderName [ string trim $HeaderName ];				# Remove Leading/Trailing spaces
	#--  1st Option:  Replace with hyphens (source: gitlab)
	if { $Format eq "hyphen" } {
   	set HeaderName [string tolower $HeaderName ];		# lower case
		set HeaderName [string map { / "" } $HeaderName];	# Replace some chars
		set HeaderName [string map { é e è e ê e ç c à a â a ê e î i ô o } $HeaderName];	# Replace french accent
   	regsub -all {[^a-z0-9\-]} $HeaderName {-} HeaderName; # Non-word text = hyphen
   	regsub -all {\s+} $HeaderName {-} HeaderName;		# All spaces convert to hyphens.
   	regsub -all {[-]+} $HeaderName {-} HeaderName;		# More than 2 hyphens: one hyphen
   	regsub -all {[-]$} $HeaderName {} HeaderName;		# Trailing hyphen remove
		#puts "DEBUG Header2Id : $HeaderName"
   	#regsub -all {[^\w-]} $HeaderName {} HeaderName; # Non-word text except hyphen removed.
	}
   #-- ToDo: If header ID already exist, append an incrementing number (starting at 1)
   return $HeaderName
}


