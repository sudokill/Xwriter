#!/usr/bin/wish
# Provide extra functions
#  1. Add 2 trailing spaces to simulate new line
#	2. Remove trailing spaces
#  3. Concatene list of files ( as cat in unix )
#  4. Execute shell command
#  5. Seach Pattern in recent file list

#==  FORCE LINEBREAK  (Add 2 trailing space at End of line )
proc ForceLineBreak {} {
	set OldPosition [ $::txt index current ]
	set Data  [split [$::txt get 1.0 "end-1c" ] "\n"]
	#--  Add 2 spaces at end of lines except some case (empty lines, header,...)
	foreach Row  $Data  {
		incr RowNum 1
		set ListTag [$::txt tag names $RowNum.0]
		#puts "DebugExtraBreak: $RowNum  $Row\n  $ListTag\n-----"
		if { $Row ne "" && ![regexp { {2,}$} $Row ] && \
				![string match "*Code*" $ListTag] && ![string match "*metadata*" $ListTag] && \
				![regexp {^(/{1,3}|>{1,3}|#{1,6}|\t*(\*|-|\+|•)|:>|\[\^|\|)\s+} $Row ]  && \
				![regexp {^\*{3,}$|^-{3,}$|```} $Row ] } {
   		append TextNew "$Row  \n"
		} else  { append TextNew "$Row\n" }
	}
	#--  Replace/Reformat the text
	$::txt replace 1.0 end $TextNew;
	$::txt delete end-1l end;							# Delete last line
	ParseDocument
	$::txt see [$::txt index $OldPosition ];     # GoTo position previously saved
	set ::status " Force newline: Done"
}

#==  REMOVE TRAILING SPACE
proc RemoveTrailingSpace {} {
	set OldPosition [ $::txt index current ]
	set Data  [split [$::txt get 1.0 "end-1c" ] "\n"]
	$::txt delete 1.0 end;
	foreach Row $Data {
		#-- Remove trailing space EXCEPT Empty line(2 spaces)/Title/Table
		if { ![regexp {^ {2,}$|^# |^\|} $Row ] } {
			$::txt insert [incr RowNum 1].end "[string trimright $Row ]\n"
		} else { $::txt insert [incr RowNum 1].end  "$Row\n" }
	}
	$::txt delete end-1l end;							# delete last line
	ParseDocument
	$::txt see [$::txt index $OldPosition ];     # scroll at Old position
	set ::status " Remove Spaces: Done"
}


#==  CONCATENE SEVERAL FILE IN ONE (# comment ignored)
proc ConcateneFiles { ListFiles } {
	#puts "DebugInclude: List: $ListFiles"
   foreach item [split $ListFiles "\n"] {
		#--  Check file path is not comment/empty. Append file content if exist
		if { ! [string match "#*" $item] && $item ne "" } {
      	set PathFile [GetAbsolutePath $item $::FileName]
      	#puts "DebugInclude: + <$PathFile>"
      	if { [file exists $PathFile] } { append result [fileutil::cat $PathFile ] }
		}
   }
   return $result
}


#==  EXECUTE SHELL COMMAND read from entry widget
proc ShellCommand {} {

	#--  Search command in alias.cfg file. Subsitute if found
	set command [string trim $::status]
	catch { set command $::cfg(ALIAS.$command) };  # Search command alias in alias.cfg
	set ::status " $command"
	#  Convention : %i=document name    %d=document directory.  
	if { [regexp ".*%i.*" $command] && [$::txt edit modified] } { 
		set ::status "(Unsaved) $command"
	}
	#--  Format command before eecution
	regsub -all "%i" $command "\"$::FileName\"" command
	regsub -all "%d" $command "\"[file dirname $::FileName]\"" command
	set command "cd \"[file dirname $::FileName]\"; $command"
	#puts "_ShellCommand:  $command"

	#--  Execute command in terminal
	if { $::cfg(MAIN.Terminal) ne "xfce4-terminal"} {
		exec {*}$::cfg(MAIN.Terminal) -e sh -c "$command" &
	} else {
		exec $::cfg(MAIN.Terminal) -x sh -c "$command" &
	}
	#--  Without Terminal. Direct Shell command
	#exec {*}sh -c "$command" &
}


