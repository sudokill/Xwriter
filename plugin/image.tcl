#!/usr/bin/wish
#  1. Draw image (png ,jpg ,bmp ,gif ,tif ,xbm ,tga, webp,svg )
#	2. webp/svg image -> png conversion with 'webp/librsvg2-bin' debian package
#  3. Define the path to draw the image :
#		if web image, PhotoPath = .cache/xwriter/image.jpg (downloaded in cache)
#		if svg/webp image, PhotoPath = /tmp/image.svg.png ( image conversion )
#		else PhotoPath = absolute path of image link in the document
#  4. Scale image size (x1,x2,x3... or x1/2,x1/3...)

#===   INIT
set ::CachePath "$::env(HOME)/.cache/xwriter";  # Cache where downloaded Image will be stored
file mkdir $::CachePath
#--  Define a tag for image ( required for event when clicking on image )
$::txt tag configure TagImage			-relief solid
$::txt tag configure TagImageError	-relief solid


#=====   INSERT IMAGE  (Index = position as Line.Column)
proc InsertImage { ImagePath  Index  Width } {

	#--  Ask Image File if not defined
	set NewImage false
	if { $ImagePath eq "" } {
		after idle ForceWindowSize;  			# Force OpenFile Window Size
		set ImagePath [ tk_getOpenFile -initialdir $::FilePath ]
		if { $ImagePath eq "" } { return }
		set ::FilePath [ file dirname $ImagePath ];  # Keep last open directory
		#--  Instruction to insert the image makdown syntax later
		set NewImage true;
	}

	#--  Init
	set ImageName [file tail $ImagePath ];	# without path, with extention
	#set RelativePath [GetRelativePath $ImagePath $::FileName]

	#--  Download image with http link
	if { [ string match "http*" "$ImagePath" ] } {
		set CacheName [GetCacheName $ImagePath]; # Build Image cache Name based on MD5
		if { $::ImageDownload && ![file exists $CacheName] } {
			#puts "DebugImage: Download WebImage $ImageName in $CacheName"
			#catch {exec wget -o /tmp/wget.log -N --timeout=5 -O $CacheName $ImagePath }
			catch {exec curl -Lk --connect-timeout 7 --retry 1  $ImagePath --output $CacheName }
		}
		if { [file exists $CacheName] && [file size $CacheName] ne 0 } {
			set PhotoPath $CacheName
		} else {  set PhotoPath "ERROR" }
	}

	#--  Set Photo path ( absolute path) from image path (if not http)
	if { ! [ string match "http*" "$ImagePath" ] } {
		set PhotoPath [GetAbsolutePath $ImagePath $::FileName]
	}


	#--  Convert SVG/WEBP image to png ( Require Package librsvg2-bin and webp )
	#   Note : lib/tksvg0.12 does not work with svg with text
	set ImageExtension [ file extension $PhotoPath ]
	#set FileType [fileutil::fileType $ImagePath]; # Don't work with some svg image
	#--  Check file extension or file type to convert with external application
	if { $ImageExtension eq ".webp" || $ImageExtension eq ".svg" } {
			set NewPath $::TempDir/$ImageName.png
			if { $ImageExtension eq ".webp" } {
				catch { exec dwebp $PhotoPath -o $NewPath }
			}
			if { $ImageExtension eq ".svg" } {
				catch { exec rsvg-convert --format png $PhotoPath --output $NewPath }
			}
			set PhotoPath $NewPath
			#puts "DebugImage: Convert  $ImagePath  to $PhotoPath"
	}

	#--  Create/Zoom image
	#puts "DebugImage: $Index  Image Path: $ImagePath  Width: <$Width>"
	#puts "DebugImage:    >>  Photo Path : $PhotoPath"
	if {  "$PhotoPath" ne "ERROR" } {
		catch {
			image create photo $ImagePath -file $PhotoPath
			if { [string match "*px" $Width ] || [string match "*%" $Width ] }  {
				scaleImage $ImagePath $Width
			}
		}
	}

	#--  Set error image if necessary
	if { $PhotoPath eq "ERROR" } { set ImagePath ::tk::icons::error }

	#-- Insert Markdown syntax if new image ( Note: Add a space after image syntax for image justification bug )
	if { $NewImage } {
		set ::status " Image: [image width $ImagePath]x[image height $ImagePath]"
		$::txt insert $Index " " normal "!\[Name\](" syntax "$ImagePath" "syntax ImagePath" ")" syntax " " normal
	}

	#-- Insert Image
	catch { $::txt image create $Index -image $ImagePath -name $ImagePath -align center }

	#--  Debug : List All available images
   #foreach img [image names] { puts "DebugImage: InUse [image inuse $img]  $img" }

	#--  Add a Tag to image to assign event when Mouse hover or click on image
	# 			refer to file mouse.tcl for event
	if { $PhotoPath eq "ERROR" } {
		$::txt tag add TagImageError $Index $Index+1c
	} else {
		$::txt tag add TagImage $Index $Index+1c
	}

}


#====   RESCALE IMAGE WITH factor
# factor= 0.2, 0.25, 0.3, 0.5 (Shrink x5,4,3,2) or 2,3,4,5... (Zoom)
proc scaleImage {img width} {
	set widthNum [ string map { "px" "" "%" "" } $width ]
	#--  Compute factor to rescale image (if width in px or in % of screen width)
	if { [string match "*px" $width ] }  {;    #-- Width in px
		set factor [ expr ( $widthNum.0/[image width $img] ) ];  #.0 to have real number
	}
	if { [string match "*%" $width ] }  {;		#-- Width in % of window width
		if { $widthNum > 100 } { set widthNum 100 }
		set factor [ expr ( $widthNum.0*[winfo vrootwidth .]/(100*[image width $img]) ) ];  #.0 to have real number
	}
	#-- Check/Set factor if factor exceed window width
	set factor_max [ expr [winfo vrootwidth .].0/[image width $img] ]
	if { $factor > $factor_max } { set factor $factor_max }
	#-- Rescale image
	if {$factor < 1} {
		set mode -subsample;
		set factor [expr round(1./$factor)]
	} else  {
		set mode -zoom;
		set factor [expr int($factor)]
	}
	#puts "DebugImg: $img  Width:$width   zoom: $factor (Max:$factor_max) Mode: $mode"
   set temp [image create photo];			# Create temp image
   $temp copy $img;   $img blank;  			# Copy source on temp image, blank source 
	$img copy $temp -shrink $mode $factor;	# Copy temp image on source with zoom
	image delete $temp;							# Delete temp image
}


#===   DISPLAY WEB  IMAGE
#--  Find All Web images and re-parse the line (will download image)
proc Display_Web_Image {} {
	set ::ImageDownload 1
	set ::status " Download Web Image: Active"
	if { $::ImageDownload && $::FileName ne "" } {
		foreach { start end } [$::txt tag ranges ImagePath] {
			set ::status " Downloading Image ($start) ..."
			update;  					# Refresh UI to display status message
			set Text [$::txt get "$start linestart" "$end lineend"]
			$::txt replace "$start linestart"  "$start lineend" $Text
			ParseLine [ expr int($start) ] $Text
		}
		set ::status " Downloading Image: Done"
	}
}


#===  RETURN AN IMAGE CACHE PATH FOR IMAGE DOWNLOADED FROM WEB
proc GetCacheName { ImagePath } {
	#--  Get Image Real extention ( remove useless text as .png?raw=true )
	regsub {(\?.*)} [ file extension $ImagePath ] {}  ImageExtension
	#-- Cache Name is build with Hash MD5 + Real extension
	return [string tolower "$::CachePath/[ md5::md5 -hex $ImagePath ]$ImageExtension" ]
}


