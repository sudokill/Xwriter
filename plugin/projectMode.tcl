#!/usr/bin/wish
# Define project mode for coding
#  1. Change User-interface (terminal, geometry, binding...)
#  2. Load project file .prj ( toml file with tab indentation )
#  3. View project files on a treeview
#  4. Run/Compile the project
#  5. Code completion


#===  Init Project-mode
proc InitProjectMode { projectfile } {
	set ::projectFile  $projectfile
	set ::cfg(GEOMETRY.PageView) large
	set ::cfg(PROJECT.RUN) ""
	LoadTheme terminal
	#--  Change Tag 'TrailingSpace'  (pb with real-time highlighting)
	$::txt tag configure TrailingSpace -elide 0; # Do not hide trailing space
	$::txt tag delete TrailingSpace
	#--  Shortcut
	bind . <Control-R>   { RunProject; break; }
	bind $::txt <Double-Return> { return 0 };  # unbind keys 
	#-- Load the project file (Open treeview with project file content)
	OpenProjectTree $projectfile
	#config:list PROJECT;   		# Debug: return project config 
}


#===  Exit Project-mode
proc ResetToDefaultMode { } {
	#--  Check document is saved before returning to default mode
   if { [$::txt edit modified] } {
      if { [tk_messageBox -message "Document not saved. Exit Project Mode ? \n" -type yesno -icon question] eq no } { return }
   }
	#--  Reset global variable, close treeview
	set ::projectFile ""
	set ::cfg(GEOMETRY.PageView) portrait
   $::txt tag configure TrailingSpace -elide 1
	TreeViewClose
	#-- Reset text Widget/Filename , Reload Theme/Shortcut
	ResetTextWidget
	LoadTheme; 										
	source "$::ScriptPath/shortcut.tcl"
	set ::status " Default Mode"
}


#===  Open Project Tree / Load Project File
proc OpenProjectTree  { projectfile } {
	set ::PaneView "ProjectMode";
	TreeViewOpen
	set HeaderName [capitalizeEachWord [file rootname [file tail $projectfile] ] ]
	$::tree heading #0 -text $HeaderName -anchor center; # anchor w/e/center
	$::tree.tb.b1 configure -text "Search"
	tooltip::tooltip $::tree.tb.b1 "Search string inside project files (control :)"
	$::tree.tb.b2 configure -text "Run"
	tooltip::tooltip $::tree.tb.b2 "Compile/Run the project (control-R)"
	$::tree.tb.b3 configure -text "Exit"
	tooltip::tooltip $::tree.tb.b3 "Exit project mode"
	LoadTreeViewFile $projectfile
}


#==  Create Project File (compilation instruction/files tree)
proc CreateProjectFile { {ProjectName project} } {

	#--  Check a file inside the project is already loaded
	if { $::FileName eq "" || [wm title . ] eq "Untitled" } { 
		tk_messageBox -message " Load any file (readme...) from project before  \n" -type ok -icon info
		return
	}

	#--  Get a recursive list of files inside same dir than current file
	set OutDirectory  [ file dirname $::FileName ] 
	set ListFile [ fileutil::findByPattern $OutDirectory -regexp -- {.+?} ]
	# Alternative Linux command = find . -type f  -not -path '*/\.*' | grep -I -Ev "\.(bin|o|so|jpg|gif|png|svg|ico|webp)" | tac > project.prj

	#--  Write project File Header
	set Output [open $OutDirectory/$ProjectName.prj w ]
	puts $Output "\[PROJECT\]"
	puts $Output "#RUN = sudo ./configure && make && sudo make install && ./MyApp\n"
	puts $Output "\[PROJECT-BOOKMARK\]\n./$ProjectName.prj\n"
	puts $Output "\[PROJECT-TREE\]"
	puts $Output "#  To add a branch, insert a line with the branch name"
	puts $Output "#  Use Tab indentation to configure level inside the tree"

	#--  Write files list ( files path relative to root file )
	set NumberFiles 0
	foreach item $ListFile { 
		incr NumberFiles
	   if { [file type $item ] eq "file" } { 
			regsub -all -line "^$OutDirectory" $item {.} item;  # Convert to relative path
			puts $Output $item
		}
	}
	close $Output
	LoadFile "$OutDirectory/$ProjectName.prj" ""
	#puts "_CreateProject: $OutDirectory/$ProjectName.prj  $NumberFiles files"
}


#==  Compile/Run the entire project
proc RunProject {} {

	#-- Check file status. Reload project file (in case RUN config changed)
	if { [$::txt edit modified] } {
		tk_messageBox -message " Document not saved. Abort \n"
		return
	}
	if { $::FileName eq $::projectFile } { LoadFile $::projectFile ""}

	#-- Get 'Make/Run' command for the project and execute in terminal 
	if { $::cfg(PROJECT.RUN) != "" } {
		set command "cd \"[file dirname $::projectFile]\"; \
							$::cfg(PROJECT.RUN) || read c "
	  	exec {*}$::cfg(MAIN.Terminal) -e sh -c "$command"
		# InitExtraView DebugCode;  				# Load Debug File
		#  Use " > $::DebugCode 2>&1" to debug to file
	} else {
   	tk_messageBox -message "\n RUN instruction missing in project file  \n" -type ok -icon error
	}
}


