#!/usr/bin/wish
#    Convert Markdock to HTML. Use export.css as style-sheet file
#    Convert line-by-line (prevent reformating in code block), then Convert all


#==  REFORMAT TEXT:  markdock syntax > pure markdown/HTML  syntax
proc Convert2Markdown { SourceFile  DestFile } {

	#==  INIT
	set Output [open $::TempDir/export.md w ];
	set CodeBlock false;								# Used to disable format inside CodeBlock

	#== GET TEXT TO EXPORT ( Selection or Entire text if no selection )
	if { [$::txt tag ranges sel] ne "" } {
 		set Data [$::txt get [$::txt index sel.first] [$::txt index sel.last] ]; # Get selection
	} else { set Data [$::txt get 1.0 "end-1c"] }

	#==  INCLUDE FILES ( if {{include ...}} or ```include ...``` )
	set Data [ IncludeFiles $Data ]

   #--  Replace list symbol '•' with '*'
	regsub -all -line  {^(\s*)(?:•)(.*?)} $Data {\1*\2} Data
   #--  Replace details symbol '▶' with ':>' (expandable content)
	regsub -all -line  {^(▶)(.*?)} $Data {:>\2} Data
	#--  Replace Task ToDo "- [ ]" or Done "- [x]" with  ☐	☑
	regsub -all -line {^(- \[x\])(.*?)$} $Data {☑\2} Data
	regsub -all -line {^(- \[ \])(.*?)$} $Data {☐\2} Data

	#-- CONVERT markdock>markdown LINE-BY-LINE EXCEPT CODEBLOCK/CODEINLINE
	set lines [split $Data "\n"];					# Array with each line of file
	foreach line $lines {

		#--  Evaluate if inside codeblock
		if { [ regexp {(^```)(.*?)$} $line ] } {
			if { ! $CodeBlock } { set CodeBlock true } else { set CodeBlock false }
		}

		if { ! $CodeBlock } {

		   #--  Replace list beginning with TABS by spaces 
			#    ( better with pandoc parsing for list if no empty line before list )
			regsub -all -line  {^\t(\*|-|\+|•)(\s+)} $line {   \1\2} line
			regsub -all -line  {^\t\t(\*|-|\+|•)(\s+)} $line {      \1\2} line
			regsub -all -line  {^\t\t\t(\*|-|\+|•)(\s+)} $line {         \1\2} line
			regsub -all -line  {^\t(\d+\)|\d+\.)(\s+)} $line {   \1\2} line
			regsub -all -line  {^\t\t(\d+\)|\d+\.)(\s+)} $line {      \1\2} line
			regsub -all -line  {^\t\t\t(\d+\)|\d+\.)(\s+)} $line {         \1\2} line

			#--  Replace line with only  "\" or 2 spaces  with HTML break-line <br />
			regsub -all -lineanchor {^\\\s*$} $line {<br />} line
			regsub -all -lineanchor {^ {2,}$} $line {<br />} line

			#-- Reformat Alerts (admonition)
			#regsub -all -lineanchor {(>\s*?\[!WARNING\])} $line {>  <span style='color:#8d6b31;font-weight:bold;'> ⚠ Warning </span>} line

			#--  Replace '[' with invisible char before 'subst'
			regsub -all -lineanchor {\[} $line "\u3164" line;# Replace '[' with invisible char to prevent error with 'subst'

			#--  Replace Header1 with 2 trailing spaces with H1/class=title (cf CSS )
			regsub -all -lineanchor {^(#\s+)(.*?)  $} $line {<h1 class="Title">\2</h1>} line

			#-- Generate Header for i=1-6   <hi id="HeaderID generator"> xx  </hi>
			if { !($::PandocMissing) } {
				#--  pandoc ONLY : Add a tab character \x09 (pandoc bug with header beginning with number (1.xx)
				regsub -all -lineanchor {(^#{1,6} )(.+?)$} $line \
						{<h[expr [string last "#" {\1}] +1] \
						 	id="[Header2Id {\2}]">\x09 \2</h[expr [string last "#" {\1}] +1]>} line
			} else {
				#--  NOT pandoc ONLY  ( lowdown )
				regsub -all -lineanchor {(^#{1,6} )(.+?)$} $line \
						{<h[expr [string last "#" {\1}] +1] \
						 	id="[Header2Id {\2}]">\2</h[expr [string last "#" {\1}] +1]>} line
			}
 			catch { set line [subst  -nobackslashes -novariables $line] }

			#--  Restore Invisible char with '[' after 'subst'
			regsub -all -lineanchor "\u3164" $line {[} line;  # Replace Invisible char with '['


			#-- Replace Text justification
			#regsub -all -lineanchor {([^^]`)(.+?)(`)} $line {<code>\2</code>} line
			regsub -all -lineanchor {(^///\s+)(.*?)$} $line {<span class="right">\2</span>} line
			regsub -all -lineanchor {(^//\s+)([^\{].*?)$} $line {<span class="center">\2</span>} line
			regsub -all -lineanchor {(^/\s+)(.*?)$} $line {<span class="fulljustify">\2</span>} line

			#-- Evaluate tcl code inline if  `tcl  command...`
			#  Note :  The line should not contain [ or ] except between backticks
			if { $::CodeEval && [regsub -all -lineanchor {(`tcl )(.*?)(`)} $line {[\2]} line]} {
				catch { set line [subst  -nobackslashes -novariables $line] }
			}

			#--   NOT IN CODE INLINE
			if { ! [ regexp {(`)(.+?)(`)} $line ] } {;   # Not in  Code Inline

				#--  Replace '[' with invisible char before 'subst'
				regsub -all -lineanchor {\[} $line "\u3164" line

				#--  Define ReferencePath for image
				if { $DestFile ne "$::TempDir/preview.html" } { set ReferencePath ""
				} else { set ReferencePath $SourceFile }
				#puts "DebugExportHTML : Dest:$DestFile  Source:$SourceFile  RefImage:$ReferencePath"

				#--  Replace ![name;100px](xx) with <img src='xx' width='100px'></img>
				if { [
					regsub -all -lineanchor {(\!\u3164)(.*?)(\]\()(.+?)(\))} $line \
						{<img  src='[GetAbsolutePath {\4} $ReferencePath]' \
							width='[lindex [split {\2} ";"] 1]'></img>} line ] } {
						catch { set line [subst  -nobackslashes -novariables $line]
					}
				}

				#--  Restore Invisible char with '[' after 'subst'
				regsub -all -lineanchor "\u3164" $line {[} line

				#--  Reformat Page Break
				regsub -all -lineanchor {^\*{3,}$} $line {<div style='page-break-after: always;'></div>} line; #PDF page break
				#--  Reformat some inline syntax
				regsub -all -lineanchor {(___)(\S.*?\S)(___)} $line {<b><u>\2</u></b>} line; #BoldUnderline
				regsub -all -lineanchor {(__)(\S.*?\S)(__)} $line {<u>\2</u>} line; #Underline
				regsub -all -lineanchor {(==)([^=]+?)(==)}  $line {<mark>\2</mark>} line
				regsub -all -lineanchor {(\^)((?=\S).*?\S)(\^)} $line {<sup>\2</sup>} line
				regsub -all -lineanchor {(~)([^~]+?)(~(?!~))}  $line {<sub>\2</sub>} line

				#--  Replace 'space+Tab' and '5spaces'  with &emsp; or CSS tag 'tab'
				#regsub -all -lineanchor {(?!(^|\S))\t} $line { \&emsp; } line
				regsub -all -lineanchor { \t} $line { \&emsp; } line
				#regsub -all -lineanchor { \t} $line {<tab/>} line

			}; # Endif  Not Code Inline


		}; #  Endif Not In CodeBlock

		# puts "DebugExport: $line"
		append DataNew "$line\n";   # Will be used after to format globally

	};  # End foreach line

	#==  SUBSITUTE TEXT GLOBALLY ( HTML alias as "text"{alias1 alias2} )
	#  Require to substitute '[' char before executing 'subst' command

	regsub -all -lineanchor {\[} $DataNew "\u3164" DataNew;# Replace '[' with invisible char to prevent error with 'subst'
		#--  Reformat HTML Alias WITHOUT newline ( "Text"{alias list} ) )
		if { [ regsub -all -lineanchor {"([^"\n]+?)"\{(.+?)\}} \
				$DataNew {<span [format_HTMLalias {\2}]>[string trim {\1}]</span>} DataNew ] } {
		}
		#--  Reformat HTML Alias WITH newline  ( "Text\nText"{alias list} ) )
		if { [ regsub -all -lineanchor {"([^"]*?\n[^"]*?)"\{(.+?)\}} \
				$DataNew {<div [format_HTMLalias {\2}]>[string trim {\1}]</div>} DataNew ] } {
		}

		#--  Subsitute  String now
		catch { set DataNew [subst -nobackslashes -novariables $DataNew] }
	regsub -all -lineanchor "\u3164" $DataNew {[} DataNew;  # Replace Invisible char with '['

	#--  Reformat indented paragraphs
	regsub -all -lineanchor {(^\|\t)([^\t].*?)(^\s*$)} $DataNew {<span class="indent1">\2</span>} DataNew
	regsub -all -lineanchor {(^\|\t\t)([^\t].*?)(^\s*$)} $DataNew {<span class="indent2">\2</span>} DataNew
	regsub -all -lineanchor {(^\|\t\t\t)(.*?)(^\s*$)} $DataNew {<span class="indent3">\2</span>} DataNew

	#-- Reformat Alerts (admonition)
	#regsub -all -lineanchor {(>\s*?\[!WARNING\])(.*?)(^\s*$)} $DataNew {<div class="warningMessage"><span style='color:#8d6b31;font-weight:bold;'> ⚠ Warning</span>\n\2</div>} DataNew

	#-- Reformat details to HTML details <details><summary>xx</summary>yy</details>
	regsub -all -lineanchor {(:>)(.*?)\n(.*?)(^$)} $DataNew \
			{<details><summary>\2</summary>\3</details>} DataNew

	#--  Reformat MERMAID code block
	regsub -all -lineanchor {(```mermaid)(.*?)(```)} $DataNew {<pre class="mermaid">\2</pre>} DataNew

	#--  Reformat Newline for pantcl/mkdoc internal converter
	if { ($::PandocMissing) && ($::LowdownMissing) } {
		regsub -all -lineanchor {\n} $DataNew "  \n" DataNew
	}

	#--  Remove YAML metadata block for lowdown converter only
	if { ($::PandocMissing) && !($::LowdownMissing)} {
		if { [ regsub -all -lineanchor {(^---$)(.+?:.+?\n){1,}?(^---$)} $DataNew {} DataNew ] > 1 } {
      	tk_messageBox -message "WARNING : \nMore than 1 YAML metadata block is ignored \nUse more than 3 '-' for horizontal rules" -type ok -icon warning
		}
	}

	#==  Include JavaScript module ( Not working with pantcl )
	#--  Include Mermaid
	append DataNew "<script type=\"module\">\n"
	append DataNew "	import mermaid from 'https://cdn.jsdelivr.net/npm/mermaid@10/dist/mermaid.esm.min.mjs';\n"
	append DataNew "</script>\n"
	#--  Include Mathjax with config  TeX-MML-AM_CHTML
	append DataNew "<script type=\"text/javascript\" src='https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.7/MathJax.js?config=TeX-MML-AM_CHTML'>\n"
	append DataNew "</script>\n\n"


	#==  SAVE FILE
   puts -nonewline $Output $DataNew
   close $Output;

}


#==  EXECUTE THIRD-PARTY CONVERTER  (Define in Menu>Preference)
proc ExportAsHTML { FileName } {

	#--  Display a message. Exporting may takes time. 
	set ::status " Exporting to HTML ..."
	update

	#--  Convert Markock to Markdown	
	Convert2Markdown $::FileName  $FileName;   # SourceFile  DestinationFile

	#--  Init Convert Markdown to HTML
	set FileCSS style.css
	file delete -force $FileName

	#==  COPY CSS/JABASCRIPT FILE 
	catch { exec cp "$::ConfPath/export.css" "[file dirname $FileName]/$FileCSS" }

	#--  Conversion with PANTCL if no pandoc/lowdown installed
	if { ($::PandocMissing) && ($::LowdownMissing) } {
		puts "Warning : Converting to HTML using pantcl (lowdown/pandoc missing)"
		catch { exec $::ScriptPath/plugin/pantcl.tapp $::TempDir/export.md  $FileName  --css "$FileCSS" --no-pandoc }  ErrorVar
	}

	#--  Conversion with LOWDOWN if pandoc not installed
	if { ($::PandocMissing) && !($::LowdownMissing) } {
		#  --html-hardwrap:  Hard-wrap paragraph content by outputting line breaks
		#  --html-no-owasp: used to simplify  Table of Content building
		catch { exec lowdown  --html-no-skiphtml --html-no-escapehtml \
						--html-hardwrap  --parse-no-codeindent  --html-no-owasp \
						--parse-math \
						-s -M "css=$FileCSS" \
						-thtml $::TempDir/export.md > $FileName \
				} ErrorVar
		#puts "DebugExportHTML (lowdown) Done "
	} else { set ErrorVar " Install application lowdown or pandoc " }

	#--  Conversion with PANDOC  if installed
	if !($::PandocMissing) {
		#--  Enable pantcl filters  ( Menu > Tools > Enable Code Evaluation )
		if { $::CodeEval } {
			set PantclFilters " --filter $::ScriptPath/plugin/pantcl.tapp"
		} else { set PantclFilters  "" }
		#--  Execute PANDOC program
		# GetExtentions : pandoc --list-extensions=markdown
		#  --metadata=title: or --metadata title=""  :  Do not use metadata 
 		# Self-contain: 	--self-contained --resource-path=/tmp:[file dirname $FileName]
		catch { eval exec pandoc --standalone \
						--from markdown+hard_line_breaks+autolink_bare_uris+lists_without_preceding_blankline$::cfg(PANDOC.PandocExtensions) \
						--to  html5 \
						--mathjax  \
						--css=$FileCSS \
						$::cfg(PANDOC.PandocOptions) \
						$PantclFilters \
						$::TempDir/export.md -o  $FileName } ErrorVar
	}; # End pandoc exec

   #-- If Error found after conversion, stop here
   if { $ErrorVar ne "" } { puts "=== Error/Warning ($FileName)\n $ErrorVar" }
   if { ! [file exists $FileName] } {
      tk_messageBox -message "ERROR : \n$ErrorVar" -type ok -icon error
      return
   }

	#==  DISPLAY MESSAGE/HTML
	set ::status " Open: $FileName"
	$::txt mark set insert 1.0;  # Avoid a bug if insert cursor is on hidden text
	catch { exec {*}$::cfg(MAIN.BrowserViewer) $FileName >& /dev/null &  }
}


#==  INCLUDE FILES  (when exporting with {{include..}} or ```include...```
proc IncludeFiles { Data } {
	#puts "DebugIncludeFile: >>>>>>>>>>>>>>>>>>>>\n$Data"
	regsub -all -lineanchor {\[} $Data "\u3164" Data;# Replace '[' with invisible char to prevent error with 'subst'
	#--  Include file with ```include ....```
	regsub -all -lineanchor {(```include\n)(.+?)(```)} \
							$Data {[ConcateneFiles {\2}]} Data
	#--  Include file with {{include [xx](path)}}
	regsub -all -lineanchor {(\{\{include.*?\()(.+?)(\).*?\}\})} \
							$Data {[ConcateneFiles {\2}]} Data
	#--  Substitute Data now
	catch { set Data [subst -nobackslashes -novariables $Data] }
	regsub -all -lineanchor "\u3164" $Data {[} Data;  # Replace Invisible char with '['
	#puts "\n\nDebugIncludeFile: <<<<<<<<<<<<<<<<<<<<\n$Data"
	return $Data
}


