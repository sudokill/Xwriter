

# Xwriter V0.6.4  
  


###  Update Instruction

After install, execute `  ./xwriter --upgrade `

### What's New

###  Fix
-  Switch theme with toolbar menu do not work 
-  Improve C code highlighting.
-  No task formatting with `- [X]` syntax


