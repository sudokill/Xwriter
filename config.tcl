#==  LOAD/SAVE TOML/INI CONFIG FILES  (in array or dictionary)
#  		Source  : https://wiki.tcl-lang.org/page/Config+File+Parser
# Load setting :  config:open  user.cfg
# Save setting :  config:save  user.cfg  "" 
# Save setting with default template:  config:save  user.cfg  default.cfg 
# Each setting is an array element : $cfg(section.name) --> return value
# Optional method : ini2dict/dict2ini to load/save config in a dictionary
# Upgrade application  : config:upgrade  (Change user-specific files)


#==  OPEN INI/TOML FILE  (To Array)
proc config:open { fname } {
	global cfg

	if { ! [file exists $fname] } { puts " Error Loading $fname"; return 0 }
	set fp [open $fname r]
	set section "";						# Default first section
	set ERROR_MSG ""
	#puts "\n\nDebugConfig:====== FILE : $fname \n"
	while {![eof $fp]} {
		set line [gets $fp]
		incr LineNumber 1
		if { $line eq "" } { continue }; # Nothing to do, go to next line
		lassign [config:parseline $line]  name value comment
		#-- Load config
		switch -regexp -- $line {
			"^#.*"			{; # Comment }
			"^\\[.*\\]$"	{ set section [string map {"[" "" "]" ""} $line] }
			".*=.*"			{; # LOAD/SET Value
									if { $section eq "FONT" && $name ne "" && $value ne "" } {
										catch { font delete $name };		# delete font if already exist
										font create $name {*}$value
									}
									set cfg($section.$name) $value
									#puts "DebugConfig($LineNumber): Load <$section.$name> = <$cfg($section.$name)>"
								}
			default			{ set ERROR_MSG " Invalid configuration in $fname : \n\n $line" }
		}
	}
	close $fp
	if { $ERROR_MSG ne "" } {
		tk_messageBox -message " Error Line: $LineNumber \n$ERROR_MSG" -type ok -icon error
	}
}


#==  SAVE TO INI/TOML FILE  (From Array)
proc config:save { fname default } {

	# 'default' is a template used to update a user-defined config file
	global cfg

	#--  Open config files. If default config file doesn't exist, create it
	if  { $default eq "" } {
		file copy -force $fname $fname.tmp
		set default $fname.tmp
	}
	set fp [open $fname w];			# new config file to write
	set ref [open $default r] ;  # default config

	#--  For each line in default config file, Write new value in destination file
	set section "";  # Default section
	while {[gets $ref line] >= 0} {
		lassign [config:parseline $line]  name value comment
		#-- Save Key/Value/Comment in the new file
		switch -regexp $line {
			"^#.*"			{ puts $fp $line; #  Comments  }
			"^$"				{ puts $fp $line; # Empty line }
			"^\\[.*\\]$"	{ 	puts $fp $line;  # write section
									set section [string map {"[" "" "]" ""} $line]
								}
			".*=.*"			{ if { $comment eq "" } {
										puts $fp "$name = $cfg($section.$name)" 
									} else {
										puts $fp "$name = $cfg($section.$name)    # $comment"
									}
								}
			default			{ puts "Warning: Undefined '$line' in file $default " }
		}; # end switch
	}
	close $fp
	close $ref
	file delete -force $fname.tmp
}


#==  RETURN KEY / VALUE  / COMMENT  FOR A LINE
proc config:parseline { line } {
	set name ""; set value "" ; set comment ""
	regexp -- {^([^=]+)\s*=\s*(.+)$}  $line  all name value
	regexp -all -- {^([^=]+)\s*=\s*(.+)\s+#\s+(.*?)\s*$}  $line  all name value comment
	set name [string trim $name]
	set value [string trim $value]
	set comment [string trim $comment]
	#puts "DebugCFG: <$name>  <$value>  <$comment>"
	return [list $name $value $comment]
}

#==  DISPLAY CONFIG ( section=ALL or xxx)
proc config:list { section } {
	global cfg
	if {$section eq "ALL"} {
		foreach item  [lsort [array names cfg]] { puts " <$item> = $cfg($item)" }
	} else {
		foreach {name value} [array get cfg $section.*] { puts "<$name> = $value" }
	}
}


#===  RETURN A DICTIONARY FROM INI/TOML
proc config:ini2dict { filename } {
	set dic [dict create]
	if [catch {open $filename r} infh] { puts "Cannot open $filename"
	} else {
		set section ""
		set comment ""
		while {[gets $infh line] >= 0} {
			# Process line
			if {[regexp {^\s*#} $line]} { continue
			} elseif {[regexp {^\[(.+)\]} $line -> section]} {
				dict set dic $section [list]
			} elseif {[regexp {^([-A-Z0-9a-z_]+)\s*=\s*(.+)\s+(#.*)\s*$} $line -> key value comment]} {
				dict set dic $section $key [string trim $value]
			} elseif {[regexp {^([-A-Z0-9a-z_]+)\s*=\s*(.+)} $line -> key value]} {
				dict set dic $section $key [string trim $value]
			}
			#puts "Debug: <$section>  <$key>  <$value>  <$comment>"
		}
		close $infh
	}
	return $dic
}

#===  SAVE A DICTIONARY TO INI/TOML
proc config:dict2ini {dic outfile} {
	set out [open $outfile w 0600]
	foreach section [lsort [dict keys $dic]] {
		puts $out "\[$section\]"
		foreach {key value} [dict get $dic $section] { puts $out "$key = $value" }
	}
	close $out
}


#==  DEMO  DICT <--> INI
#set cfg_demo1 [ config:ini2dict config.cfg ]
#puts $cfg_demo1
#config:dict2ini $cfg_demo1 config_demo2.cfg


#==  UPGRADE CONFIG/APPS
proc config:upgrade {} {
	puts "Found Version: ( $::cfg(INFO.Version) )"
	set Version [ string map {"V" "" "." ""} $::cfg(INFO.Version) ]
	if { $Version <= 060 } {
		puts "Apply patch V0.6.0"
		exec sed -i "s/.fr.tr/\$::tree/g; s/.fr.t/\$::txt/g;"  $::ConfPath/theme.tcl
		exec sed -i "s/ShortCode/HTMLalias/g;"  $::ConfPath/theme.tcl; # change string
		exec sed -i "/TableOfContent/d"  $::ConfPath/theme.tcl;   # delete line
		exec sed -i "/.fr configure/d"  $::ConfPath/theme.tcl
		set ::cfg(INFO.Version) "0.6.0"
	}
	if { $Version < 061 } {
		puts "Apply patch V0.6.1"
		exec sed -i "s/\{//g; s/\}//g;"  $::ConfPath/bookmark.md
		exec rm -f $::ConfPath/theme.tcl
		set ::cfg(INFO.Version) "0.6.1"
	}
	if { $Version < 062 } {
		puts "Apply patch V0.6.2"
		set ::cfg(INFO.Version) "0.6.2"
	}
	if { $Version < 063 } { set ::cfg(INFO.Version) "0.6.3" }
	if { $Version < 064 } { set ::cfg(INFO.Version) "0.6.4" }

	#-- Save new version in user-config file ( with ScriptPath/config.cfg as reference )
	config:save $::ConfFile $::ScriptPath/config.cfg
	puts "Upgrade Done.\n"
	exit
}


